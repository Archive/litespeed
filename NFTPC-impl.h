#include "NFTPC.h"

/*** App-specific servant structures ***/

#include <glib.h>
                        




typedef struct {
POA_Metadata servant;
PortableServer_POA poa;
Info attr_metadata;

} impl_POA_Metadata;


typedef struct {
POA_NCC servant;
PortableServer_POA poa;
























} impl_POA_NCC;


typedef struct {
POA_NFTPC servant;
PortableServer_POA poa;


} impl_POA_NFTPC;



/*** Implementation stub prototypes ***/





 void impl_Metadata__destroy(impl_POA_Metadata *servant,
CORBA_Environment *ev);
Info*
impl_Metadata__get_metadata(impl_POA_Metadata *servant,
CORBA_Environment *ev);
void
impl_Metadata__set_metadata(impl_POA_Metadata *servant,
Info* value,
CORBA_Environment *ev);


 void impl_NCC__destroy(impl_POA_NCC *servant,
CORBA_Environment *ev);
void
impl_NCC_All_cb(impl_POA_NCC *servant,
CORBA_long sn,
CORBA_long pointer,
CORBA_Environment *ev);

void
impl_NCC_Open_cb(impl_POA_NCC *servant,
CORBA_long ret,
CORBA_Object loc,
CORBA_long pointer,
CORBA_Environment *ev);

void
impl_NCC_Close_cb(impl_POA_NCC *servant,
CORBA_long ret,
CORBA_long pointer,
CORBA_Environment *ev);

void
impl_NCC_StartRead_cb(impl_POA_NCC *servant,
CORBA_long ret,
CORBA_long id,
CORBA_char * local,
CORBA_long pointer,
CORBA_Environment *ev);

void
impl_NCC_StartWrite_cb(impl_POA_NCC *servant,
CORBA_long ret,
CORBA_long id,
CORBA_char * local,
CORBA_long pointer,
CORBA_Environment *ev);

void
impl_NCC_Read_cb(impl_POA_NCC *servant,
CORBA_long ret,
Buffer* buf,
CORBA_long pointer,
CORBA_Environment *ev);

void
impl_NCC_Write_cb(impl_POA_NCC *servant,
CORBA_long ret,
CORBA_long pointer,
CORBA_Environment *ev);

void
impl_NCC_GetMetaData_cb(impl_POA_NCC *servant,
CORBA_long ret,
Metadata data,
CORBA_long pointer,
CORBA_Environment *ev);

void
impl_NCC_SetMetaData_cb(impl_POA_NCC *servant,
CORBA_long ret,
CORBA_long pointer,
CORBA_Environment *ev);

void
impl_NCC_GetResource_cb(impl_POA_NCC *servant,
CORBA_long ret,
CORBA_char * resource,
CORBA_long pointer,
CORBA_Environment *ev);

void
impl_NCC_SetResource_cb(impl_POA_NCC *servant,
CORBA_long ret,
CORBA_long pointer,
CORBA_Environment *ev);

void
impl_NCC_GetCurrentBase_cb(impl_POA_NCC *servant,
CORBA_long ret,
CORBA_char * base,
CORBA_long pointer,
CORBA_Environment *ev);

void
impl_NCC_Create_cb(impl_POA_NCC *servant,
CORBA_long ret,
CORBA_long pointer,
CORBA_Environment *ev);

void
impl_NCC_Delete_cb(impl_POA_NCC *servant,
CORBA_long ret,
CORBA_long pointer,
CORBA_Environment *ev);

void
impl_NCC_Move_cb(impl_POA_NCC *servant,
CORBA_long ret,
CORBA_long pointer,
CORBA_Environment *ev);

void
impl_NCC_Copy_cb(impl_POA_NCC *servant,
CORBA_long ret,
CORBA_long pointer,
CORBA_Environment *ev);

void
impl_NCC_List_cb(impl_POA_NCC *servant,
CORBA_long ret,
InfoList* list,
CORBA_long pointer,
CORBA_Environment *ev);

void
impl_NCC_Aliases_cb(impl_POA_NCC *servant,
CORBA_long ret,
CORBA_Object loc,
CORBA_long pointer,
CORBA_Environment *ev);

void
impl_NCC_Pause_cb(impl_POA_NCC *servant,
CORBA_long ret,
CORBA_long pointer,
CORBA_Environment *ev);

void
impl_NCC_Resume_cb(impl_POA_NCC *servant,
CORBA_long ret,
CORBA_long pointer,
CORBA_Environment *ev);

void
impl_NCC_Abort_cb(impl_POA_NCC *servant,
CORBA_long ret,
CORBA_long pointer,
CORBA_Environment *ev);

void
impl_NCC_NewMetadata_cb(impl_POA_NCC *servant,
CORBA_long ret,
Metadata data,
CORBA_long pointer,
CORBA_Environment *ev);

void
impl_NCC_NewLocation_cb(impl_POA_NCC *servant,
CORBA_long ret,
CORBA_Object loc,
CORBA_long pointer,
CORBA_Environment *ev);

void
impl_NCC_Response_cb(impl_POA_NCC *servant,
CORBA_long ret,
CORBA_char * resp,
CORBA_long pointer,
CORBA_Environment *ev);


 void impl_NFTPC__destroy(impl_POA_NFTPC *servant,
CORBA_Environment *ev);
void
impl_NFTPC_GetRecur_cb(impl_POA_NFTPC *servant,
CORBA_long ret,
CORBA_long pointer,
CORBA_Environment *ev);

void
impl_NFTPC_PutRecur_cb(impl_POA_NFTPC *servant,
CORBA_long ret,
CORBA_long pointer,
CORBA_Environment *ev);

void
impl_NFTPC_All_cb(impl_POA_NFTPC *servant,
CORBA_long sn,
CORBA_long pointer,
CORBA_Environment *ev);
void
impl_NFTPC_Open_cb(impl_POA_NFTPC *servant,
CORBA_long ret,
CORBA_Object loc,
CORBA_long pointer,
CORBA_Environment *ev);
void
impl_NFTPC_Close_cb(impl_POA_NFTPC *servant,
CORBA_long ret,
CORBA_long pointer,
CORBA_Environment *ev);
void
impl_NFTPC_StartRead_cb(impl_POA_NFTPC *servant,
CORBA_long ret,
CORBA_long id,
CORBA_char * local,
CORBA_long pointer,
CORBA_Environment *ev);
void
impl_NFTPC_StartWrite_cb(impl_POA_NFTPC *servant,
CORBA_long ret,
CORBA_long id,
CORBA_char * local,
CORBA_long pointer,
CORBA_Environment *ev);
void
impl_NFTPC_Read_cb(impl_POA_NFTPC *servant,
CORBA_long ret,
Buffer* buf,
CORBA_long pointer,
CORBA_Environment *ev);
void
impl_NFTPC_Write_cb(impl_POA_NFTPC *servant,
CORBA_long ret,
CORBA_long pointer,
CORBA_Environment *ev);
void
impl_NFTPC_GetMetaData_cb(impl_POA_NFTPC *servant,
CORBA_long ret,
Metadata data,
CORBA_long pointer,
CORBA_Environment *ev);
void
impl_NFTPC_SetMetaData_cb(impl_POA_NFTPC *servant,
CORBA_long ret,
CORBA_long pointer,
CORBA_Environment *ev);
void
impl_NFTPC_GetResource_cb(impl_POA_NFTPC *servant,
CORBA_long ret,
CORBA_char * resource,
CORBA_long pointer,
CORBA_Environment *ev);
void
impl_NFTPC_SetResource_cb(impl_POA_NFTPC *servant,
CORBA_long ret,
CORBA_long pointer,
CORBA_Environment *ev);
void
impl_NFTPC_GetCurrentBase_cb(impl_POA_NFTPC *servant,
CORBA_long ret,
CORBA_char * base,
CORBA_long pointer,
CORBA_Environment *ev);
void
impl_NFTPC_Create_cb(impl_POA_NFTPC *servant,
CORBA_long ret,
CORBA_long pointer,
CORBA_Environment *ev);
void
impl_NFTPC_Delete_cb(impl_POA_NFTPC *servant,
CORBA_long ret,
CORBA_long pointer,
CORBA_Environment *ev);
void
impl_NFTPC_Move_cb(impl_POA_NFTPC *servant,
CORBA_long ret,
CORBA_long pointer,
CORBA_Environment *ev);
void
impl_NFTPC_Copy_cb(impl_POA_NFTPC *servant,
CORBA_long ret,
CORBA_long pointer,
CORBA_Environment *ev);
void
impl_NFTPC_List_cb(impl_POA_NFTPC *servant,
CORBA_long ret,
InfoList* list,
CORBA_long pointer,
CORBA_Environment *ev);
void
impl_NFTPC_Aliases_cb(impl_POA_NFTPC *servant,
CORBA_long ret,
CORBA_Object loc,
CORBA_long pointer,
CORBA_Environment *ev);
void
impl_NFTPC_Pause_cb(impl_POA_NFTPC *servant,
CORBA_long ret,
CORBA_long pointer,
CORBA_Environment *ev);
void
impl_NFTPC_Resume_cb(impl_POA_NFTPC *servant,
CORBA_long ret,
CORBA_long pointer,
CORBA_Environment *ev);
void
impl_NFTPC_Abort_cb(impl_POA_NFTPC *servant,
CORBA_long ret,
CORBA_long pointer,
CORBA_Environment *ev);
void
impl_NFTPC_NewMetadata_cb(impl_POA_NFTPC *servant,
CORBA_long ret,
Metadata data,
CORBA_long pointer,
CORBA_Environment *ev);
void
impl_NFTPC_NewLocation_cb(impl_POA_NFTPC *servant,
CORBA_long ret,
CORBA_Object loc,
CORBA_long pointer,
CORBA_Environment *ev);
void
impl_NFTPC_Response_cb(impl_POA_NFTPC *servant,
CORBA_long ret,
CORBA_char * resp,
CORBA_long pointer,
CORBA_Environment *ev);


