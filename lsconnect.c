#include "lspeed.h"

extern lsNCS* ncs;

int unshow (GtkWidget *widget, gpointer data)
{ 
  gtk_widget_hide(widget);
  /*
  gtk_signal_emit_stop_by_name (GTK_OBJECT (widget),
				"destroy");
  gtk_signal_connect (GTK_OBJECT (widget), "destroy",
                      GTK_SIGNAL_FUNC (unshow), NULL);
		      */

  return TRUE;
}

void Connect_cb (GtkWidget *widget, lswin* win)
{
  
  Connect_d *connect = win->connect_d;

  gtk_widget_show(win->connect_d->window);

}
 
GtkWidget* label_new(char *label)
{
  
  GtkWidget *widget = gtk_label_new(label);
  gtk_widget_show(widget);

  return widget;
}

GtkWidget* entry_new()
{
  
  GtkWidget *widget = gtk_entry_new();

  gtk_widget_show(widget);

  return widget;
}

GtkWidget* radio_new(GSList **group, char *label)
{
  
  GtkWidget *widget = gtk_radio_button_new_with_label(*group, label);

  *group = gtk_radio_button_group (GTK_RADIO_BUTTON (widget));

  gtk_widget_show(widget);

  return widget;
}

int updatesite(lswin* win)
{
  lsgeneral *general = win->connect_d->general;
  lsstartup *startup = win->connect_d->startup;
  lsadvanced *advanced = win->connect_d->advanced;
  lsgeneral* g = general;
  
  char* port;
  
  GList* sites = win->gopts->sites;
  site* site1 = (site*)win->gopts->sites->data;
  site* cursite = general->curedit;
  
  /*g_free( cursite->name);*/
  cursite->name = g_strdup(general->curtext);
  
  /*g_free( cursite->login);*/
  cursite->login = 
    g_strdup(gtk_entry_get_text(GTK_ENTRY(general->userid)));
  
  /*g_free( cursite->address);*/
  cursite->address = 
    g_strdup(gtk_entry_get_text(GTK_ENTRY(general->host)));
  
  /*g_free( cursite->password);*/
  cursite->password = 
    g_strdup(gtk_entry_get_text(GTK_ENTRY(general->password)));
  
  /*g_free(cursite->initialdir);*/
  cursite->initialdir =  
      g_strdup(gtk_entry_get_text(GTK_ENTRY(startup->remote)));
  if(strlen(cursite->initialdir) < 1)
  {
    cursite->initialdir = g_strdup(".");
  }

  port = 
      gtk_entry_get_text(GTK_ENTRY(advanced->port));
  
  if(strlen(port) < 0)
  {
    cursite->port = atoi(port);
  }
  else
  {
    cursite->port = 21;
  }

  gtk_signal_disconnect(GTK_OBJECT
	  (GTK_COMBO(general->profile)->list), general->c1);
  
  set_popdown(win, general); /*Fix the popdown.*/
  
  g->c1 = gtk_signal_connect(GTK_OBJECT(GTK_COMBO(g->profile)->list),
	  "select_child", GTK_SIGNAL_FUNC (general_cb), win);

  gtk_entry_set_text(GTK_ENTRY(GTK_COMBO(general->profile)->entry), 
	  cursite->name);

}

int delete_cb (GtkWidget *widget, lswin* win)
{
  lsgeneral * general = win->connect_d->general;
  opts* opts = win->gopts;
  site* cursite;
  lsgeneral* g = general;

  opts->sites = g_list_remove(opts->sites, general->curedit);
  
  if(!opts->sites->next)
  {
    new_cb(NULL, win);
  }

  gtk_signal_disconnect(GTK_OBJECT
			(GTK_COMBO(general->profile)->list), general->c1);
  
  set_popdown(win, general); /*Fix the popdown.*/
      
  g->c1 = gtk_signal_connect(GTK_OBJECT(GTK_COMBO(g->profile)->list),
		     "select_child", GTK_SIGNAL_FUNC (general_cb), win);


  gtk_entry_set_text(GTK_ENTRY(GTK_COMBO(general->profile)->entry), 
		     ((site*)opts->sites->next->data)->name);
}

int new_cb (GtkWidget *widget, lswin* win)
{
  lsgeneral * general = win->connect_d->general;
  char *current;
  site* cursite;
  
  opts* opts = win->gopts;

  GList* prolist = win->connect_d->general->prolist;
  GtkWidget *entry = GTK_COMBO(win->connect_d->general->profile)->entry;
  site* newsite = g_new0(site,1);
  GList* sites = opts->sites;
  int siteinc;

  g_list_append(sites, newsite);
  
  opts->totalsites++;
  
  /*updatesite(win, general->curedit);*/

  current = gtk_entry_get_text(GTK_ENTRY(entry));
  
  
  newsite->name = g_strdup("");
  newsite->address = g_strdup("");
  newsite->login = g_strdup("");
  newsite->password = g_strdup("");
  newsite->initialdir = g_strdup(".");
  newsite->hosttype = g_strdup("Unix");
  newsite->comment = g_strdup("NYI");
  
  /*general->prolist = g_list_prepend(general->prolist,"");*/
  general->prolist = NULL;
  
  /*sites = sites->next;*/
  sites = sites->next;
  while(sites)
  {
    /*  win->opts->sites[siteinc].name << endl;*/
    cursite = (site*)sites->data;
    general->prolist = g_list_append(general->prolist, 
				     cursite->name);
    sites = sites->next;
  }
  gtk_combo_set_popdown_strings (GTK_COMBO (general->profile), 
				 general->prolist);

  gtk_entry_set_text(GTK_ENTRY(entry),"");
  /*gtk_entry_set_text(GTK_ENTRY(general->userid), "");*/
  /*gtk_entry_set_text(GTK_ENTRY(general->host), "");*/
  
  /*return TRUE;*/
}

void set_popdown(lswin* win, lsgeneral* general)
{
  opts* opts = win->gopts;
  GList* prolist;
  /*GtkWidget *entry = GTK_COMBO(general->profile)->entry;*/
  GList* sites;
  site* cursite;
  
  general->prolist = NULL; /*memory leak.*/
  sites = win->gopts->sites;
  while(sites)
  {
    cursite = (site*)sites->data;
    general->prolist = g_list_append(general->prolist, cursite->name);
    sites = sites->next;
  }
  
  gtk_combo_set_popdown_strings (GTK_COMBO (general->profile), 
				 general->prolist);
  
}

/*int general_cb (GtkWidget *widget, lswin* win)*/
int general_cb (GtkWidget *widget, gpointer* other, lswin* win)
{
  int modsmade;
  lsgeneral* general = win->connect_d->general;
  lsgeneral* g = general;
  lsstartup* startup = win->connect_d->startup;
  lsadvanced* advanced = win->connect_d->advanced;

  site* curedit = general->curedit;
  GtkWidget *entry = GTK_COMBO(general->profile)->entry;
  char *current = gtk_entry_get_text(GTK_ENTRY(entry));
  GList* sites;
  site* cursite;
  site* tempsite;
  int i, found = FALSE, siteinc;
  
  /*
  gtk_signal_emit_stop_by_name (GTK_OBJECT 
	(GTK_COMBO(general->profile)->list), "select_child");
  */
  
  /*#$%*^(#%$%@#%$@#^%$&^*@#$.  yes this pisses me off*/
  /*the buttfucked combo widget is fucked up beyond belief!*/
  /*Must see if we were already called. [mw]*/
  if(general->keyed == TRUE && other) /*don't do anything here.*/
  {
    general->keyed = FALSE;
    return FALSE;
  }
  if( strcmp(current, general->curtext) == 0  && other)
  {
    return FALSE; /*FALSE because it is truely buttfucked!!!*/
  }


  /*GList* prolist = general->prolist;*/
  
  sites = win->gopts->sites;

  found = FALSE;
  
  /*updatesite(win, general->curedit);*/

  /*sites = sites->next;*/

  while(sites) /*look for the current site.*/
  {
    tempsite = (site*)sites->data;
    if(strcmp(general->curtext, tempsite->name) == 0)
    {
      found = TRUE;
    }
    if(strcmp(tempsite->name, current) == 0)
    {
      cursite = tempsite;
    }
    sites = sites->next;
  }

  curedit->login = 
    g_strdup(gtk_entry_get_text(GTK_ENTRY(general->userid)));
  
  curedit->address = 
    g_strdup(gtk_entry_get_text(GTK_ENTRY(general->host)));
  
  curedit->password = 
    g_strdup(gtk_entry_get_text(GTK_ENTRY(general->password)));

  curedit->initialdir = 
    g_strdup(gtk_entry_get_text(GTK_ENTRY(startup->remote)));
  
  if(!found)
  {
    curedit->name = g_strdup(general->curtext);
    
    
    gtk_signal_disconnect(GTK_OBJECT
			  (GTK_COMBO(general->profile)->list), general->c1);
    
    set_popdown(win, general); /*Fix the popdown.*/
      
    g->c1 = gtk_signal_connect(GTK_OBJECT(GTK_COMBO(g->profile)->list),
		     "select_child", GTK_SIGNAL_FUNC (general_cb), win);
  }
    
  gtk_entry_set_text(GTK_ENTRY(GTK_COMBO(general->profile)->entry), 
		     cursite->name);
  
  if(cursite->address)
    gtk_entry_set_text(GTK_ENTRY(general->host), 
		       cursite->address);
  if(cursite->login)
    gtk_entry_set_text(GTK_ENTRY(general->userid), 
		       cursite->login);
  if(cursite->password)
    gtk_entry_set_text(GTK_ENTRY(general->password),
		       cursite->password);

  if(cursite->initialdir)
    gtk_entry_set_text(GTK_ENTRY(startup->remote),
		       cursite->initialdir);

  
  general->curedit = cursite;  /*The one the user selected.*/
  
  general->curtext = g_strdup(cursite->name);
  
  return FALSE;
}

static gint
passwd_key_press (GtkWidget   *widget,
		 GdkEventKey *event,
		 gpointer     user_data)
{
}

static gint
name_key_press (GtkWidget   *widget,
		 GdkEventKey *event,
		 gpointer     user_data)
{
  lswin* win = (lswin*) user_data;
  lsgeneral* general = win->connect_d->general;
  
  GtkWidget *entry = GTK_COMBO(general->profile)->entry;
  char *current = gtk_entry_get_text(GTK_ENTRY(entry));

  general->curtext = g_strdup(current);
  /*
  gtk_signal_emit_stop_by_name (GTK_OBJECT 
	(GTK_COMBO(general->profile)->entry), "key_press_event");
  */
  
  general->keyed = TRUE;
  return  FALSE;
}

#if 0 /*Using libPropList now*/
void backup_sites(lswin* win)
{
  opts* opts = win->gopts;
  GList* sites;

  win->backup = NULL; /*memory leak.*/
  sites = opts->sites;
  while(sites)
  {
    site* cursite = (site*)sites->data;
    site* newsite = g_new0(site,1);

    newsite->name = g_strdup(cursite->name);
    newsite->address = g_strdup(cursite->address);
    newsite->login = g_strdup(cursite->login);
    newsite->password = g_strdup(cursite->password);
    newsite->initialdir = g_strdup(cursite->initialdir);
    newsite->port = cursite->port;
    newsite->goodspeed = cursite->goodspeed;

    newsite->totalbytesin = cursite->totalbytesin;
    newsite->totalbytesout = cursite->totalbytesout;
    newsite->totaltimexfer = cursite->totaltimexfer;
    newsite->filesdownloaded = cursite->filesdownloaded;

    opts->backup = g_list_append(opts->backup, newsite);
    sites = sites->next;
  }
  
}
#endif /*0*/

lsgeneral* new_lsgeneral(lswin* win)
{
  int i;
  lsgeneral *general;
  lsgeneral *g;
  site* cursite = (site*)win->gopts->sites->data;

  general = g_new0(lsgeneral,1);
  g = general;

  g->General = label_new("General");
  g->Profile = label_new("Profile:");
  g->profile = combo_new();
  g->Host = label_new("Host:");
  g->host  = entry_new();
  g->Type = label_new("Type:");
  g->type = combo_new();
  gtk_entry_set_editable(GTK_ENTRY(GTK_COMBO(g->type)->entry), FALSE);

  g->Userid = label_new("UserID:");
  g->userid = entry_new();

  g->Password = label_new("Password:");
  g->password = entry_new();
  gtk_entry_set_visibility(GTK_ENTRY(g->password), FALSE);

  g->Account = label_new("Account:");
  g->account = entry_new();
  g->Comment = label_new("Comment:");
  g->comment = entry_new();
  g->New = button_new("New");
  g->Delete = button_new("Delete");
  g->Anonymous = check_new("Anonymous");
  g->SavePasswd = check_new("Save Password");

  g->prolist = NULL;
  set_popdown(win, general);
  
  g->typelist = NULL;
  for(i=0; i < HOSTTYPES; i++)
  {
    g->typelist = g_list_append(g->typelist, g_strdup(hosttypes[i]));
    gtk_combo_set_popdown_strings (GTK_COMBO (g->type), 
				   g->typelist);
  }
  gtk_entry_set_text(GTK_ENTRY(g->host), cursite->address);
  gtk_entry_set_text(GTK_ENTRY(g->userid), cursite->login);
  /*gtk_entry_set_text(GTK_ENTRY(g->password), win->gopts->sites[2].password);*/
  g->curedit = cursite;
  g->curtext = g_strdup(cursite->name);

  /*
  g->c1 = gtk_signal_connect(GTK_OBJECT(GTK_COMBO(g->profile)->list),
		    "selection_changed", GTK_SIGNAL_FUNC (general_cb), win);
  g->c1 = gtk_signal_connect(GTK_OBJECT(GTK_COMBO(g->profile)->entry),
		     "changed", GTK_SIGNAL_FUNC (general_cb), win);
  */
  g->c1 = gtk_signal_connect(GTK_OBJECT(GTK_COMBO(g->profile)->list),
		     "select_child", GTK_SIGNAL_FUNC (general_cb), win);
  /*
  g->c1 = gtk_signal_connect(GTK_OBJECT(GTK_COMBO(g->profile)->list),
		     "selection_changed", GTK_SIGNAL_FUNC (general_cb), win);
  */
  gtk_signal_connect_after (GTK_OBJECT 
		      (GTK_COMBO(g->profile)->entry), "key_press_event",
		      (GtkSignalFunc) name_key_press, win); 

  gtk_signal_connect (GTK_OBJECT 
		      (g->password), "key_press_event",
		      (GtkSignalFunc) passwd_key_press, win);
  
  gtk_signal_connect (GTK_OBJECT (g->New), "clicked",
                      GTK_SIGNAL_FUNC (new_cb), win);
  
  gtk_signal_connect (GTK_OBJECT (g->Delete), "clicked",
                      GTK_SIGNAL_FUNC (delete_cb), win);
  
  g->table = gtk_table_new(7, 3, FALSE);
  gtk_table_set_col_spacing(GTK_TABLE(g->table), 1, 15);
  gtk_table_set_col_spacing(GTK_TABLE(g->table), 0, 15);

  g->box = gtk_hbox_new(FALSE, 0);
  g->vbox = gtk_vbox_new(FALSE, 0);
  /*g->hbox = gtk_new_vbox(FALSE, 0);*/

  gtk_box_pack_start(GTK_BOX(g->box), g->vbox, TRUE, FALSE, 15);
  gtk_box_pack_start(GTK_BOX(g->vbox), g->table, TRUE, TRUE, 15);

  gtk_table_attach_defaults (GTK_TABLE(g->table), g->Profile, 0, 1, 0, 1);
  gtk_table_attach_defaults (GTK_TABLE(g->table), g->profile, 1, 2, 0, 1);
  gtk_table_attach_defaults (GTK_TABLE(g->table), g->New,     2, 3, 0, 1);

  gtk_table_attach_defaults (GTK_TABLE(g->table), g->Host,   0, 1, 1, 2);
  gtk_table_attach_defaults (GTK_TABLE(g->table), g->host,   1, 2, 1, 2);
  gtk_table_attach_defaults (GTK_TABLE(g->table), g->Delete, 2, 3, 1, 2);

  gtk_table_attach_defaults (GTK_TABLE(g->table), g->Type,   0, 1, 2, 3);
  gtk_table_attach_defaults (GTK_TABLE(g->table), g->type,   1, 2, 2, 3);

  gtk_table_attach_defaults (GTK_TABLE(g->table), g->Userid,    0, 1, 3, 4);
  gtk_table_attach_defaults (GTK_TABLE(g->table), g->userid,    1, 2, 3, 4);
  gtk_table_attach_defaults (GTK_TABLE(g->table), g->Anonymous, 2, 3, 3, 4);


  gtk_table_attach_defaults (GTK_TABLE(g->table), g->Password,   0, 1, 4, 5);
  gtk_table_attach_defaults (GTK_TABLE(g->table), g->password,   1, 2, 4, 5);
  gtk_table_attach_defaults (GTK_TABLE(g->table), g->SavePasswd, 2, 3, 4, 5);

  gtk_table_attach_defaults (GTK_TABLE(g->table), g->Account,   0, 1, 5, 6);
  gtk_table_attach_defaults (GTK_TABLE(g->table), g->account,   1, 2, 5, 6);

  gtk_table_attach_defaults (GTK_TABLE(g->table), g->Comment,   0, 1, 6, 7);
  gtk_table_attach_defaults (GTK_TABLE(g->table), g->comment,   1, 3, 6, 7);


  gtk_widget_show(g->box);
  gtk_widget_show(g->table);
  gtk_widget_show(g->vbox);


  return general;
}

lsstartup* new_lsstartup()
{
  lsstartup *startup;
  lsstartup *s;

  startup = g_new(lsstartup,1);
  s = startup;

  s->Startup = label_new("Startup");

  s->Remote = label_new("Initial Remote Directory:");
  s->remote = entry_new();

  s->Local = label_new("Initial Local Directory:");
  s->local = entry_new();

  s->Commands = label_new("Initial Command(s).  "
			  "[Use `;'s to separate cammands.]:");
  s->commands = entry_new();

  s->RemoteMask = label_new("Remote File Mask:");
  s->remoteMask = entry_new();
  gtk_widget_set_usize(s->remoteMask, 20, -1);

  s->LocalMask = label_new("Local File Mask:");
  s->localMask = entry_new(); 
  gtk_widget_set_usize(s->localMask, 20, -1);

  s->TimeOffset = label_new("Time Offset in Hours:");
  s->timeOffset = entry_new();
  gtk_widget_set_usize(s->timeOffset, 20, -1);

  s->table = gtk_table_new(8, 6, FALSE);
  /*gtk_table_set_col_spacing(GTK_TABLE(s->table), 1, 15);*/
  /*gtk_table_set_col_spacing(GTK_TABLE(s->table), 0, 15);*/

  s->box = gtk_hbox_new(FALSE, 0);
  /*s->vbox = gtk_vbox_new(FALSE, 0);*/
  /*g->hbox = gtk_new_vbox(FALSE, 0);*/

  gtk_box_pack_start(GTK_BOX(s->box), s->table, TRUE, TRUE, 15);

  gtk_table_attach_defaults (GTK_TABLE(s->table), s->Remote, 0, 6, 0, 1);
  gtk_table_attach_defaults (GTK_TABLE(s->table), s->remote, 0, 6, 1, 2);

  gtk_table_attach_defaults (GTK_TABLE(s->table), s->Local, 0, 6, 2, 3);
  gtk_table_attach_defaults (GTK_TABLE(s->table), s->local, 0, 6, 3, 4);

  gtk_table_attach_defaults (GTK_TABLE(s->table), s->Commands, 0, 6, 4, 5);
  gtk_table_attach_defaults (GTK_TABLE(s->table), s->commands, 0, 6, 5, 6);

  gtk_table_attach_defaults (GTK_TABLE(s->table), s->RemoteMask, 0, 2, 6, 7);
  gtk_table_attach_defaults (GTK_TABLE(s->table), s->remoteMask, 2, 3, 6, 7);

  gtk_table_attach_defaults (GTK_TABLE(s->table), s->LocalMask, 0, 2, 7, 8);
  gtk_table_attach_defaults (GTK_TABLE(s->table), s->localMask, 2, 3, 7, 8);

  gtk_table_attach_defaults (GTK_TABLE(s->table), s->TimeOffset, 3, 5, 6, 7);
  gtk_table_attach_defaults (GTK_TABLE(s->table), s->timeOffset, 5, 6, 6, 7);

  gtk_widget_show(s->box);
  gtk_widget_show(s->table);


  return startup;
}

lsadvanced* new_lsadvanced()
{
  lsadvanced *advanced;
  lsadvanced *a;

  advanced = g_new(lsadvanced,1);
  a = advanced;

  a->Advanced = label_new("Advanced");

  a->Retry = label_new("Connection Retry:");
  a->retry = entry_new();
  gtk_widget_set_usize(a->retry, 50, -1);
  a->retryi = label_new("Number of failed attempts.");

  a->Timeout = label_new("Network Timeout:");
  a->timeout = entry_new();
  gtk_widget_set_usize(a->timeout, 50, -1);
  a->timeouti = label_new("Number of seconds idle.");

  a->Port = label_new("Remote Port:");
  a->port = entry_new();
  gtk_widget_set_usize(a->port, 50, -1);
  a->porti = label_new("TCP port of FPT server.");

  a->Passive = check_new("Passive Transfer");
  a->passivei = label_new("Use with Firewall or Gateway. [Maybe.]");

  a->table = gtk_table_new(4, 4, FALSE);
  /*gtk_table_set_col_spacing(GTK_TABLE(s->table), 1, 15);*/
  /*gtk_table_set_col_spacing(GTK_TABLE(s->table), 0, 15);*/

  a->box = gtk_hbox_new(FALSE, 0);
  /*s->vbox = gtk_vbox_new(FALSE, 0);*/
  /*g->hbox = gtk_new_vbox(FALSE, 0);*/

  gtk_box_pack_start(GTK_BOX(a->box), a->table, FALSE, FALSE, 15);

  gtk_table_attach_defaults (GTK_TABLE(a->table), a->Retry, 0, 1, 0, 1);
  gtk_table_attach_defaults (GTK_TABLE(a->table), a->retry, 1, 2, 0, 1);
  gtk_table_attach_defaults (GTK_TABLE(a->table), a->retryi, 2, 4, 0, 1);

  gtk_table_attach_defaults (GTK_TABLE(a->table), a->Timeout, 0, 1, 1, 2);
  gtk_table_attach_defaults (GTK_TABLE(a->table), a->timeout, 1, 2, 1, 2);
  gtk_table_attach_defaults (GTK_TABLE(a->table), a->timeouti, 2, 4, 1, 2);

  gtk_table_attach_defaults (GTK_TABLE(a->table), a->Port, 0, 1, 2, 3);
  gtk_table_attach_defaults (GTK_TABLE(a->table), a->port, 1, 2, 2, 3);
  gtk_table_attach_defaults (GTK_TABLE(a->table), a->porti, 2, 4, 2, 3);

  gtk_table_attach_defaults (GTK_TABLE(a->table), a->Passive, 0, 2, 3, 4);
  gtk_table_attach_defaults (GTK_TABLE(a->table), a->passivei, 2, 4, 3, 4);

  gtk_widget_show(a->box);
  gtk_widget_show(a->table);


  return advanced;
}

lsfirewall* new_lsfirewall()
{
  lsfirewall *firewall;
  lsfirewall *f;

  firewall = g_new0(lsfirewall,1);
  f = firewall;

  f->Firewall = label_new("Firewall");

  f->Host = label_new("Host:");
  f->host = entry_new();

  f->UseFirewall = check_new("Use Firewall");

  f->Userid = label_new("User ID:");
  f->userid = entry_new();

  f->Password = label_new("Password:");
  f->password = entry_new();

  f->SavePasswd = check_new("Save Password");

  f->Port = label_new("Port:");
  f->port = entry_new();
  gtk_widget_set_usize(f->port, 50, -1);

  f->SITEhostname = gtk_radio_button_new_with_label (NULL, "SITE Hostname");
  gtk_widget_show(f->SITEhostname);

  f->group = gtk_radio_button_group (GTK_RADIO_BUTTON (f->SITEhostname)); 

  f->USERafter = radio_new(&f->group, "USER after Login");
  f->ProxyOPEN = radio_new(&f->group, "Proxy OPEN");
  f->Transparent = radio_new(&f->group, "Transparent");
  f->USERnolog = radio_new(&f->group, "USER with no Login");
  f->USERfidat = radio_new(&f->group, "USER FireID@Host");
  f->USERremote = radio_new(&f->group, "USER Remote@Host FireID");
  f->USERatat = radio_new(&f->group, "USER Remote@FireID@Host");
  
  f->toptable = gtk_table_new(4, 3, FALSE);
  f->btable = gtk_table_new(4, 3, FALSE);

  f->frame = gtk_frame_new("Firewall Type");
  /*gtk_table_set_col_spacing(GTK_TABLE(s->table), 1, 15);*/
  /*gtk_table_set_col_spacing(GTK_TABLE(s->table), 0, 15);*/

  f->vbox = gtk_vbox_new(FALSE, 0);
  /*s->vbox = gtk_vbox_new(FALSE, 0);*/
  /*g->hbox = gtk_new_vbox(FALSE, 0);*/

  gtk_container_border_width (GTK_CONTAINER (f->vbox), 8);
  gtk_box_pack_start(GTK_BOX(f->vbox), f->toptable, FALSE, FALSE, 4);
  gtk_box_pack_start(GTK_BOX(f->vbox), f->frame, FALSE, FALSE, 4);
  gtk_container_add (GTK_CONTAINER (f->frame), f->btable);

  gtk_table_attach_defaults (GTK_TABLE(f->toptable), f->Host, 0, 1, 0, 1);
  gtk_table_attach_defaults (GTK_TABLE(f->toptable), f->host, 1, 2, 0, 1);
  gtk_table_attach_defaults (GTK_TABLE(f->toptable),f->UseFirewall,2,3,0,1);

  gtk_table_attach_defaults (GTK_TABLE(f->toptable), f->Userid, 0, 1, 1, 2);
  gtk_table_attach_defaults (GTK_TABLE(f->toptable), f->userid, 1, 2, 1, 2);

  gtk_table_attach_defaults (GTK_TABLE(f->toptable), f->Password, 0, 1, 2, 3);
  gtk_table_attach_defaults (GTK_TABLE(f->toptable), f->password, 1, 2, 2, 3);
  gtk_table_attach_defaults (GTK_TABLE(f->toptable),f->SavePasswd,2,3,2,3);

  gtk_table_attach_defaults (GTK_TABLE(f->toptable), f->Port, 0, 1, 3, 4);
  gtk_table_attach_defaults (GTK_TABLE(f->toptable), f->port, 1, 2, 3, 4);
  
  /*Now the bottom table*/
  gtk_table_attach_defaults (GTK_TABLE(f->btable), f->SITEhostname,0,1,0,1);
  gtk_table_attach_defaults (GTK_TABLE(f->btable), f->USERafter,0,1,1,2);
  gtk_table_attach_defaults (GTK_TABLE(f->btable),f->ProxyOPEN,0,1,2,3);
  gtk_table_attach_defaults (GTK_TABLE(f->btable),f->Transparent,0,1,3,4);

  gtk_table_attach_defaults (GTK_TABLE(f->btable), f->USERnolog,1,2,0,1);
  gtk_table_attach_defaults (GTK_TABLE(f->btable), f->USERfidat,1,2,1,2);
  gtk_table_attach_defaults (GTK_TABLE(f->btable),f->USERremote,1,2,2,3);
  gtk_table_attach_defaults (GTK_TABLE(f->btable),f->USERatat,1,2,3,4);

  gtk_widget_show(f->frame);
  gtk_widget_show(f->vbox);
  gtk_widget_show(f->toptable);
  gtk_widget_show(f->btable);


  return firewall;
}

int connectCancel_cb (GtkWidget *widget, lswin* win)
{
  opts* opts = win->gopts;
  site* cursite;
  lsgeneral* general = win->connect_d->general;
  lsgeneral* g = general;

  /*reload .litespeedrc*/
  
  gtk_signal_disconnect(GTK_OBJECT
			(GTK_COMBO(general->profile)->list), general->c1);
  
  set_popdown(win, general); /*Fix the popdown.*/
  
  g->c1 = gtk_signal_connect(GTK_OBJECT(GTK_COMBO(g->profile)->list),
		     "select_child", GTK_SIGNAL_FUNC (general_cb), win);
  
  gtk_entry_set_text(GTK_ENTRY(GTK_COMBO(general->profile)->entry), 
		     ((site*)opts->sites->data)->name);
		     
  gtk_widget_hide(win->connect_d->window);
}

int connectOKed_cb (GtkWidget *widget, lswin *win)
{
  opts* opts = win->gopts;
  GList* sites = opts->sites;
  site* cursite;
  lsaction* action;
  lssystem* sys = win->remote;
  char *current;
  
  /*connetion*/
  n_action* open_conn;


  int i, found = FALSE;
  int ret=0; /*Return value of connection based code.*/

  updatesite(win);
  /*write options to disk*/

  ls_write_config(win->gopts, ".litespeedrc");

  /*get ready to connect to host.*/


  current = gtk_entry_get_text
    (GTK_ENTRY(GTK_COMBO(win->connect_d->general->profile)->entry));

  fprintf(stderr, "current = %s\n", current);
  /*sites = sites->next;*/
  while(sites)
  {
    cursite = (site*)sites->data;
    if(strcmp(cursite->name, current) == 0)
    {
      found = TRUE;
      break;
    }
    sites = sites->next;
  }
  cursite = (site*)sites->data;

  sys->url = URL__alloc();
  sys->url->url = g_strdup("ftp://");
  sys->url->host = g_strdup(cursite->address);
  sys->url->user = g_strdup(cursite->login);
  sys->url->password = g_strdup(cursite->password);
  sys->url->resource = g_strdup(cursite->initialdir);
  sys->url->proto = g_strdup("ftp");
  sys->url->name = g_strdup("");
  sys->url->port = 21;
  
  win->remotecwd = g_strdup(cursite->initialdir);

  fprintf(stderr, "url = %s\n", sys->url->url);
  fprintf(stderr, "host = %s\n", sys->url->host);
  fprintf(stderr, "user = %s\n", sys->url->user);
  fprintf(stderr, "password = %s\n", sys->url->password);
  fprintf(stderr, "resource = %s\n", sys->url->resource);
  fprintf(stderr, "proto = %s\n", sys->url->proto);
  fprintf(stderr, "name = %s\n", sys->url->name);

  /*
  sys->url->url = g_strdup("ftp://");
  sys->url->proto = g_strdup("ftp");
  sys->url->user = g_strdup("test");
  sys->url->password = g_strdup("testme");
  sys->url->host = g_strdup("thunder.cgibuilder.com");
  sys->url->resource = g_strdup("/home/");
  sys->url->name = g_strdup("");
  */
  sys->url->port = 21;

  /*sys->url->port = cursite->port;*/

  action = g_new0(lsaction, 1);

  action->win = win;
  action->sys = sys;
  action->action = LA_LIST;

  /*Build the new operation object*/
  open_conn = n_connection_open_new(ncs->NCS, win->nftpc->NFTPC,
	  sys->url, "passwd");

  /*Connect up to the signals of interest*/
  n_signal_connect(open_conn, "done",
	  N_SIGNAL_FUNC(ls_nftpc_open_cb), action);
  /*
     n_signal_connect(open_conn, N_SIG_ERROR, 
     N_SIGNAL_FUNC(ls_open_error_cb), action);
   */

  /*start the operation*/
  n_action_activate(open_conn);
	      
  /*
  NCS_Connection_Open(ncs->NCS, win->nftpc->NFTPC, sys->url, "passwd",
	  action, &sys->filer_ev);
   */

  /*NCS_Loc_Close(sys->lister, action, &sys->filer_ev);*/
  
  gtk_widget_hide(win->connect_d->window);
}

Connect_d * new_Connect_d(lswin * win)
{
  Connect_d* connect_d;
  Connect_d* c;

  connect_d = g_new0(Connect_d,1);
  c = connect_d;
  
  c->window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  
  gtk_container_border_width (GTK_CONTAINER (c->window), 15);

  c->general = new_lsgeneral(win);
  c->startup = new_lsstartup();
  c->advanced = new_lsadvanced();
  c->firewall = new_lsfirewall();

  c->OK = button_new("OK");
  c->Cancel = button_new("Cancel");
  c->Apply = button_new("Apply");
  c->Help = button_new("Help");
  
  c->hbox = gtk_hbox_new(TRUE, 0);
  gtk_widget_show(c->hbox);
  c->vbox = gtk_vbox_new(FALSE, 8);
  gtk_widget_show(c->vbox);

  c->Notebook = gtk_notebook_new();
  
  gtk_container_add (GTK_CONTAINER (c->window), c->vbox);
  
  gtk_box_pack_start(GTK_BOX(c->vbox), c->Notebook, FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX(c->vbox), c->hbox, FALSE, FALSE, 0);

  gtk_box_pack_start(GTK_BOX(c->hbox), c->OK, FALSE, TRUE, 8);
  gtk_box_pack_start(GTK_BOX(c->hbox), c->Cancel, FALSE, TRUE, 8);
  gtk_box_pack_start(GTK_BOX(c->hbox), c->Apply, FALSE, TRUE, 8);
  gtk_box_pack_start(GTK_BOX(c->hbox), c->Help, FALSE, TRUE, 8);

  gtk_widget_show(c->Notebook);

  gtk_notebook_append_page(GTK_NOTEBOOK(c->Notebook), 
	  c->general->box, c->general->General);
  gtk_notebook_append_page(GTK_NOTEBOOK(c->Notebook),
	  c->startup->box, c->startup->Startup);
  gtk_notebook_append_page(GTK_NOTEBOOK(c->Notebook), 
	  c->advanced->box,c->advanced->Advanced);

  /*
  gtk_notebook_append_page(c->notebook, c->advanced->box,c->advanced->Advanced);
  */

  gtk_signal_connect (GTK_OBJECT (c->window), "delete_event",
                      GTK_SIGNAL_FUNC (unshow), NULL);
  
  gtk_signal_connect (GTK_OBJECT (c->OK), "clicked",
                        GTK_SIGNAL_FUNC (connectOKed_cb), win);

  gtk_signal_connect (GTK_OBJECT (c->Cancel), "clicked",
                        GTK_SIGNAL_FUNC (connectCancel_cb), win);

  return connect_d;
}

