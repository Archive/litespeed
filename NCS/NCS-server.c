#include "../N/support/CS.h"
#include <orb/orbit.h>
#include <libgnorba/gnorba.h>
#include <stdio.h>
#include <sys/errno.h>
#include <string.h>
#include "NCS.h"
#include "NCS-impl.h"
#include "NCS-server.h"

NCS_Connection NCS = CORBA_OBJECT_NIL;

CORBA_ORB orb;
PortableServer_POA poa;
CORBA_Environment ev;

GHashTable* proto_hash;
NCS_Connection ftp_conn;  /*FIXME: something wrong with the hashtable*/
NCS_Connection file_conn;  /*FIXME: something wrong with the hashtable*/

CORBA_Object name_service;

typedef void* (*PT_FUNC) (void*);

int main(int argc, char** argv)
{
  char *retval;
  int host_ior_fd;

  char *name = "NCS";                     
  int n = 1;                     
  
  IIOPAddConnectionHandler = orb_add_connection;
  IIOPRemoveConnectionHandler = orb_remove_connection;


  proto_hash = g_hash_table_new(g_int_hash, g_int_equal);

  /*FIXME: Register myself as "NCS"*/
  CORBA_exception_init(&ev);
  /*
  orb = gnome_CORBA_init("NCS", "1.0", &argc, argv,
	  GNORBA_INIT_SERVER_FUNC, &ev);
  */

  orb = CORBA_ORB_init(&n, &name /* dummy */, "orbit-local-orb", &ev);


  poa = (PortableServer_POA)CORBA_ORB_resolve_initial_references(orb, 
	  "RootPOA", &ev);
  PortableServer_POAManager_activate(
	  PortableServer_POA__get_the_POAManager(poa, &ev), &ev);

  NCS = impl_NCS_Connection__create(poa,  &ev);
  printf("NCS = %i\n", NCS);

  retval = CORBA_ORB_object_to_string(orb, NCS , &ev);
  
  host_ior_fd = open(ncs_get_default_ior_filename(), O_WRONLY|O_CREAT, 0600);
  write(host_ior_fd, retval, strlen(retval) );
  close(host_ior_fd);

  /*CORBA_ORB_run(orb, &ev);*/
  gtk_main ();
  gdk_exit (0);


  return 0;
}

/**
 * impl_NCS_Connection_Open:
 * @passwd: Authenticate the user proccess on this key.
 * @URL: URL that the program thinks it wants.
 * @loc: Have an ORB build this so it can be returned.
 *
 * Create a socket object that can be used in susequent
 * requests of the server.
 *
 * If the protocol is handled by a different ORB, connect
 * to that ORB and use NCS_Connection_Open to get the 
 * socket.
 *
 * This ORB handles ALL requests, but only services "file"
 * requests.
 *
 * We need to authenticate the user, before handing out a
 * socket.
 *
 * returns: 0 on success else -1 on error
 */
void
ncs_pt_connection_open(nc_connection_open_pt_st *connection_open_pt_st)
{
  impl_POA_NCS_Loc *newservant;
  PortableServer_ObjectId *objid;
  NCS_Connection conn;
  impl_POA_NCS_Connection *servant;
  CORBA_Object handle;
  URL* url;
  CORBA_char * passwd;
  CORBA_long pointer;
  CORBA_Environment ev;
  int urlok = 1; /*Good for now.*/

  fprintf(stderr, "in ncs_pt_connection_open\n");

  servant = connection_open_pt_st->servant;
  handle = connection_open_pt_st->handle;
  url = connection_open_pt_st->url;
  passwd = connection_open_pt_st->passwd;
  pointer = connection_open_pt_st->pointer;
  /*ev = connection_open_pt_st->ev;*/


  /*CS_parse_url(url);*/

  fprintf(stderr, "url->url = %s\n", url->url);
  fprintf(stderr, "url->resource = %s\n", url->resource);
  fprintf(stderr, "url->proto = %s\n", url->proto);
  fprintf(stderr, "url->host = %s\n", url->host);
  fprintf(stderr, "url->port = %i\n", url->port);
  fprintf(stderr, "url->password = %s\n", url->password);

  /*We can't, maybe someone else can?*/
  if( (conn = g_hash_table_lookup(proto_hash, url->proto)) != NULL )
  {
    fprintf(stderr, "Calling: %i\n", conn);
    NCS_Connection_Open(conn, handle, url, "passwd", pointer, &ev);
  }
  else /*URL->proto doesn't exist in any of the running ORBs*/
  {
    if(!strcmp(url->proto, "ftp"))
    {
      conn = ftp_conn;
    }
    if(!strcmp(url->proto, "file"))
    {
      conn = file_conn;
    }
  
    fprintf(stderr, "making the call to open the connection\n", conn);
    NCS_Connection_Open(conn, handle, url, "passwd", pointer, &ev);
    fprintf(stderr, "done making the call to open the connection\n", conn);
  }
}

void 
impl_NCS_Connection_Open(impl_POA_NCS_Connection *servant,
	CORBA_Object handle, 
	URL* url,
	CORBA_char * passwd,
	CORBA_long pointer,
	CORBA_Environment *ev)
{
  pthread_t* threadptr;
  pthread_attr_t d_attr;
  nc_connection_open_pt_st *connection_open_pt_st;
  
  fprintf(stderr, "in impl_NCS_Connection_Open\n");
  threadptr = g_new0(pthread_t, 1);

  connection_open_pt_st = g_new0(nc_connection_open_pt_st, 1);
  connection_open_pt_st->url = URL__alloc();

  fprintf(stderr, "url->url = %s\n", url->url);
  fprintf(stderr, "url->resource = %s\n", url->resource);
  fprintf(stderr, "url->proto = %s\n", url->proto);
  fprintf(stderr, "url->host = %s\n", url->host);
  fprintf(stderr, "url->port = %i\n", url->port);
  fprintf(stderr, "url->password = %s\n", url->password);
  fflush(stderr);

  connection_open_pt_st->servant = servant;
  connection_open_pt_st->handle = handle;
  connection_open_pt_st->url = clone_url(url);
  connection_open_pt_st->passwd = passwd;
  connection_open_pt_st->pointer = pointer;
  connection_open_pt_st->ev = ev;


  /*pthread_attr_setdetachstate(&d_attr, PTHREAD_CREATE_DETACHED);
  pthread_create(threadptr, &d_attr,
	  (PT_FUNC)ncs_pt_connection_open,
	  (void*) connection_open_pt_st);
   */

  pthread_create(threadptr, NULL,
	  (PT_FUNC)ncs_pt_connection_open,
	  (void*) connection_open_pt_st);
  
  fprintf(stderr, "ending impl_NCS_Connection_Open\n");

  /*Generate and return SN*/
}

/**
 * impl_NCS_Connection_Register:
 * @conn: a reference to an ORB.
 * @proto: the protocol the the ORB services.
 *
 * Store the @conn in the protos hash liked off the 
 * @proto that ORB services.
 *
 * returns: 0 for success and -1 for error.
 */
void 
impl_NCS_Connection_Register(impl_POA_NCS_Connection *servant,
	NCS_Connection conn,
	CORBA_char * proto,                
	CORBA_char * passwd,               
	CORBA_Environment *ev) 
{
  CORBA_long      retval;

  /*FIXME: Authenticate the calling server.*/

  fprintf(stderr, "Registering %s.\n", proto);

  g_hash_table_insert(proto_hash, proto, conn);

  if(!strcmp(proto, "ftp"))
  {
    ftp_conn = conn;
  }
  if(!strcmp(proto, "file"))
  {
    file_conn = conn;
  }
  
  return retval;
}

