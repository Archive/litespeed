#!/usr/bin/perl

##Make sure we start with fresh copy of the impl.c file.

print `orbit-idl -Eskeleton_impl ../N/NCS.idl`;

open (IMPL_H, ">NCS-impl.h");
open (IMPL_C, "<NCS-impl.c");
open (TEMP_IMPL_C, ">TEMP-impl.c");

$in_impl = 0;
$here = 0;

print TEMP_IMPL_C "
#include \"NCS-impl.h\"
                  ";


while( <IMPL_C> )
{
  s/static//g;

  if(/Stub implementations/)
  {
    $in_impl = 1;
  }

  if(!$in_impl)
  {

    if(/App-specific servant structures/)
    {
      print IMPL_H "
#include <glib.h>
#include <pthread.h>
                        ";
    }

    if(//)
    {
      print IMPL_H "
                       ";
     $here = 1;
    }
    print IMPL_H $_;
  }
  else
  {
    print TEMP_IMPL_C $_;
  }
}

`mv TEMP-impl.c NCS-impl.c`;
