#ifndef NCS_SERVER_H
#define NCS_SERVER_H

/*
 * This code it public domain. Matt Wimer thinks he has the
 * credit of creating it.  It can be used any way you want, but
 * Matt Wimer is not responsible for your actions.
 * 
 * If you feel better with GPL code then this code is GPL as well.
 * copyright 1999 developers
 */

typedef struct _nc_connection_open_pt_st nc_connection_open_pt_st;

struct _nc_connection_open_pt_st
{
  impl_POA_NCS_Connection *servant;
  CORBA_Object handle;
  URL* url;
  CORBA_char *passwd;
  CORBA_long pointer;
  CORBA_Environment *ev;
};

#endif /*NCS_SERVER_H*/
