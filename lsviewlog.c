#include "lspeed.h"


ViewLog_d* new_ViewLog_d(void)
{
  ViewLog_d* viewlog_d;
  ViewLog_d* v;

  viewlog_d = g_new0(ViewLog_d,1);
  v = viewlog_d; 

  v->window = gnome_dialog_new(N_("Litespeed Log"),
                               GNOME_STOCK_BUTTON_CLOSE,
			       NULL);
  gnome_dialog_close_hides(GNOME_DIALOG(v->window), TRUE);
  
  gnome_dialog_button_connect_object(GNOME_DIALOG(v->window), 0, gtk_widget_hide, GTK_OBJECT(v->window));

  gtk_container_border_width (GTK_CONTAINER (v->window), 15);
  gtk_widget_set_usize(v->window, 500, 300);

  v->text = gtk_text_new (NULL, NULL);
  gtk_text_set_editable(GTK_TEXT(v->text),FALSE);
  v->sbar = gtk_vscrollbar_new (GTK_TEXT (v->text)->vadj); 
  v->table = gtk_table_new(1, 2, FALSE);

  gtk_table_attach (GTK_TABLE (v->table), v->sbar, 1, 2, 0, 1,
                    GTK_FILL, GTK_EXPAND | GTK_SHRINK | GTK_FILL, 0, 0);      
  gtk_table_attach (GTK_TABLE (v->table), v->text, 0, 1, 0, 1,
                    GTK_EXPAND | GTK_SHRINK | GTK_FILL,
                    GTK_EXPAND | GTK_SHRINK | GTK_FILL, 0, 0); 

  /*gtk_container_add (GTK_CONTAINER (v->window), v->table);*/
  gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(v->window)->vbox), v->table, TRUE, TRUE, 1);

  gtk_widget_show(v->text);
  gtk_widget_realize(v->text);
  gtk_widget_show(v->sbar);
  gtk_widget_show(v->table);
  
  return v;
}

void ViewLog_cb (GtkWidget *widget, lswin* win)
{
  /*ViewLog_d *viewlog = win->viewlog_d;*/

  gtk_widget_show(win->viewlog_d->window);
}
