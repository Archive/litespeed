#include "NCC-actions.h"
#include <stdio.h>

/*
   g_hash_table_new(NULL, g_direct_equal);
 */

n_action*
n_connection_open_new(NCS_Connection obj,
	const CORBA_Object handle, const URL* url, const CORBA_char * passwd)
{
  n_connection_open_action* action;

  action = g_new0(n_connection_open_action, 1);

  action->type = N_CONNECTION_OPEN;
  action->signals = NULL;
  action->obj = obj;
  action->handle = handle;
  action->url = url;
  action->passwd = g_strdup(passwd);

  return N_ACTION(action);
}

n_action*
n_loc_list_new(NCS_Connection obj, const CORBA_char* dir)
{
  n_loc_list_action* action;

  action = g_new0(n_loc_list_action, 1);
  
  action->type = N_LOC_LIST;
  action->signals = NULL;
  action->obj = obj;
  action->dir = g_strdup(dir);
  
  return N_ACTION(action);
}


int
n_action_activate(n_action* a)
{
  CORBA_Environment ev;

  if(a->type == N_CONNECTION_OPEN)
  {
    n_connection_open_action* action = (n_connection_open_action*)a;
    NCS_Connection_Open(action->obj, action->handle, action->url,
	    action->passwd, action, &ev);
  }
  else if(a->type == N_LOC_LIST)
  {
    n_loc_list_action* action = (n_loc_list_action*)a;
    NFTPLoc_List(action->obj, action->dir, action, &ev);
  }
}

int
n_signal_connect(n_action* action, char* signal,
	NSignalFunc callback, gpointer user_data)
{
  n_sig* sig;
  sig = g_new0(n_sig, 1);
  
  sig->sig_name = g_strdup(signal);
  sig->callback = callback;
  sig->user_data = user_data;

  action->signals = g_slist_append(action->signals, sig);
  
  return GPOINTER_TO_INT(sig);
}

