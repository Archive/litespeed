#include "lspeed.h"

lsNCS* ncs;


GtkWidget* combo_new(void)
{
  
  GtkWidget *widget  = gtk_combo_new ();
  gtk_widget_show(widget);

  return widget;
}
GtkWidget* button_new(char *label)
{
  
  GtkWidget *widget  = gtk_button_new_with_label (label);
  gtk_widget_show(widget);

  return widget;
}

GtkWidget* check_new(char *label)
{
  
  GtkWidget *widget  = gtk_check_button_new_with_label (label);
  gtk_widget_show(widget);

  return widget;
}

void destroy (GtkWidget *widget, gpointer *data)
{ 
  gtk_main_quit ();
} 

GtkWidget* new_lsmenu_item(GtkWidget *menu, char *name)
{
  GtkWidget * item = gtk_menu_item_new_with_label(name);

  gtk_menu_append( GTK_MENU(menu),item);  
  
  gtk_widget_show(item);
  
  return item;
}

lsftpcmd* new_lsftpcmd()
{
  lsftpcmd *ftpcmd;
  lsftpcmd *f;

  ftpcmd = g_new0(lsftpcmd,1);
  f = ftpcmd;
  
  f->menu = gtk_menu_new();
  
  f->STOR = new_lsmenu_item(f->menu, "STOR");

  return ftpcmd;
}

void testing_cb(GtkWidget *widget, GdkEventButton *event, gpointer *data)
{

}

lsitems *new_lsitems(GtkWidget * menu)
{
  lsitems *items;
  lsitems *i;
  GtkWidget *sep;

  items = g_new0(lsitems,1);
  i = items;

  
  i->Sort = new_lsmenu_item(menu, "Sort...");

  sep = gtk_menu_item_new();
  gtk_menu_append (GTK_MENU (menu), sep);
  gtk_widget_show (sep);

  i->ChgDir = new_lsmenu_item(menu, "Change Directroy");
  i->MkDir = new_lsmenu_item(menu, "Make Directroy");
  i->DelDir = new_lsmenu_item(menu, "Delete Directory");

  sep = gtk_menu_item_new();
  gtk_menu_append (GTK_MENU (menu), sep);
  gtk_widget_show (sep);

  
  /*gtk_menu_append(GTK_MENU(menu), gtk_menu_item_new());*/
  i->Transfer = new_lsmenu_item(menu, "Transfer File(s)");
  i->ViewFile = new_lsmenu_item(menu, "View File(s)");
  i->Execute = new_lsmenu_item(menu, "Execute");

  sep = gtk_menu_item_new();
  gtk_menu_append (GTK_MENU (menu), sep);
  gtk_widget_show (sep);

  i->Rename = new_lsmenu_item(menu, "Rename File");
  i->Move = new_lsmenu_item(menu, "Move File(s)...");
  i->Delete = new_lsmenu_item(menu, "Delete File(s)...");
  i->Chmod = new_lsmenu_item(menu, "Chmod File(s)...");

  sep = gtk_menu_item_new();
  gtk_menu_append (GTK_MENU (menu), sep);
  gtk_widget_show (sep);

  i->RefreshList = new_lsmenu_item(menu, "Refresh List");
  i->DirList = new_lsmenu_item(menu, "Directory List");

  sep = gtk_menu_item_new();
  gtk_menu_append (GTK_MENU (menu), sep);
  gtk_widget_show (sep);
  
  /*i->FTPCommands = gtk_menu_item_new_with_label("FTP COmmands");*/
  i->ftpcmd = new_lsftpcmd();
  gtk_menu_append( GTK_MENU(menu),i->FTPCommands);  
  
  gtk_menu_item_set_submenu (GTK_MENU_ITEM (i->FTPCommands),i->ftpcmd->menu);
  gtk_widget_show(i->FTPCommands);


  
  return items;
}


lsbuttons *new_lsbuttons()
{
  
  lsbuttons *buttons;
  lsbuttons *b;
  
  buttons = g_new0(lsbuttons,1);
  b = buttons;

  /*Build the buttons.*/
  b->vbox = gtk_vbox_new(FALSE,0);
  
  /*Set size.*/
  gtk_widget_set_usize (b->vbox, 70, 0);
  /*Set border.*/
  gtk_container_border_width(GTK_CONTAINER(b->vbox), 3);

  b->regex = gtk_entry_new();
  b->MkDir = gtk_button_new_with_label(N_("MkDir"));
  b->ChgDir = gtk_button_new_with_label(N_("ChgDir"));
  b->View = gtk_button_new_with_label(N_("View"));
  b->Rename = gtk_button_new_with_label(N_("Rename"));
  b->Delete = gtk_button_new_with_label(N_("Delete"));
  b->Refresh = gtk_button_new_with_label(N_("Refresh"));
  b->DirInfo = gtk_button_new_with_label(N_("DirInfo"));

  /*Pack buttons.*/
  gtk_box_pack_start (GTK_BOX (b->vbox), b->regex, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX (b->vbox), b->MkDir, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX (b->vbox), b->ChgDir, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX (b->vbox), b->View, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX (b->vbox), b->Rename, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX (b->vbox), b->Delete, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX (b->vbox), b->Refresh, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX (b->vbox), b->DirInfo, FALSE, FALSE, 0);

  /*Show buttons.*/
  gtk_widget_show(b->MkDir);
  gtk_widget_show(b->regex);
  gtk_widget_show(b->View);
  gtk_widget_show(b->Rename);
  gtk_widget_show(b->Delete);
  gtk_widget_show(b->Refresh);
  gtk_widget_show(b->DirInfo);
  gtk_widget_show(b->vbox);

  /*Return the struct.*/
  return buttons;
}

ls_init_conn(lswin* win, lssystem* sys, char* name)
{
  n_action* open_conn;	

  lsaction* action;
  action = g_new0(lsaction, 1);

  sys->url = URL__alloc();

  printf("name = %s\n", name);

  if(strcmp(name, "Local System") == 0  /*test only && FALSE */)
  {
    sys->url->url = g_strdup("file:/");
    sys->url->resource = g_strdup("/home/");
    sys->url->proto = g_strdup("file");
    sys->url->host = g_strdup("thunder.cgibuilder.com");
    sys->url->password = g_strdup("otherpasswd");
    sys->url->user = g_strdup("matt");
    sys->url->port = 0;

    /*
    set_url(sys->url, "file:/home/");
    */

    action->sys = sys;
    action->win = win;
    action->action = LA_LIST;
    
    /*Build the new operation object*/
    open_conn = n_connection_open_new(ncs->NCS, win->nftpc->NFTPC,
	    sys->url, "passwd");

    /*Connect up to the signals of interest*/
    n_signal_connect(open_conn, "done", 
	    N_SIGNAL_FUNC(ls_nftpc_open_cb), action);
    /*
    n_signal_connect(open_conn, N_SIG_ERROR, 
	    N_SIGNAL_FUNC(ls_open_error_cb), action);
     */
    
    /*start the operation*/
    n_action_activate(open_conn);

  }
  else /*name == Remote System*/
  {
    if(/*sys->autoconnect*/ 0)
    {
      /*FIXME: use the config.*/
      sys->url->url = g_strdup("ftp://");
      sys->url->resource = g_strdup("/home/test/");
      sys->url->proto = g_strdup("ftp");
      sys->url->host = g_strdup("thunder.cgibuilder.com");
      sys->url->password = g_strdup("testme");
      sys->url->user = g_strdup("test");
      sys->url->port = 21;

      action->sys = sys;
      action->win = NULL;
      action->action = LA_LIST;

      /*
      NCS_Connection_Open(ncs->NCS, win->nftpc->NFTPC, sys->url, "passwd", 
	      action, &sys->filer_ev);
       */
    }
  }
}

lssystem* new_system(lswin* win, char * name)
{
  char *titles[] = {
    N_("^"), 
    N_("Name"), 
    N_("Permissions"), 
    N_("Size"), 
    N_("Date")
  };

  /*This system*/
  lssystem *sys = g_new0(lssystem,1);
  lssystem *s = sys;

  s->sort = g_new0(lssort,1);
  s->sort->current = TYPE;
  s->sort->type[TYPE] = 0;
  s->sort->type[NAME] = 0;
  s->sort->type[DATE] = 0;
  s->sort->type[SIZE] = 0;

  /*this system's clist*/
  s->clist = gtk_clist_new_with_titles (5, titles);
  /*gtk_widget_set_usize (clist, 200, -1);*/

  /*this system clist's scrolled window*/
  s->scwindow = gtk_scrolled_window_new(0,0);
  gtk_scrolled_window_set_policy((GTK_SCROLLED_WINDOW(s->scwindow)), 
	      GTK_POLICY_AUTOMATIC, 
	      GTK_POLICY_AUTOMATIC);

  gtk_clist_set_column_width(GTK_CLIST(s->clist),0,20);
  gtk_clist_set_column_justification(GTK_CLIST(s->clist),0,
				     GTK_JUSTIFY_CENTER);
  gtk_clist_set_column_width(GTK_CLIST(s->clist),1,100);
  gtk_clist_set_column_width(GTK_CLIST(s->clist),2,90);
  gtk_clist_set_column_width(GTK_CLIST(s->clist),3,50);
  gtk_clist_set_column_justification(GTK_CLIST(s->clist),3,
				     GTK_JUSTIFY_RIGHT);
  gtk_clist_set_column_width(GTK_CLIST(s->clist),4,125);

  gtk_clist_set_selection_mode(GTK_CLIST(s->clist),
			       GTK_SELECTION_MULTIPLE);
  /*
  gtk_clist_set_policy(GTK_CLIST(s->clist),
		       GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
		       */

  /*This system's buttons.*/
  s->buttons = new_lsbuttons();


  /*This system's frame.*/
  s->frame = gtk_frame_new(name);

  /*Directory entry widget.*/
  s->cbitems = NULL;
  s->directory = combo_new();
  /*gtk_combo_set_popdown_strings (GTK_COMBO (directory), sys->cbitems);*/

  /*This systems's box.*/
  s->topbox = gtk_hbox_new(FALSE,0);
  s->vbox = gtk_vbox_new(FALSE,0);
  s->hbox = gtk_hbox_new(FALSE,0);
  
  /*Pack the members of this system.*/
  gtk_box_pack_start (GTK_BOX (s->topbox), s->frame, TRUE, TRUE, 0);

  gtk_container_border_width(GTK_CONTAINER(s->vbox), 2);
  gtk_box_pack_start (GTK_BOX (s->vbox), s->directory, FALSE, TRUE, 0);
  gtk_box_pack_start (GTK_BOX (s->vbox), s->hbox, TRUE, TRUE, 0);
 
  gtk_container_add(GTK_CONTAINER(s->scwindow), s->clist);
  gtk_box_pack_start (GTK_BOX (s->hbox), s->scwindow, TRUE, TRUE, 0);

  gtk_box_pack_start (GTK_BOX (s->hbox), s->buttons->vbox, FALSE, FALSE, 0);
  /*gtk_box_pack_end (GTK_BOX (s->hbox), s->buttons->vbox, FALSE, FALSE, 0);*/

  gtk_container_add (GTK_CONTAINER (s->frame), s->vbox);
  
  /*Show system.*/
  gtk_widget_show(s->scwindow);
  gtk_widget_show(s->clist);
  gtk_widget_show(s->hbox);
  gtk_widget_show(s->vbox);

  gtk_widget_show(s->frame);
  gtk_widget_show(s->topbox);

  /*This system's menu.*/
  s->menu = gtk_menu_new();
  s->items = new_lsitems(s->menu);
  
  /*Add members to system.*/
  s->last_clicked_row = 0;
  
  /*sys->buttons = buttons;*/
  
  /*Gui built.*/

  ls_init_conn(win, sys, name);
  return sys;
}

lsmove* new_lsmove(lswin *newwin)
{
  lsmove *move = g_new0(lsmove,1);
  GtkWidget *win = GTK_WIDGET(&(GNOME_APP(newwin->app)->parent_object));
  GtkWidget *box, *vbox;
  GtkWidget *Upload, *Download;
  GtkWidget* pix=NULL;

  box = gtk_vbox_new(TRUE, 0); /*We just use the center.*/
  move->box = box;

  vbox = gtk_vbox_new(FALSE,0);
  gtk_box_pack_start (GTK_BOX (box), vbox, FALSE, FALSE, 0);
  gtk_widget_show(vbox);
  move->vbox = vbox;

  Upload = gtk_button_new();
  pix = gnome_stock_pixmap_widget(win, GNOME_STOCK_PIXMAP_FORWARD);
  gtk_widget_show(pix);
  gtk_container_add(GTK_CONTAINER(Upload), pix);
  gtk_box_pack_start (GTK_BOX (vbox), Upload, FALSE, FALSE,5);
  gtk_widget_show(Upload);
  move->Upload = Upload;

  Download = gtk_button_new();
  pix = gnome_stock_pixmap_widget(win, GNOME_STOCK_PIXMAP_BACK);
  gtk_widget_show(pix);
  gtk_container_add(GTK_CONTAINER(Download), pix);
  gtk_box_pack_start (GTK_BOX (vbox), Download, FALSE, FALSE,5);
  gtk_widget_show(Download);
  move->Download = Download;

  return move;
}

#if 0
lsmove* new_lsmove()
{
  lsmove *move;
  lsmove *m;

  move = g_new0(lsmove,1);
  m = move;

  m->box = gtk_vbox_new(TRUE, 0); /*We just use the center.*/

  m->vbox = gtk_vbox_new(FALSE,0);
  
  /*
  m->Upload = gtk_button_new_with_label(" -> ");
  m->Download = gtk_button_new_with_label(" <- ");
  */

  gtk_box_pack_start (GTK_BOX (m->vbox), m->Upload, FALSE, FALSE,5);
  gtk_box_pack_start (GTK_BOX (m->vbox), m->Download, FALSE, FALSE,5);

  gtk_box_pack_start (GTK_BOX (m->box), m->vbox, FALSE, FALSE, 0);

  /*Show.*/
  gtk_widget_show(m->Upload);
  gtk_widget_show(m->Download);
  gtk_widget_show(m->vbox);
  /*gtk_widget_show(m->box);*/

  return move;
}
#endif /*0*/


lstransfer* new_lstransfer()
{
  lstransfer *transfer;
  lstransfer *t;

  transfer = g_new0(lstransfer,1);
  t = transfer;
  
  t->vbox = gtk_hbox_new(FALSE, 0);
  t->box = gtk_hbox_new(TRUE, 0);

  
  t->Text = gtk_radio_button_new_with_label (NULL,
					     N_("Text"));
  
  t->group = gtk_radio_button_group (GTK_RADIO_BUTTON (t->Text)); 
  
  t->Binary = gtk_radio_button_new_with_label (t->group,
					       N_("Binary"));

  t->Auto = gtk_check_button_new_with_label (N_("Auto"));

  gtk_box_pack_start (GTK_BOX (t->vbox), t->Text, FALSE, FALSE, 1);
  gtk_box_pack_start (GTK_BOX (t->vbox), t->Binary, FALSE, FALSE, 1);
  gtk_box_pack_start (GTK_BOX (t->vbox), t->Auto, FALSE, FALSE, 1);

  gtk_box_pack_start (GTK_BOX (t->box), t->vbox, FALSE, FALSE, 1);

  gtk_widget_show(t->vbox);
  gtk_widget_show(t->Auto);
  gtk_widget_show(t->Text);
  gtk_widget_show(t->Binary);
  
  return transfer;
}

lsprogress_d* new_progress_d(char* label)
{
  lsprogress_d* progress_d;
  lsprogress_d* p;

  progress_d = g_new0(lsprogress_d,1);
  p = progress_d;

  p->window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_container_border_width(GTK_CONTAINER(p->window), 3);
  gtk_window_set_title(GTK_WINDOW(p->window), label);
  gtk_signal_connect (GTK_OBJECT (p->window), "delete_event",
                      GTK_SIGNAL_FUNC (unshow), NULL);
  
  p->outer = gtk_vbox_new(FALSE,0);
  gtk_widget_show(p->outer);
  p->inner = gtk_hbox_new(TRUE,0);
  gtk_widget_show(p->inner);
  p->left = gtk_vbox_new(FALSE,0);
  gtk_widget_show(p->left);
  p->right = gtk_vbox_new(FALSE,0);
  gtk_widget_show(p->right);
  p->pbar = gtk_progress_bar_new();
  gtk_widget_show(p->pbar);
  
  p->From = label_new(N_("From: ")); 
  p->To = label_new(N_("To: ")); 

  p->Percent = label_new(N_("Percent:")); 
  p->Size = label_new(N_("Size:")); 
  p->ToGo = label_new(N_("To Go:")); 
  p->Rate = label_new(N_("Rate:")); 
  
  p->Xfered = label_new(label); 
  p->TotalXfered = label_new(label); 

  p->TotalRate = label_new(N_("Total Rate:")); 

  p->table = gtk_table_new(1, 4, TRUE);
  gtk_widget_show(p->table);

  p->Cancel = button_new("Cancel");
  p->Retry = button_new("Retry");

  gtk_table_attach_defaults(GTK_TABLE(p->table), p->Cancel, 0, 1, 0, 1);
  gtk_table_attach_defaults(GTK_TABLE(p->table), p->Retry, 3, 4, 0, 1);

  gtk_box_pack_start(GTK_BOX(p->outer), p->From, FALSE, FALSE, 2);
  gtk_box_pack_start(GTK_BOX(p->outer), p->To, FALSE, FALSE, 2);
  gtk_box_pack_start(GTK_BOX(p->outer), p->pbar, FALSE, FALSE, 2);
  gtk_box_pack_start(GTK_BOX(p->outer), p->inner, FALSE, FALSE, 2);
  gtk_box_pack_start(GTK_BOX(p->outer), p->table, FALSE, FALSE, 2);

  gtk_box_pack_start(GTK_BOX(p->inner), p->left, FALSE, FALSE, 2);
  gtk_box_pack_start(GTK_BOX(p->inner), p->right, FALSE, FALSE, 2);

  gtk_box_pack_start(GTK_BOX(p->left), p->Percent, FALSE, FALSE, 2);
  gtk_box_pack_start(GTK_BOX(p->left), p->Size, FALSE, FALSE, 2);
  gtk_box_pack_start(GTK_BOX(p->left), p->ToGo, FALSE, FALSE, 2);
  gtk_box_pack_start(GTK_BOX(p->left), p->Rate, FALSE, FALSE, 2);

  gtk_box_pack_start(GTK_BOX(p->right), p->Xfered, FALSE, FALSE, 2);
  gtk_box_pack_start(GTK_BOX(p->right), p->TotalXfered, FALSE, FALSE, 2);
  gtk_box_pack_start(GTK_BOX(p->right), p->TotalRate, FALSE, FALSE, 2);


  
  gtk_widget_realize(p->window);
  gtk_container_add (GTK_CONTAINER (p->window), p->outer);

  return progress_d;
}

GnomeUIInfo lswin_filemenu[] = {
  GNOMEUIINFO_ITEM_STOCK
  (N_("Exit"),
   N_("Get out of here"),
   lsquit, 
   GNOME_STOCK_PIXMAP_EXIT),
  
  GNOMEUIINFO_END
};

GnomeUIInfo lswin_windowmenu[] = {
  GNOMEUIINFO_ITEM_STOCK
  (N_("ViewLog"),
   N_("View log file"),
   ViewLog_cb, 
   GNOME_STOCK_PIXMAP_BOOK_YELLOW),

  GNOMEUIINFO_ITEM_STOCK
  (N_("Options"),
   N_("Set options for litespeed"),
   Options_cb,
   GNOME_STOCK_PIXMAP_PREFERENCES),

  GNOMEUIINFO_END
};

GnomeUIInfo lswin_helpmenu[] = {
  GNOMEUIINFO_ITEM_STOCK
  (N_("About"),
   N_("Info about this program"),
   about_cb, 
   GNOME_STOCK_MENU_ABOUT),

  GNOMEUIINFO_SEPARATOR,

  GNOMEUIINFO_HELP
  ("help-browser"),

  GNOMEUIINFO_END
};

GnomeUIInfo lswin_mainmenu[] = {
  GNOMEUIINFO_SUBTREE
  (N_("File"), 
   lswin_filemenu),

  GNOMEUIINFO_SUBTREE
  (N_("Window"), 
   lswin_windowmenu),

  GNOMEUIINFO_SUBTREE
  (N_("Help"), 
   lswin_helpmenu),

  GNOMEUIINFO_END
};

GnomeUIInfo lswin_toolbar[] = {
  GNOMEUIINFO_ITEM_STOCK
  (N_("Connect"), 
   N_("Connect to remote site"),
   Connect_cb, 
   GNOME_STOCK_PIXMAP_OPEN),

  GNOMEUIINFO_SEPARATOR,

  GNOMEUIINFO_ITEM_STOCK
  (N_("Cancel"),
   N_("Cancel connection to remote site"),
   NULL,
   GNOME_STOCK_PIXMAP_STOP),

  GNOMEUIINFO_SEPARATOR,

  GNOMEUIINFO_ITEM_STOCK
  (N_("ViewLog"),
   N_("View log file"),
   ViewLog_cb,
   GNOME_STOCK_PIXMAP_BOOK_YELLOW),

  GNOMEUIINFO_SEPARATOR,

  GNOMEUIINFO_ITEM_STOCK
  (N_("Help"),
   N_("Show help about litespeed"),
   NULL,
   GNOME_STOCK_PIXMAP_HELP),

  GNOMEUIINFO_SEPARATOR,

  GNOMEUIINFO_ITEM_STOCK
  (N_("Options"),
   N_("Set options for litespeed"),
   Options_cb,
   GNOME_STOCK_PIXMAP_PREFERENCES),

  GNOMEUIINFO_SEPARATOR,

  GNOMEUIINFO_ITEM_STOCK
  (N_("About"),
   N_("Who is responsible for this"),
   about_cb,
   GNOME_STOCK_PIXMAP_ABOUT),

  GNOMEUIINFO_SEPARATOR,

  GNOMEUIINFO_ITEM_STOCK
  (N_("Exit"),
   N_("Get out of here"),
   lsquit,
   GNOME_STOCK_PIXMAP_EXIT),

  GNOMEUIINFO_END
};
						              
lswin* new_lswin(GList* lswinlist)
{
  lswin* win;
  lswin* w;
  /*lsmove* move;*/

  win = g_new0(lswin,1);
  w = win;

  win->gopts = new_options(".litespeedrc");
  
  w->localcwd = g_strdup("/home/");
  w->remotecwd = g_strdup(".");

  w->get_progress_d = new_progress_d("Get");
  w->put_progress_d = new_progress_d("Put");

  /*gtk_widget_show(newwin->get_progress_d->window);*/

  w->move = new_lsmove(win);
  
  w->app = gnome_app_new("Litespeed", _("Litespeed"));
  gnome_app_create_toolbar_with_data(GNOME_APP(w->app), lswin_toolbar, win);
  gnome_app_create_menus_with_data(GNOME_APP(w->app), lswin_mainmenu, win);


  w->nftpc = ls_cb_CORBA_new(ncs->ORB, &ncs->ev);
  
  w->vbox = gtk_vbox_new(FALSE, 0);
  w->hbox = gtk_hbox_new(FALSE, 0);

  /*w->tips;*/
  
  w->connect_d = new_Connect_d(win);
  w->viewlog_d = new_ViewLog_d();
  w->options_d = new_Options_d();

  /*Text widget for logging.*/
  w->text = gtk_text_new (NULL, NULL);
  w->sbar = gtk_vscrollbar_new (GTK_TEXT (w->text)->vadj); 
  gtk_widget_set_usize (w->text, -1, 75);
  gtk_text_set_editable(GTK_TEXT(w->text),FALSE);

  w->table = gtk_table_new(1, 2, FALSE);

  gtk_widget_show(w->table);
  gtk_widget_show(w->sbar);

  gtk_table_attach (GTK_TABLE (w->table), w->sbar, 1, 2, 0, 1,
                    GTK_FILL, GTK_EXPAND | GTK_SHRINK | GTK_FILL, 0, 0);      
  gtk_table_attach (GTK_TABLE (w->table), w->text, 0, 1, 0, 1,
                    GTK_EXPAND | GTK_SHRINK | GTK_FILL,
                    GTK_EXPAND | GTK_SHRINK | GTK_FILL, 0, 0); 
  
  w->local = new_system(win, "Local System");
  w->remote = new_system(win, "Remote System");
  
  w->transfer = new_lstransfer();

  gtk_widget_set_usize (w->app, 600, 500);
  gtk_signal_connect (GTK_OBJECT (w->app), "delete_event",
                      GTK_SIGNAL_FUNC (destroy),
                      NULL);
  gnome_app_set_contents(GNOME_APP(w->app), w->vbox);
  gtk_container_border_width(GTK_CONTAINER(w->vbox), 2);
  gtk_box_pack_start(GTK_BOX(w->vbox), w->hbox, TRUE, TRUE, 0);

  /*Add local system.*/
  gtk_box_pack_start(GTK_BOX(w->hbox), w->local->topbox, TRUE, TRUE, 0);

  /*Add the lsmove buttons.*/
  gtk_widget_show(w->move->box);
  gtk_box_pack_start(GTK_BOX(w->hbox), w->move->box, FALSE, FALSE,5);
  
  /*Add remote system.*/
  gtk_box_pack_start(GTK_BOX(w->hbox), w->remote->topbox, TRUE, TRUE, 0);

  /*Back to the vbox.*/
  gtk_box_pack_start(GTK_BOX(w->vbox), w->transfer->box, FALSE, FALSE, 0);
  gtk_widget_show(w->transfer->box);
  gtk_box_pack_start(GTK_BOX(w->vbox), w->table, FALSE, TRUE, 0);


  
  /*I'll show you mine, if you show....*/
  gtk_widget_show(w->vbox);
  gtk_widget_show(w->hbox);
  gtk_widget_show(w->text);
  gtk_widget_show(w->app);


  return win;
}

int lsquit(GdkEventAny* unused)
{
  printf("LSQUIT\n");
  
  /*FIXME: test for all connections closed*/
  gtk_main_quit ();
  return 0;
}

static void 
staticfunction(GdkEventKey* e) {
  printf("Staticfunction called! %p\n",e);
}

static void
staticfunction_cb(GdkEventKey* e,void *unused)
{
  printf("Staticfunction-cb called! %p\n",e);
}


opts* new_options(char *lsrc)
{
  opts* options;
  
  fprintf(stderr, "in new_options\n");
  options = g_new0(opts,1);
  options->sites = NULL;

  /*init_config(options);*/
  if( !ls_read_config(options, lsrc) )
  {
    fprintf(stderr, "Config error ~/.litespeedrc\n");
    /*exit();*/
  }
  
  /*tempshowall(options);*/
  /*printf("port=%i\n",options->sites[1].port);*/
  return options;
}

char *clist_dnd_types[] = {
  "text/plain"
};

void ls_connect_signals(lswin* win)
{
  /* Drag and Drop */
  /*FIXME:  Use new api to do dnd, with XDnD
  gtk_signal_connect (GTK_OBJECT(win->local->clist), "drag_request_event",
                      GTK_SIGNAL_FUNC (clist_drag_cb), win);
  gtk_widget_dnd_drag_set(win->local->clist, TRUE, clist_dnd_types, 1);
  gtk_signal_connect (GTK_OBJECT(win->local->clist), "drop_enter_event",
                      GTK_SIGNAL_FUNC(clist_drop_highlight_cb), win);
  gtk_signal_connect (GTK_OBJECT(win->local->clist), "drop_leave_event",
                      GTK_SIGNAL_FUNC(clist_drop_highlight_cb), win);
  gtk_signal_connect (GTK_OBJECT(win->local->clist), 
		      "drop_data_available_event",
                      GTK_SIGNAL_FUNC(clist_drop_cb), win);

  gtk_widget_dnd_drop_set(win->local->clist, TRUE, clist_dnd_types, 1, FALSE);
  */

  
  /*Clists*/
  gtk_signal_connect (GTK_OBJECT(win->remote->clist), "event",
		      GTK_SIGNAL_FUNC (clist_event_cb),win);
  gtk_signal_connect_after (GTK_OBJECT(win->local->clist), "event",
		      GTK_SIGNAL_FUNC (clist_event_cb),win);


  /*Sort*/
  gtk_signal_connect (GTK_OBJECT (win->local->clist), "click_column",
		      GTK_SIGNAL_FUNC (change_sort_cb), win);
  gtk_signal_connect (GTK_OBJECT (win->remote->clist), "click_column",
		      GTK_SIGNAL_FUNC (change_sort_cb), win);

  /*Buttons*/
  gtk_signal_connect (GTK_OBJECT(win->local->buttons->MkDir), "clicked",
                      GTK_SIGNAL_FUNC(localMkDir_cb),win);

  gtk_signal_connect (GTK_OBJECT(win->remote->buttons->MkDir), "clicked",
		      GTK_SIGNAL_FUNC (remoteMkDir_cb),win);

  gtk_signal_connect (GTK_OBJECT(win->local->buttons->Rename), "clicked",
		      GTK_SIGNAL_FUNC (localRename_cb),win);

  gtk_signal_connect (GTK_OBJECT(win->remote->buttons->Rename), "clicked",
		      GTK_SIGNAL_FUNC (remoteRename_cb),win);

  gtk_signal_connect (GTK_OBJECT(win->local->buttons->Delete), "clicked",
		      GTK_SIGNAL_FUNC (localDelete_cb),win);

  gtk_signal_connect (GTK_OBJECT(win->local->buttons->Refresh), "clicked",
		      GTK_SIGNAL_FUNC (Refresh_cb),win);

  gtk_signal_connect (GTK_OBJECT(win->remote->buttons->Refresh), "clicked",
		      GTK_SIGNAL_FUNC (Refresh_cb),win);

  gtk_signal_connect (GTK_OBJECT(win->remote->buttons->Delete), "clicked",
		      GTK_SIGNAL_FUNC (localDelete_cb),win);


  /*Move*/
  gtk_signal_connect (GTK_OBJECT(win->move->Upload), "clicked",
		      GTK_SIGNAL_FUNC (Upload_cb),win);

  gtk_signal_connect (GTK_OBJECT(win->move->Download), "clicked",
		      GTK_SIGNAL_FUNC (Download_cb),win);

  /*Items*/
  gtk_signal_connect (GTK_OBJECT(win->local->items->Transfer), "activate",
		      GTK_SIGNAL_FUNC (Upload_cb),win);

  gtk_signal_connect (GTK_OBJECT(win->local->items->Delete), "activate",
		      GTK_SIGNAL_FUNC (Delete_cb),win);

  gtk_signal_connect (GTK_OBJECT(win->local->buttons->regex), "activate",
		      GTK_SIGNAL_FUNC (Refresh_cb),win);

  
}

CORBA_Environment ev;
/*
CORBA_ORB orb;
*/

int main(int argc, char **argv)
{
  lswin *win;

  /*gtk_init(&argc, &argv);*/
  gnome_init("Litespeed", "0.1", argc, argv);

  ncs = LS_and_CORBA_init(&argc, argv);

  /* Initialise the i18n stuff */
  bindtextdomain(PACKAGE, GNOMELOCALEDIR);
  textdomain(PACKAGE);
  
  win = new_lswin(NULL);

  ls_connect_signals(win);

  lsStatusText (win, "Welcome to Litespeed.\n", LC_NORMAL);

  local_init (win);
  remote_init (win);
  
  gtk_rc_parse("gtkrc");
  gtk_main ();
  gdk_exit (0);
  return 0;
}

void
about_cb (GtkWidget *widget, void *data)
{
  GtkWidget *about;
  const gchar *authors[] = {
    "Matt Wimer <matt@cgibuilder.com>",
    "Raj Dutt <rdutt@voxel.net>",
    "Craig Small <csmall@small.dropbear.id.au>",
    NULL
  };

  about = gnome_about_new( 
     _("Litespeed"), 
     VERSION,
     _("Copyright 1998, 1999 Litespeed Developers."),
     authors,
     _("A nice Gnome-ified FTP client."),
     NULL);

  gtk_widget_show(about);

}
