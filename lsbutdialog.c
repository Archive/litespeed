#include "lspeed.h"

void ChgDir_cb (GtkWidget *widget, lswin* win)
{
  GtkWidget* local_button = win->local->buttons->ChgDir;
  GtkWidget* remote_button = win->remote->buttons->ChgDir;
  char *txt;

  if(widget == local_button)
  {
    
    txt = gtk_entry_get_text
      (GTK_ENTRY(GTK_COMBO(win->local->directory)->entry));
    gtk_widget_show(win->local->dialogs->ChgDir_d->window);
    gtk_entry_set_text (GTK_ENTRY(win->local->dialogs->ChgDir_d->dir), txt);
    gtk_signal_connect (GTK_OBJECT(win->local->dialogs->ChgDir_d->OK),
			"clicked",
			GTK_SIGNAL_FUNC(chgdir_confirmed),win);
    gtk_signal_connect (GTK_OBJECT(win->local->dialogs->ChgDir_d->dir),
			"activate",
			GTK_SIGNAL_FUNC(chgdir_confirmed),win);
    gtk_signal_connect (GTK_OBJECT(win->local->dialogs->ChgDir_d->Cancel),
			"clicked",
			GTK_SIGNAL_FUNC(chgdir_canceled),win);
  }
  if(widget == remote_button)
  {
    gtk_widget_show(win->remote->dialogs->ChgDir_d->window);
  }
}


void do_local_mkdir_cb(gchar *path, lswin* win)
{
  lssystem* sys = win->local;
  lsaction* action;

  /* If the user hit cancel then string will be null */
  if (path == NULL)
    return;

  g_print("You said %s\n", path);
  /* Enter some magic here to create dir */

  action = g_new0(lsaction,1);
  action->sys = sys;
  action->win = NULL;
  action->action = LA_LIST;

  NCS_Loc_Create(sys->lister, path, action, &sys->lister_ev);
}

void localMkDir_cb (GtkWidget *widget, lswin* win)
{
  GtkWidget *dlg;

  dlg = gnome_app_request_string(GNOME_APP(win->app), 
        N_("Enter the directory you want created on the Local System"),
        (GnomeStringCallback)do_local_mkdir_cb, win);
  gtk_window_set_title(GTK_WINDOW(&(GNOME_DIALOG(dlg)->window)), 
		       N_("Make Local directory."));

  gtk_widget_show(dlg);

}


void do_remote_mkdir_cb(gchar *path, lswin* win)
{
  lssystem* sys = win->remote;
  lsaction* action;

  /* If the user hit cancel then string will be null */
  if (path == NULL)
    return;

  g_print("You said %s\n", path);
  /* Enter some magic here to create dir */
  action = g_new0(lsaction,1);
  action->sys = sys;
  action->win = NULL;
  action->action = LA_LIST;

  NCS_Loc_Create(sys->lister, path, action, &sys->lister_ev);

}

void remoteMkDir_cb (GtkWidget *widget, lswin* win)
{
  GtkWidget *dlg;

  dlg = gnome_app_request_string(GNOME_APP(win->app), 
        N_("Enter the directory you want created on the Remote System:"),
        (GnomeStringCallback)do_remote_mkdir_cb, win);

  gtk_window_set_title(GTK_WINDOW(&(GNOME_DIALOG(dlg)->window)), 
		       N_("Make Remote Directory."));

  gtk_widget_show(dlg);

}

static void do_local_rename_cb(gchar *string, lswin* win)
{
  /* If the user hit cancel then string will be null */
  if (string == NULL)
    return;

  g_print("You wanted local file renamed to  %s\n", string);
  /* Enter some magic here to rename files */

}

void localRename_cb (GtkWidget *widget, lswin* win)
{
  GtkWidget *dlg;

  dlg = gnome_app_request_string(GNOME_APP(win->app), 
        N_("Enter the new name of the file on the Local System:"),
        (GnomeStringCallback)do_local_rename_cb, win);
  gtk_window_set_title(GTK_WINDOW(&(GNOME_DIALOG(dlg)->window)), 
		       N_("Rename Local File."));

  gtk_widget_show(dlg);

}

static void do_remote_rename_cb(gchar *string, lswin* win)
{
  /* If the user hit cancel then string will be null */
  if (string == NULL)
    return;

  g_print("You wanted remote file renamed to  %s\n", string);
  /* Enter some magic here to rename files */

}

void remoteRename_cb (GtkWidget *widget, lswin* win)
{
  GtkWidget *dlg;

  dlg = gnome_app_request_string(GNOME_APP(win->app), 
        N_("Enter the new name of the file on the Remote System."),
        (GnomeStringCallback)do_remote_rename_cb, win);
  gtk_window_set_title(GTK_WINDOW(&(GNOME_DIALOG(dlg)->window)), 
		       N_("Rename Remote File:"));
  
  gtk_widget_show(dlg);

}

void Rename_cb (GtkWidget *widget, lswin* win)
{
  GtkWidget* local_button = win->local->buttons->Rename;
  GtkWidget* remote_button = win->remote->buttons->Rename;
  GtkWidget* clist;
  GList* list;


  char *txt;
  int row;

  if(widget == local_button)
  {
    clist = win->local->clist; 
    list = GTK_CLIST(clist)->selection;
    if(!list)
      return;

    row = (gint) list->data;
    gtk_clist_get_text (GTK_CLIST (clist), row, 1, &txt);

    gtk_widget_show(win->local->dialogs->Rename_d->window);
    gtk_entry_set_text (GTK_ENTRY(win->local->dialogs->Rename_d->file),
			txt);
  }
  if(widget == remote_button)
  {
    clist = win->local->clist; 
    list = GTK_CLIST(clist)->selection;
    if(!list)
      return;

    row = (gint) list->data;
    gtk_clist_get_text (GTK_CLIST (clist), row, 1, &txt);
    
    gtk_widget_show(win->remote->dialogs->Rename_d->window);
    gtk_entry_set_text (GTK_ENTRY(win->remote->dialogs->Rename_d->file),
			txt);
  }

}


static void do_remote_delete_cb(gint reply, lswin* win)
{
  g_print("remote delete reply was %d\n", reply);
 
}
void remoteDelete_cb(GtkWidget *widget, lswin* win)
{
  GtkWidget *dlg;

  dlg = gnome_app_ok_cancel(GNOME_APP(win->app),
                            N_("Do you want to delete this file"),
			    (GnomeReplyCallback)do_remote_delete_cb, win);

  gtk_window_set_title(GTK_WINDOW(&(GNOME_DIALOG(dlg)->window)), 
		       N_("Delete Remote file"));

  gtk_widget_show(dlg);
}

static void do_local_delete_cb(gint reply, lswin* win)
{
  if (reply == GNOME_OK)
    delete_file_confirmed(NULL, win->local, win);
}

void localDelete_cb(GtkWidget *widget, lswin* win)
{
  GtkWidget *dlg;

  dlg = gnome_app_ok_cancel(GNOME_APP(win->app),
        N_("Do you want to delete this file?"),
	(GnomeReplyCallback)do_local_delete_cb, win);

  gtk_window_set_title(GTK_WINDOW(&(GNOME_DIALOG(dlg)->window)), 
		       N_("Delete Remote file"));

  gtk_widget_show(dlg);
}

void Delete_cb (GtkWidget *widget, lswin* win)
{
  GtkWidget* local_button = win->local->buttons->Delete;
  GtkWidget* local_item = win->local->items->Delete;
  GtkWidget* remote_button = win->remote->buttons->Delete;
  GtkWidget* remote_item = win->remote->items->Delete;

  if((widget == local_button 
      || widget == local_item) 
     && GTK_CLIST(win->local->clist)->selection)
  {
#ifdef 0
    GtkWidget* w = gnome_app_request_string(GNOME_APP(win->app), 
					    "Enter file to delete:",
                   (GnomeStringCallback)(delete_cb), thewindow);
    gtk_widget_show(w);
#else
    gtk_signal_connect (GTK_OBJECT(win->local->dialogs->Delete_d->Yes),
			"clicked",
			GTK_SIGNAL_FUNC(delete_file_confirmed),win);
    gtk_signal_connect (GTK_OBJECT(win->local->dialogs->Delete_d->No),
			"clicked",
			GTK_SIGNAL_FUNC(delete_file_cancel),win);
    gtk_signal_connect (GTK_OBJECT(win->local->dialogs->Delete_d->Cancel),
			"clicked",
			GTK_SIGNAL_FUNC(delete_file_cancel),win);
    gtk_widget_show(win->local->dialogs->Delete_d->window);
#endif
  }

  if(widget == remote_button)
  {
    /*Show now.*/
    gtk_widget_show(win->remote->dialogs->Delete_d->window);
  }
}

void Refresh_cb (GtkWidget *widget, lswin* win)
{
  lssystem *sys;
  GtkWidget* local_button = win->local->buttons->Refresh;
  GtkWidget* local_regex = win->local->buttons->regex;
  GtkWidget* remote_button = win->remote->buttons->Refresh;
  GtkWidget* remote_regex = win->remote->buttons->regex;

  if(widget == local_button || widget == local_regex)
  {
    sys = win->local; 
    start_clist_populate (win, sys, "", FALSE);
  }
  if(widget == remote_button)
  {
    /*gtk_widget_show(win->remote->dialogs->Delete_d->window);*/
  }

}

void chgdir_confirmed (GtkWidget *widget, lswin* win)
{
  char *txt;
  lssystem *sys;
  GtkWidget* local_button = win->local->buttons->ChgDir;
  GtkWidget* remote_button = win->remote->buttons->ChgDir;

  if(local_button == widget)
  {
    sys = win->local;
    txt = gtk_entry_get_text
      (GTK_ENTRY(win->local->dialogs->ChgDir_d->dir));
  }
  if(remote_button == widget)
  {
    sys = win->remote;
    txt = gtk_entry_get_text
      (GTK_ENTRY(win->remote->dialogs->ChgDir_d->dir));
  }

  if(chdir(txt) != 0)
  {
    lsStatusText(win, "Can't change into directory.\n", LC_ERROR);
  }
  else
  {
    gtk_widget_hide(win->local->dialogs->ChgDir_d->window);
    start_clist_populate(win, sys, txt, FALSE);
  }
}

void chgdir_canceled(GtkWidget *widget, lswin* win)
{
  gtk_widget_hide(win->local->dialogs->ChgDir_d->window);
}

void delete_file_cancel(GtkWidget *widget, lswin* win)
{
  gtk_widget_hide(win->local->dialogs->Delete_d->window);
}

void r_mkdir_confirmed (GtkWidget *widget, lswin* win)
{  
  char *txt = gtk_entry_get_text
    (GTK_ENTRY(win->remote->dialogs->MkDir_d->dir));
  
  /*FtpStartMkdir(win, g_strdup(txt) );*/

  gtk_widget_hide(win->remote->dialogs->MkDir_d->dlg);
}

void r_mkdir_canceled(GtkWidget *widget, lswin* win)
{
  gtk_widget_hide(win->remote->dialogs->MkDir_d->dlg);
}
