#include "lspeed.h"

void change_sort_cb (GtkWidget *widget, int col, lswin* win)
{
  GtkWidget * local_clist = win->local->clist;
  GtkWidget * remote_clist = win->remote->clist;
  lssystem *sys;

  if(widget == local_clist)
  {
    sys = win->local;
  }
  if(widget == remote_clist)
  {
    sys = win->remote;
  }
  

  if(col == 0)
  {
    sys->sort->current = TYPE;
    sys->sort->type[TYPE] =  (sys->sort->type[TYPE] +1) % MAXTYPE;
    start_clist_populate (win, sys, "", FALSE);
  }

  if(col == 1)
  {
    sys->sort->current = NAME;
    sys->sort->type[NAME] =  (sys->sort->type[NAME] +1) % MAXNAME;
    start_clist_populate (win, sys, "", FALSE);
  }
  if(col == 2)
  {
    sys->sort->current = DATE;
    sys->sort->type[DATE] =  (sys->sort->type[DATE] +1) % MAXDATE;
    start_clist_populate (win, sys, "", FALSE);
  }
  if(col == 3)
  {
    sys->sort->current = SIZE;
    sys->sort->type[SIZE] =  (sys->sort->type[SIZE] +1) % MAXSIZE;
    start_clist_populate (win, sys, "", FALSE);
  }
}

