#!/usr/local/bin/perl
# test-regexes.pl
# test the regexes we compiled.


require "patterns" || die "can't bring in regex patterns: $!.\n";

print "URL: ";

READ_LOOP:
  while ( chomp($line=<STDIN>) )
  {
    if ($line eq "") {  next; }
    print "'$line'\n";
    foreach (@Regexes)
    {
      my $regex = $_->[1];
      my @matches;
      if ( (@matches) = $line =~m!$regex!i )
      {
        print "    '$_->[0]'  Flags: '$_->[3]'.\n";
        { local ($") = ", "; print "    @matches\n"; }
        print "\nURL: ";
        next READ_LOOP;
      }
    }
    print "-- Match failed! --\n";
  }

