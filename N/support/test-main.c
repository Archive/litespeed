#include "url.h"

/* Dummy main routine, just tests the functionality of the library */
void main(void)
{
  struct URL * url_data;
  char string[100];
  int length;
  int buff_size = 99;


  URL_init();

  while (! feof(stdin) )
  {
    fgets(string, buff_size, stdin);
    length = strlen(string);
    if (length == 1) { continue; }
    string[length - 1] = 0;  /* trim the newline */

    printf("Checking '%s'.\n", string);

    url_data = URL_parse(string);

    /* print out all the information in the URL structure */
    printf("Returned URL data:\n");
    if (url_data->url != NULL)
      printf("	URL = '%s'.\n", url_data->url);
    if (url_data->proto != NULL)
      printf("	Proto = '%s'.\n", url_data->proto);
    if (url_data->user != NULL)
      printf("	User = '%s'.\n", url_data->user);
    if (url_data->password != NULL)
      printf("	Password = '%s'.\n", url_data->password);
    if (url_data->host != NULL)
      printf("	Host = '%s'.\n", url_data->host);
    if (url_data->resource != NULL)
      printf("	Resource = '%s'.\n", url_data->resource);
    if (url_data->port != 0)
      printf("	Port = '%d'.\n", url_data->port);
    if (url_data->name != NULL)
      printf("	Name = '%s'.\n", url_data->name);
    printf("	Errors = '%d'.\n", url_data->errors);


    URL_free(url_data);
    printf("\n");
  }
}

