/*  --- This is a generated file! ---
 * All modifications will be lost after the next run of build-C.pl.
 * See the file README for information on how to rebuild your URL_parse
 * lib from scratch.
 * Generally, you won't need to modify this file.
 */

#include "url.h"

/* a structure for holding the regex and assorted data for it */
struct url_regex {
  char * pattern;      /* the non-compiled regular expression */
  pcre * regex;        /* the compiled regex */
  char * name;         /* the variable name for this regex */
  int positions[7];    /* array of match positions in the url string */
  int errors;          /* for oring error flags into */
};

static struct url_regex NURL_ho = {
    "^([a-z0-9#-][a-z0-9.#-]*?)$",
    NULL,   /* place holder for the compiled regex */
    "NURL_ho",
    { 1, 4 },
    0 | NURL_NO_PROTO | NURL_NO_PORT
};

static struct url_regex NURL_hore = {
    "^([a-z0-9#-][a-z0-9.#-]*?)/(|/*?.+)$",
    NULL,   /* place holder for the compiled regex */
    "NURL_hore",
    { 2, 4, 6 },
    0 | NURL_NO_PROTO | NURL_NO_PORT
};

static struct url_regex NURL_hoCpo = {
    "^([a-z0-9#-][a-z0-9.#-]*?):([0-9]+)$",
    NULL,   /* place holder for the compiled regex */
    "NURL_hoCpo",
    { 2, 4, 5 },
    0 | NURL_NO_PROTO
};

static struct url_regex NURL_hoCpore = {
    "^([a-z0-9#-][a-z0-9.#-]*?):([0-9]+)/(|/*?.+)$",
    NULL,   /* place holder for the compiled regex */
    "NURL_hoCpore",
    { 3, 4, 5, 6 },
    0 | NURL_NO_PROTO
};

static struct url_regex NURL_prCho = {
    "^([a-z][a-z0-9]+)://([a-z0-9#-][a-z0-9.#-]*?)$",
    NULL,   /* place holder for the compiled regex */
    "NURL_prCho",
    { 2, 1, 4 },
    0 | NURL_NO_PORT
};

static struct url_regex NURL_prChore = {
    "^([a-z][a-z0-9]+)://([a-z0-9#-][a-z0-9.#-]*?)/(|/*?.+)$",
    NULL,   /* place holder for the compiled regex */
    "NURL_prChore",
    { 3, 1, 4, 6 },
    0 | NURL_NO_PORT
};

static struct url_regex NURL_prChoCpo = {
    "^([a-z][a-z0-9]+)://([a-z0-9#-][a-z0-9.#-]*?):([0-9]+)$",
    NULL,   /* place holder for the compiled regex */
    "NURL_prChoCpo",
    { 3, 1, 4, 5 },
    0 | 0
};

static struct url_regex NURL_prChoCpore = {
    "^([a-z][a-z0-9]+)://([a-z0-9#-][a-z0-9.#-]*?):([0-9]+)/(|/*?.+)$",
    NULL,   /* place holder for the compiled regex */
    "NURL_prChoCpore",
    { 4, 1, 4, 5, 6 },
    0 | 0
};

static struct url_regex NURL_maCusATho = {
    "^mailto:([^-][^:@/]*?)@([a-z0-9#-][a-z0-9.#-]*?)$",
    NULL,   /* place holder for the compiled regex */
    "NURL_maCusATho",
    { 2, 2, 4 },
    0 | 0
};

static struct url_regex NURL_usATho = {
    "^([^-][^:@/]*?)@([a-z0-9#-][a-z0-9.#-]*?)$",
    NULL,   /* place holder for the compiled regex */
    "NURL_usATho",
    { 2, 2, 4 },
    0 | NURL_NO_PROTO
};

static struct url_regex NURL_usCATho = {
    "^([^-][^:@/]*?):@([a-z0-9#-][a-z0-9.#-]*?)$",
    NULL,   /* place holder for the compiled regex */
    "NURL_usCATho",
    { 2, 2, 4 },
    0 | NURL_NO_PASS | NURL_NO_PROTO | NURL_NO_PORT
};

static struct url_regex NURL_usCAThore = {
    "^([^-][^:@/]*?):@([a-z0-9#-][a-z0-9.#-]*?)/(|/*?.+)$",
    NULL,   /* place holder for the compiled regex */
    "NURL_usCAThore",
    { 3, 2, 4, 6 },
    0 | NURL_NO_PASS | NURL_NO_PROTO | NURL_NO_PORT
};

static struct url_regex NURL_usCAThoCpo = {
    "^([^-][^:@/]*?):@([a-z0-9#-][a-z0-9.#-]*?):([0-9]+)$",
    NULL,   /* place holder for the compiled regex */
    "NURL_usCAThoCpo",
    { 3, 2, 4, 5 },
    0 | NURL_NO_PASS | NURL_NO_PROTO
};

static struct url_regex NURL_usCAThoCpore = {
    "^([^-][^:@/]*?):@([a-z0-9#-][a-z0-9.#-]*?):([0-9]+)/(|/*?.+)$",
    NULL,   /* place holder for the compiled regex */
    "NURL_usCAThoCpore",
    { 4, 2, 4, 5, 6 },
    0 | NURL_NO_PASS | NURL_NO_PROTO
};

static struct url_regex NURL_CpaATho = {
    "^:([^/]+?)@([a-z0-9#-][a-z0-9.#-]*?)$",
    NULL,   /* place holder for the compiled regex */
    "NURL_CpaATho",
    { 2, 3, 4 },
    0 | NURL_NO_USER | NURL_NO_PROTO | NURL_NO_PORT
};

static struct url_regex NURL_CpaAThore = {
    "^:([^/]+?)@([a-z0-9#-][a-z0-9.#-]*?)/(|/*?.+)$",
    NULL,   /* place holder for the compiled regex */
    "NURL_CpaAThore",
    { 3, 3, 4, 6 },
    0 | NURL_NO_USER | NURL_NO_PROTO | NURL_NO_PORT
};

static struct url_regex NURL_CpaAThoCpo = {
    "^:([^/]+?)@([a-z0-9#-][a-z0-9.#-]*?):([0-9]+)$",
    NULL,   /* place holder for the compiled regex */
    "NURL_CpaAThoCpo",
    { 3, 3, 4, 5 },
    0 | NURL_NO_USER | NURL_NO_PROTO
};

static struct url_regex NURL_CpaAThoCpore = {
    "^:([^/]+?)@([a-z0-9#-][a-z0-9.#-]*?):([0-9]+)/(|/*?.+)$",
    NULL,   /* place holder for the compiled regex */
    "NURL_CpaAThoCpore",
    { 4, 3, 4, 5, 6 },
    0 | NURL_NO_USER | NURL_NO_PROTO
};

static struct url_regex NURL_usCpaATho = {
    "^([^-][^:@/]*?):([^/]+?)@([a-z0-9#-][a-z0-9.#-]*?)$",
    NULL,   /* place holder for the compiled regex */
    "NURL_usCpaATho",
    { 3, 2, 3, 4 },
    0 | NURL_NO_PROTO | NURL_NO_PORT
};

static struct url_regex NURL_usCpaAThore = {
    "^([^-][^:@/]*?):([^/]+?)@([a-z0-9#-][a-z0-9.#-]*?)/(|/*?.+)$",
    NULL,   /* place holder for the compiled regex */
    "NURL_usCpaAThore",
    { 4, 2, 3, 4, 6 },
    0 | NURL_NO_PROTO | NURL_NO_PORT
};

static struct url_regex NURL_usCpaAThoCpo = {
    "^([^-][^:@/]*?):([^/]+?)@([a-z0-9#-][a-z0-9.#-]*?):([0-9]+)$",
    NULL,   /* place holder for the compiled regex */
    "NURL_usCpaAThoCpo",
    { 4, 2, 3, 4, 5 },
    0 | NURL_NO_PROTO
};

static struct url_regex NURL_usCpaAThoCpore = {
    "^([^-][^:@/]*?):([^/]+?)@([a-z0-9#-][a-z0-9.#-]*?):([0-9]+)/(|/*?.+)$",
    NULL,   /* place holder for the compiled regex */
    "NURL_usCpaAThoCpore",
    { 5, 2, 3, 4, 5, 6 },
    0 | NURL_NO_PROTO
};

static struct url_regex NURL_CATho = {
    "^:@([a-z0-9#-][a-z0-9.#-]*?)$",
    NULL,   /* place holder for the compiled regex */
    "NURL_CATho",
    { 1, 4 },
    0 | NURL_NO_AUTH | NURL_NO_PROTO | NURL_NO_PORT
};

static struct url_regex NURL_CAThore = {
    "^:@([a-z0-9#-][a-z0-9.#-]*?)/(|/*?.+)$",
    NULL,   /* place holder for the compiled regex */
    "NURL_CAThore",
    { 2, 4, 6 },
    0 | NURL_NO_AUTH | NURL_NO_PROTO | NURL_NO_PORT
};

static struct url_regex NURL_CAThoCpo = {
    "^:@([a-z0-9#-][a-z0-9.#-]*?):([0-9]+)$",
    NULL,   /* place holder for the compiled regex */
    "NURL_CAThoCpo",
    { 2, 4, 5 },
    0 | NURL_NO_AUTH | NURL_NO_PROTO
};

static struct url_regex NURL_CAThoCpore = {
    "^:@([a-z0-9#-][a-z0-9.#-]*?):([0-9]+)/(|/*?.+)$",
    NULL,   /* place holder for the compiled regex */
    "NURL_CAThoCpore",
    { 3, 4, 5, 6 },
    0 | NURL_NO_AUTH | NURL_NO_PROTO
};

static struct url_regex NURL_prCusCATho = {
    "^([a-z][a-z0-9]+)://([^-][^:@/]*?):@([a-z0-9#-][a-z0-9.#-]*?)$",
    NULL,   /* place holder for the compiled regex */
    "NURL_prCusCATho",
    { 3, 1, 2, 4 },
    0 | NURL_NO_PASS | NURL_NO_PORT
};

static struct url_regex NURL_prCusCAThore = {
    "^([a-z][a-z0-9]+)://([^-][^:@/]*?):@([a-z0-9#-][a-z0-9.#-]*?)/(|/*?.+)$",
    NULL,   /* place holder for the compiled regex */
    "NURL_prCusCAThore",
    { 4, 1, 2, 4, 6 },
    0 | NURL_NO_PASS | NURL_NO_PORT
};

static struct url_regex NURL_prCusCAThoCpo = {
    "^([a-z][a-z0-9]+)://([^-][^:@/]*?):@([a-z0-9#-][a-z0-9.#-]*?):([0-9]+)$",
    NULL,   /* place holder for the compiled regex */
    "NURL_prCusCAThoCpo",
    { 4, 1, 2, 4, 5 },
    0 | NURL_NO_PASS
};

static struct url_regex NURL_prCusCAThoCpore = {
    "^([a-z][a-z0-9]+)://([^-][^:@/]*?):@([a-z0-9#-][a-z0-9.#-]*?):([0-9]+)/(|/*?.+)$",
    NULL,   /* place holder for the compiled regex */
    "NURL_prCusCAThoCpore",
    { 5, 1, 2, 4, 5, 6 },
    0 | NURL_NO_PASS
};

static struct url_regex NURL_prCCpaATho = {
    "^([a-z][a-z0-9]+)://:([^/]+?)@([a-z0-9#-][a-z0-9.#-]*?)$",
    NULL,   /* place holder for the compiled regex */
    "NURL_prCCpaATho",
    { 3, 1, 3, 4 },
    0 | NURL_NO_USER | NURL_NO_PORT
};

static struct url_regex NURL_prCCpaAThore = {
    "^([a-z][a-z0-9]+)://:([^/]+?)@([a-z0-9#-][a-z0-9.#-]*?)/(|/*?.+)$",
    NULL,   /* place holder for the compiled regex */
    "NURL_prCCpaAThore",
    { 4, 1, 3, 4, 6 },
    0 | NURL_NO_USER | NURL_NO_PORT
};

static struct url_regex NURL_prCCpaAThoCpo = {
    "^([a-z][a-z0-9]+)://:([^/]+?)@([a-z0-9#-][a-z0-9.#-]*?):([0-9]+)$",
    NULL,   /* place holder for the compiled regex */
    "NURL_prCCpaAThoCpo",
    { 4, 1, 3, 4, 5 },
    0 | NURL_NO_USER
};

static struct url_regex NURL_prCCpaAThoCpore = {
    "^([a-z][a-z0-9]+)://:([^/]+?)@([a-z0-9#-][a-z0-9.#-]*?):([0-9]+)/(|/*?.+)$",
    NULL,   /* place holder for the compiled regex */
    "NURL_prCCpaAThoCpore",
    { 5, 1, 3, 4, 5, 6 },
    0 | NURL_NO_USER
};

static struct url_regex NURL_prCusCpaATho = {
    "^([a-z][a-z0-9]+)://([^-][^:@/]*?):([^/]+?)@([a-z0-9#-][a-z0-9.#-]*?)$",
    NULL,   /* place holder for the compiled regex */
    "NURL_prCusCpaATho",
    { 4, 1, 2, 3, 4 },
    0 | NURL_NO_PORT
};

static struct url_regex NURL_prCusCpaAThore = {
    "^([a-z][a-z0-9]+)://([^-][^:@/]*?):([^/]+?)@([a-z0-9#-][a-z0-9.#-]*?)/(|/*?.+)$",
    NULL,   /* place holder for the compiled regex */
    "NURL_prCusCpaAThore",
    { 5, 1, 2, 3, 4, 6 },
    0 | NURL_NO_PORT
};

static struct url_regex NURL_prCusCpaAThoCpo = {
    "^([a-z][a-z0-9]+)://([^-][^:@/]*?):([^/]+?)@([a-z0-9#-][a-z0-9.#-]*?):([0-9]+)$",
    NULL,   /* place holder for the compiled regex */
    "NURL_prCusCpaAThoCpo",
    { 5, 1, 2, 3, 4, 5 },
    0 | 0
};

static struct url_regex NURL_prCusCpaAThoCpore = {
    "^([a-z][a-z0-9]+)://([^-][^:@/]*?):([^/]+?)@([a-z0-9#-][a-z0-9.#-]*?):([0-9]+)/(|/*?.+)$",
    NULL,   /* place holder for the compiled regex */
    "NURL_prCusCpaAThoCpore",
    { 6, 1, 2, 3, 4, 5, 6 },
    0 | 0
};

static struct url_regex NURL_prCCATho = {
    "^([a-z][a-z0-9]+)://:@([a-z0-9#-][a-z0-9.#-]*?)$",
    NULL,   /* place holder for the compiled regex */
    "NURL_prCCATho",
    { 2, 1, 4 },
    0 | NURL_NO_AUTH | NURL_NO_PORT
};

static struct url_regex NURL_prCCAThore = {
    "^([a-z][a-z0-9]+)://:@([a-z0-9#-][a-z0-9.#-]*?)/(|/*?.+)$",
    NULL,   /* place holder for the compiled regex */
    "NURL_prCCAThore",
    { 3, 1, 4, 6 },
    0 | NURL_NO_AUTH | NURL_NO_PORT
};

static struct url_regex NURL_prCCAThoCpo = {
    "^([a-z][a-z0-9]+)://:@([a-z0-9#-][a-z0-9.#-]*?):([0-9]+)$",
    NULL,   /* place holder for the compiled regex */
    "NURL_prCCAThoCpo",
    { 3, 1, 4, 5 },
    0 | NURL_NO_AUTH
};

static struct url_regex NURL_prCCAThoCpore = {
    "^([a-z][a-z0-9]+)://:@([a-z0-9#-][a-z0-9.#-]*?):([0-9]+)/(|/*?.+)$",
    NULL,   /* place holder for the compiled regex */
    "NURL_prCCAThoCpore",
    { 4, 1, 4, 5, 6 },
    0 | NURL_NO_AUTH
};

static struct url_regex NURL_fiCre = {
    "^(file)://(|/*?.+)$",
    NULL,   /* place holder for the compiled regex */
    "NURL_fiCre",
    { 2, 1, 6 },
    0 | NURL_NO_HOST | NURL_NO_PORT
};

static struct url_regex NURL_re = {
    "^/(|/*?.+)$",
    NULL,   /* place holder for the compiled regex */
    "NURL_re",
    { 1, 6 },
    0 | NURL_NO_PROTO | NURL_NO_HOST | NURL_NO_PORT
};

static struct url_regex NURL_Tre = {
    "^~(|/*?.+)$",
    NULL,   /* place holder for the compiled regex */
    "NURL_Tre",
    { 1, 6 },
    0 | NURL_NO_PROTO | NURL_NO_HOST | NURL_NO_PORT
};

static struct url_regex NURL_Dre = {
    "^\\.(|/*?.+)$",
    NULL,   /* place holder for the compiled regex */
    "NURL_Dre",
    { 1, 6 },
    0 | NURL_NO_PROTO | NURL_NO_HOST | NURL_NO_PORT
};

static struct url_regex NURL_SMBma = {
    "^\\\\\\\\([a-z0-9_-][a-z0-9._-]*?)$",
    NULL,   /* place holder for the compiled regex */
    "NURL_SMBma",
    { 1, 4 },
    0 | 0
};

static struct url_regex NURL_SMBmare = {
    "^\\\\\\\\([a-z0-9_-][a-z0-9._-]*?)\\\\(|/*?.+)$",
    NULL,   /* place holder for the compiled regex */
    "NURL_SMBmare",
    { 2, 4, 6 },
    0 | 0
};

static struct url_regex NURL_SMB = {
    "^\\\\\\\\$",
    NULL,   /* place holder for the compiled regex */
    "NURL_SMB",
    { 0,  },
    0 | NURL_NO_HOST
};

/* The array of all the patterns. */
static struct url_regex * all_regexes[49] =
   { 
    &NURL_ho,
    &NURL_hore,
    &NURL_hoCpo,
    &NURL_hoCpore,
    &NURL_prCho,
    &NURL_prChore,
    &NURL_prChoCpo,
    &NURL_prChoCpore,
    &NURL_maCusATho,
    &NURL_usATho,
    &NURL_usCATho,
    &NURL_usCAThore,
    &NURL_usCAThoCpo,
    &NURL_usCAThoCpore,
    &NURL_CpaATho,
    &NURL_CpaAThore,
    &NURL_CpaAThoCpo,
    &NURL_CpaAThoCpore,
    &NURL_usCpaATho,
    &NURL_usCpaAThore,
    &NURL_usCpaAThoCpo,
    &NURL_usCpaAThoCpore,
    &NURL_CATho,
    &NURL_CAThore,
    &NURL_CAThoCpo,
    &NURL_CAThoCpore,
    &NURL_prCusCATho,
    &NURL_prCusCAThore,
    &NURL_prCusCAThoCpo,
    &NURL_prCusCAThoCpore,
    &NURL_prCCpaATho,
    &NURL_prCCpaAThore,
    &NURL_prCCpaAThoCpo,
    &NURL_prCCpaAThoCpore,
    &NURL_prCusCpaATho,
    &NURL_prCusCpaAThore,
    &NURL_prCusCpaAThoCpo,
    &NURL_prCusCpaAThoCpore,
    &NURL_prCCATho,
    &NURL_prCCAThore,
    &NURL_prCCAThoCpo,
    &NURL_prCCAThoCpore,
    &NURL_fiCre,
    &NURL_re,
    &NURL_Tre,
    &NURL_Dre,
    &NURL_SMBma,
    &NURL_SMBmare,
    &NURL_SMB
   };



/* we have to compile each of the regular expressions, and store the
 * compiled version off into it's structure.
 */
void URL_init()
{
  /* stuff that's gonna be used when doing the regex compile */
  int error_ptr;
  const char * error;
  int options;
  int i;

  options = 0 | PCRE_CASELESS;

  /* run down the global array 'all_regexes' and initialize the
   * regex value of each url_regex structure in it.
   */
  for (i = 0; i < 49; i++)
  {
    all_regexes[i]->regex = pcre_compile(all_regexes[i]->pattern,
                  options, &error, &error_ptr, NULL);

    if (all_regexes[i]->regex == NULL)
    {
       fprintf(stderr, "Error in regex '%s'\n   at offset %d: %s.\n", 
               all_regexes[i]->pattern, error_ptr, error);
       exit(2);
    }
  }
}

/* ********************************************************************** */

/* parse the URL into it's various component pieces
 * any string that doesn't match one of the regexes that defined above
 * isn't a url that we can recognize, and we return an error accordingly
 */
struct URL * URL_parse(char * url_string)
{
  struct URL * this_URL;
  int match = 0;
  int offsets[30];
  int string_size;
  int start_position;   /* start position in the array of regexex */
  int i, j, k, m;       /* intermediate counter variables */
  int count;
  char * url_pieces[7];

  /* initialize some stuff to zero */
  start_position = 0;
  bzero(url_pieces, 7 * (sizeof(char *)));
  this_URL = (struct URL *) malloc( sizeof(struct URL) );
  bzero(this_URL, sizeof(struct URL));
  string_size = strlen(url_string);


  /* copy the url string into the URL structure we'll return */
  this_URL->url = malloc( string_size + 1);
  strcpy(this_URL->url, url_string);

  /* search through the all_regexes array */
  if (! match )
  {
    for (i = start_position; i < 49; i++)
    {
      match = pcre_exec(all_regexes[i]->regex, NULL, url_string, string_size,
                        0, offsets, 30);
      if (match > 0)
      {
        printf("Match found! --  Name = %s.\n", all_regexes[i]->name);
        printf("Matching substrings = %d.    ", match - 1 );
        printf("Positions = %d.\n", all_regexes[i]->positions[0]);
        if (match -1 != all_regexes[i]->positions[0])
        {
          printf("ERROR:  match did not have number of positions expectd.\n");
          this_URL->errors |= NURL_PARSE_ERROR;
          return(this_URL);
        }

        j = 2;  /* index into offsets past the whole matched string */
        for (count = 1; count < match; count++)
        {
          printf("Offsets: %d, %d.\n", j, j+1);
          printf("Start: '%d', end: '%d'.\n", offsets[j], offsets[j+1]);
          if ( ! (offsets[j] == -1 || offsets[j+1] == -1) )
          {
            /* prepend the '/' to the resource, we need one more byte for 
             * the piece, so this situation is a bit different than the rest.
             */
            if (all_regexes[i]->positions[count] == 6)
            {
              printf("prepending a '/'.\n");
              url_pieces[ all_regexes[i]->positions[count] ] = 
                                         malloc( offsets[j+1] - offsets[j] +2 );
              url_pieces[ all_regexes[i]->positions[count] ][0] = '/';
              k = 1;
            }
            else
            {
              url_pieces[ all_regexes[i]->positions[count] ] = 
                                         malloc( offsets[j+1] - offsets[j] +1 );
              k = 0;
            }
            printf("Match = '");
            for (m=offsets[j]; m < offsets[j+1]; m++, k++)
            {
              url_pieces[ all_regexes[i]->positions[count] ][k] = url_string[m];
/*              printf("%c", url_string[m]);  */
            }
            url_pieces[ all_regexes[i]->positions[count] ][k] = ' ';
            printf("%s'\n", url_pieces[ all_regexes[i]->positions[count] ]);
          }
          printf("URL piece index: %d.\n", all_regexes[i]->positions[count]);
          printf("Error value: %d.\n", all_regexes[i]->errors);
          j += 2;
        }
        break;
      }
    }
  }

  if (match < 1)
  {
    printf("Error: No matching regex found for '%s'.\n", url_string);
    return(this_URL);
  }
  else
  {
    /* store off the pieces of this match into the URL struct */
    for (k = 1; k <= all_regexes[i]->positions[0]; k++)
    {
      switch(all_regexes[i]->positions[k])
      {
        case NURL_PROTO:
          this_URL->proto = url_pieces[NURL_PROTO];
          break;
        case NURL_USER:
          this_URL->user = url_pieces[NURL_USER];
          break;
        case NURL_PASS:
          this_URL->password = url_pieces[NURL_PASS];
          break;
        case NURL_HOST:
          this_URL->host = url_pieces[NURL_HOST];
          break;
        case NURL_PORT:
          this_URL->port = atoi(url_pieces[NURL_PORT]);
          break;
        case NURL_RESOURCE:
          this_URL->resource = url_pieces[NURL_RESOURCE];
          break;
        default:
          printf("Very wierd position value: %d.\n",  
                  all_regexes[i]->positions[k]);
          break;
      }
    }

    /* store off various other info into the URL struct: name and flags */
    this_URL->name = malloc( strlen(all_regexes[i]->name) + 1);
    strcpy(this_URL->name, all_regexes[i]->name);
    this_URL->errors = all_regexes[i]->errors;
  }

  return(this_URL);
}


/* ********************************************************************** */

/* reclaim the space used by a URL structure */
void URL_free(struct URL * this_url)
{
    if (this_url->url       != NULL)    free(this_url->url);
    if (this_url->proto     != NULL)    free(this_url->proto);
    if (this_url->user      != NULL)    free(this_url->user);
    if (this_url->password  != NULL)    free(this_url->password);
    if (this_url->host      != NULL)    free(this_url->host);
    if (this_url->resource  != NULL)    free(this_url->resource);
    if (this_url->name      != NULL)    free(this_url->name);

    free(this_url);
}


