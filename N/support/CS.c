#include "CS.h"

extern int errno;

void replace(char** prim, char **second)
{
  if( *second != NULL )
  {
    if(*prim) g_free(*prim);
    *prim = g_strdup(*second);
  }
}

void replace_url(URL* url, URL* newurl)
{
  replace(&url->url, &newurl->url);
  replace(&url->proto, &newurl->proto);
  replace(&url->password, &newurl->password);
  replace(&url->host, &newurl->host);
  replace(&url->user, &newurl->user);
  replace(&url->resource, &newurl->resource);
  replace(&url->name, &newurl->name);
}

URL* clone_url(URL* url)
{
  URL* newurl;

  newurl = URL__alloc();
  newurl->url = g_strdup(url->url);
  newurl->proto = g_strdup(url->proto);
  newurl->password = g_strdup(url->password);
  newurl->host = g_strdup(url->host);
  newurl->user = g_strdup(url->user);
  newurl->resource = g_strdup(url->resource);
  newurl->name = g_strdup(url->name);
  newurl->port = url->port;
  return newurl;
}

void Info_print(Info* info)
{
  fprintf(stderr, "url = %s\n", info->url.url);
  fprintf(stderr, "proto = %s\n", info->url.proto);
  fprintf(stderr, "user = %s\n", info->url.user);
  fprintf(stderr, "password = %s\n", info->url.password);
  fprintf(stderr, "host = %s\n", info->url.host);
  fprintf(stderr, "resource = %s\n", info->url.resource);
  fprintf(stderr, "name = %s\n", info->url.name);
  fprintf(stderr, "port = %i\n", info->url.port);
  fprintf(stderr, "errors = %i\n", info->url.errors);

  fprintf(stderr, "perms = %s\n", info->perms);
  fprintf(stderr, "user = %s\n", info->user);
  fprintf(stderr, "group = %s\n", info->group);
  fprintf(stderr, "loc = %i\n", info->loc);

  /*
  fprintf(stderr, "Nmode = %i\n", info->Nmode);
  fprintf(stderr, "Ntype = %i\n", info->Ntype);
  fprintf(stderr, "Nctime = %i\n", info->Nctime);
  fprintf(stderr, "Nmtime = %i\n", info->Nmtime);
  fprintf(stderr, "Nsize = %i\n", info->Nsize);
  */
}

Info* Info_clone(Info* info)
{
  Info * newinfo;
  newinfo = Info__alloc();
  
  newinfo->url.url = g_strdup(info->url.url);
  newinfo->url.proto = g_strdup(info->url.proto);
  newinfo->url.user = g_strdup(info->url.user);
  newinfo->url.password = g_strdup(info->url.password);
  newinfo->url.host = g_strdup(info->url.host);
  newinfo->url.resource = g_strdup(info->url.resource);
  newinfo->url.name = g_strdup(info->url.name);
  newinfo->url.port = info->url.port;
  newinfo->url.errors = info->url.errors;

  newinfo->Nmode = info->Nmode;
  newinfo->Ntype = info->Ntype;
  newinfo->Nctime = info->Nctime;
  newinfo->Nmtime = info->Nctime;
  newinfo->Nsize = info->Nsize;
  
  newinfo->perms = g_strdup(info->perms);
  newinfo->user = g_strdup(info->user);
  newinfo->group = g_strdup(info->group);
  newinfo->loc = info->loc;
  
  return newinfo;
}

InfoList* InfoList_clone(InfoList* list)
{
  int i;
  InfoList* infolist;

  infolist = InfoList__alloc();
  infolist->_buffer = g_new0(Info, list->_length);
  infolist->_length = list->_length;
  infolist->_maximum = list->_maximum;

  for(i=0; i < list->_length; i++)
  {
    infolist->_buffer[i].url.url = g_strdup(list->_buffer[i].url.url);
    infolist->_buffer[i].url.proto = g_strdup(list->_buffer[i].url.proto);
    infolist->_buffer[i].url.user = g_strdup(list->_buffer[i].url.user);
    infolist->_buffer[i].url.password=g_strdup(list->_buffer[i].url.password);
    infolist->_buffer[i].url.host = g_strdup(list->_buffer[i].url.host);
    infolist->_buffer[i].url.resource=g_strdup(list->_buffer[i].url.resource);
    infolist->_buffer[i].url.name = g_strdup(list->_buffer[i].url.name);

    infolist->_buffer[i].url.port = list->_buffer[i].url.port;
    infolist->_buffer[i].url.errors = list->_buffer[i].url.errors;

    infolist->_buffer[i].Nmode = list->_buffer[i].Nmode;
    infolist->_buffer[i].Ntype = list->_buffer[i].Ntype;
    infolist->_buffer[i].Nctime = list->_buffer[i].Nctime;
    infolist->_buffer[i].Nmtime = list->_buffer[i].Nmtime;
    infolist->_buffer[i].Nsize = list->_buffer[i].Nsize;

    infolist->_buffer[i].perms = g_strdup(list->_buffer[i].perms);
    infolist->_buffer[i].user = g_strdup(list->_buffer[i].user);
    infolist->_buffer[i].group = g_strdup(list->_buffer[i].group);
    infolist->_buffer[i].loc = list->_buffer[i].loc;
  }

  return infolist;	
}

/*
This needs to parse out the url, but be care full not to blowaway,
any thing in the url->structure.  This mean if a port is specified, in 
the url->port be sure not to over write it with someting in the url->url
*/
void CS_parse_url(URL* url)
{
  static int inited=0;
  URL* newurl;

  if(inited == 0)
  {
    URL_init();
    inited = 1;
  }

  newurl = URL_parse(url->url);
  
  replace_url(url, newurl);

  if( !(newurl->errors & NURL_NO_PORT) )
  {
    url->port = newurl->port;
  }
  url->errors = newurl->errors;
}

void add_slash(char *path)
  /*Add a slash to the end of the [] if it doesn't already have one.*/
{
  if(strlen(path) == 0)
  {
    return;
  }
   
  if( *(path+strlen(path)-1) != '/' )
  {
    strcat(path, "/");
  }
}

char* ncs_get_default_ior_filename()
{
  char* dir;
  char* file;
  int ret;

  dir =  g_strconcat(g_get_home_dir(), "/.gnome/NCS/", NULL);
  ret = mkdir(dir, 0700);
  if(ret && errno != EEXIST)
  {
    fprintf(stderr, "Can't make dir: %s\n", dir);
  }
  file = g_strconcat(dir, "default-NCS-ior", NULL);
  g_free(dir);
  
  return file;
}

void orb_handle_connection(GIOPConnection *cnx,
                gint source, GdkInputCondition cond)
{
  switch(cond) {
  case GDK_INPUT_EXCEPTION:
    giop_main_handle_connection_exception(cnx);
    break;
  default:
    giop_main_handle_connection(cnx);
  }
}


void orb_add_connection(GIOPConnection *cnx)
{
  cnx->user_data = (gpointer)gtk_input_add_full(GIOP_CONNECTION_GET_FD(cnx),
                                                GDK_INPUT_READ|GDK_INPUT_EXCEPTION,
                                                (GdkInputFunction)orb_handle_connection,
                                                NULL, cnx, NULL);
}

void orb_remove_connection(GIOPConnection *cnx)
{
  gtk_input_remove((guint)cnx->user_data);
  cnx->user_data = (gpointer)-1;
}

