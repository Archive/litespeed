#include <stdio.h>
#include <stdlib.h>	/* For atol() */
#include <stdarg.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <malloc.h>
#include <fcntl.h>
#include <signal.h>
#ifdef SCO_FLAVOR
#include <sys/timeb.h>	/* alex: for struct timeb definition */
#endif /* SCO_FLAVOR */
#include <time.h>
#include <sys/time.h>

#if 0

#include "../src/fs.h"
#include "../src/mad.h"
#include "../src/dir.h"
#include "../src/util.h"
#include "../src/main.h"
#ifndef VFS_STANDALONE
#include "../src/panel.h"
#include "../src/key.h"		/* Required for the async alarm handler */
#include "../src/layout.h"	/* For get_panel_widget and get_other_index */
#endif
#include "vfs.h"
#include "mcfs.h"
#include "names.h"
#include "extfs.h"
#ifdef USE_NETCODE
#   include "tcputil.h"
#endif

#endif /*0*/

#include <pwd.h>
#include <grp.h>

#include "CS.h"

#ifndef TUNMLEN 
#define TUNMLEN 256
#endif
#ifndef TGNMLEN
#define TGNMLEN 256
#endif

static int saveuid = -993;
static char saveuname[TUNMLEN];
static int my_uid = -993;

static int savegid = -993;
static char savegname[TGNMLEN];
static int my_gid = -993;

#define myuid	( my_uid < 0? (my_uid = getuid()): my_uid )
#define	mygid	( my_gid < 0? (my_gid = getgid()): my_gid )

static int current_mday;
static int current_mon;
static int current_year;

int finduid (char *uname)
{
    struct passwd *pw;
    extern struct passwd *getpwnam ();
    
    if (uname[0] != saveuname[0]/* Quick test w/o proc call */
	||0 != strncmp (uname, saveuname, TUNMLEN)) {
	strncpy (saveuname, uname, TUNMLEN);
	pw = getpwnam (uname);
	if (pw) {
	    saveuid = pw->pw_uid;
	} else {
	    saveuid = myuid;
	}
    }
    return saveuid;
}
int findgid (char *gname)
{
    struct group *gr;
    extern struct group *getgrnam ();
    
    if (gname[0] != savegname[0]/* Quick test w/o proc call */
	||0 != strncmp (gname, savegname, TUNMLEN)) {
	strncpy (savegname, gname, TUNMLEN);
	gr = getgrnam (gname);
	if (gr) {
	    savegid = gr->gr_gid;
	} else {
	    savegid = mygid;
	}
    }
    return savegid;
}

#define MAXCOLS 30

static char *columns [MAXCOLS];	/* Points to the string in column n */
static int   column_ptr [MAXCOLS]; /* Index from 0 to the starting positions of the columns */

static int split_text (char *p)
{
    char *original = p;
    int  numcols;


    for (numcols = 0; *p && numcols < MAXCOLS; numcols++){
	while (*p == ' ' || *p == '\r' || *p == '\n'){
	    *p = 0;
	    p++;
	}
	columns [numcols] = p;
	column_ptr [numcols] = p - original;
	while (*p && *p != ' ' && *p != '\r' && *p != '\n')
	    p++;
    }
    return numcols;
}

static int is_num (int idx)
{
    if (columns [idx][0] < '0' || columns [idx][0] > '9')
	return 0;
    return 1;
}

static int is_time (char *str, struct tm *tim)
{
    char *p, *p2;

    if ((p=strchr(str, ':')) && (p2=strrchr(str, ':'))) {
	if (p != p2) {
    	    if (sscanf (str, "%2d:%2d:%2d", &tim->tm_hour, &tim->tm_min, &tim->tm_sec) != 3)
		return (0);
	}
	else {
	    if (sscanf (str, "%2d:%2d", &tim->tm_hour, &tim->tm_min) != 2)
		return (0);
	}
    }
    else 
        return (0);
    
    return (1);
}

static int is_year(char *str, struct tm *tim)
{
    long year;

    if (!strchr(str, ':')) {
	year = atol (str);
	if (year < 1900 || year > 3000)
            return (0);
	tim->tm_year = (int) (year - 1900);
        return (1);
    }
    else 
        return (0);
}

#define free_and_return(x) { free (p_copy); return (x); }
/*int parse_ls_lga (char *p, struct stat *s, char **filename, char**linkname)*/
int parse_ls_lga (char *p, ftpitem* item)
{
    char** filename;
    char** linkname;
    struct stat *s;
    static char *month = "JanFebMarAprMayJunJulAugSepOctNovDec";
    static char *week = "SunMonTueWedThuFriSat";
    char *pos;
    int idx, idx2, num_cols, isconc = 0;
    int i;
    struct tm tim;
    int extfs_format_date = 0;
    char *p_copy;
    
    filename = &item->file;
    linkname = &item->link;
    s = &item->s;
    
    s->st_mode = 0;
    if (strncmp (p, "total", 5) == 0){
        return 0;
    }
    switch (*(p++)){
        case 'd': s->st_mode |= S_IFDIR; break;
        case 'b': s->st_mode |= S_IFBLK; break;
        case 'c': s->st_mode |= S_IFCHR; break;
        case 'm': s->st_mode |= S_IFREG; break; /* Don't know what it is :-) */
        case 'n': s->st_mode |= S_IFREG; break; /* and this as well */
        case 'l': s->st_mode |= S_IFLNK; break;
#ifdef IS_IFSOCK        
        case 's': s->st_mode |= S_IFSOCK; break;
#endif
        case 'p': s->st_mode |= S_IFIFO; break;
        case '-': s->st_mode |= S_IFREG; break;
        case '?': s->st_mode |= S_IFREG; break;
        default: return 0;
    }
    if (*p == '['){
	if (strlen (p) <= 8 || p [8] != ']')
	    return 0;
	/* Should parse here the Notwell permissions :) */
	if (S_ISDIR (s->st_mode))
	    s->st_mode |= (S_IRUSR | S_IRGRP | S_IROTH | S_IWUSR | S_IXUSR | S_IXGRP | S_IXOTH);
	else
	    s->st_mode |= (S_IRUSR | S_IRGRP | S_IROTH | S_IWUSR);
	p += 9;
    } else {
	switch (*(p++)){
	 case 'r': s->st_mode |= 0400; break;
	 case '-': break;
	 default: return 0;
	}
	switch (*(p++)){
	 case 'w': s->st_mode |= 0200; break;
	 case '-': break;
	 default: return 0;
	}
	switch (*(p++)){
	 case 'x': s->st_mode |= 0100; break;
	 case 's': s->st_mode |= 0100 | S_ISUID; break;
	 case 'S': s->st_mode |= S_ISUID; break;
	 case '-': break;
	 default: return 0;
	}
	switch (*(p++)){
	 case 'r': s->st_mode |= 0040; break;
	 case '-': break;
	 default: return 0;
	}
	switch (*(p++)){
	 case 'w': s->st_mode |= 0020; break;
	 case '-': break;
	 default: return 0;
	}
	switch (*(p++)){
	 case 'x': s->st_mode |= 0010; break;
	 case 's': s->st_mode |= 0010 | S_ISGID; break;
	 case 'S': s->st_mode |= S_ISGID; break;
	 case '-': break;
	 default: return 0;
	}
	switch (*(p++)){
	 case 'r': s->st_mode |= 0004; break;
	 case '-': break;
	 default: return 0;
	}
	switch (*(p++)){
	 case 'w': s->st_mode |= 0002; break;
	 case '-': break;
	 default: return 0;
	}
	switch (*(p++)){
	 case 'x': s->st_mode |= 0001; break;
	 case 't': s->st_mode |= 0001 | S_ISVTX; break;
	 case 'T': s->st_mode |= S_ISVTX; break;
	 case '-': break;
	 default: return 0;
	}
    }
	
    p_copy = strdup (p);
    num_cols = split_text (p);

    s->st_nlink = atol (columns [0]);
    if (s->st_nlink <= 0)
        free_and_return (0);

    if (!is_num (1))
	s->st_uid = finduid (columns [1]);
    else
        s->st_uid = (uid_t) atol (columns [1]);

    /* Mhm, the ls -lg did not produce a group field */
    for (idx = 3; idx <= 5; idx++) 
        if ((*columns [idx] >= 'A' && *columns [idx] <= 'S' &&
            strlen (columns[idx]) == 3) || (strlen (columns[idx])==8 &&
            columns [idx][2] == '-' && columns [idx][5] == '-'))
            break;
    if (idx == 6 || (idx == 5 && !S_ISCHR (s->st_mode) && !S_ISBLK (s->st_mode)))
        free_and_return (0);
    if (idx < 5){
        char *p = strchr(columns [idx - 1], ',');
        if (p && p[1] >= '0' && p[1] <= '9')
            isconc = 1;
    }
    if (idx == 3 || (idx == 4 && !isconc && (S_ISCHR(s->st_mode) || S_ISBLK (s->st_mode))))
        idx2 = 2;
    else {
	if (is_num (2))
	    s->st_gid = (gid_t) atol (columns [2]);
	else
	    s->st_gid = findgid (columns [2]);
	idx2 = 3;
    }

    if (S_ISCHR (s->st_mode) || S_ISBLK (s->st_mode)){
        char *p;
	if (!is_num (idx2))
	    free_and_return (0);
#ifdef HAVE_ST_RDEV
	s->st_rdev = (atol (columns [idx2]) & 0xff) << 8;
#endif
	if (isconc){
	    p = strchr (columns [idx2], ',');
	    if (!p || p [1] < '0' || p [1] > '9')
	        free_and_return (0);
	    p++;
	} else {
	    p = columns [idx2 + 1];
	    if (!is_num (idx2+1))
	        free_and_return (0);
	}
	
#ifdef HAVE_ST_RDEV
	s->st_rdev |= (atol (p) & 0xff);
#endif
	s->st_size = 0;
    } else {
	if (!is_num (idx2))
	    free_and_return (0);
	
	s->st_size = (size_t) atol (columns [idx2]);
#ifdef HAVE_ST_RDEV
	s->st_rdev = 0;
#endif
    }

    /* Let's setup default time values */
    tim.tm_year = current_year;
    tim.tm_mon  = current_mon;
    tim.tm_mday = current_mday;
    tim.tm_hour = 0;
    tim.tm_min  = 0;
    tim.tm_sec  = 0;
    tim.tm_isdst = 0;
    
    p = columns [idx++];
    
    if((pos=strstr(week, p)) != NULL){
        tim.tm_wday = (pos - week)/3;
	p = columns [idx++];
	}

    if((pos=strstr(month, p)) != NULL)
        tim.tm_mon = (pos - month)/3;
    else {
        /* This case should not normaly happen, but in extfs we allow these
           date formats:
           Mon DD hh:mm
           Mon DD YYYY
           Mon DD YYYY hh:mm
	   Wek Mon DD hh:mm:ss YYYY
           MM-DD-YY hh:mm
           where Mon is Jan-Dec, DD, MM, YY two digit day, month, year,
           YYYY four digit year, hh, mm two digit hour and minute. */

        if (strlen (p) == 8 && p [2] == '-' && p [5] == '-'){
            p [2] = 0;
            p [5] = 0;
            tim.tm_mon = (int) atol (p);
            if (!tim.tm_mon)
                free_and_return (0)
            else
                tim.tm_mon--;
            tim.tm_mday = (int) atol (p + 3);
            tim.tm_year = (int) atol (p + 6);
            if (tim.tm_year < 70)
                tim.tm_year += 70;
            extfs_format_date = 1;
        } else
            free_and_return (0);
    }

    if (!extfs_format_date){
        if (!is_num (idx))
	    free_and_return (0);
        tim.tm_mday = (int)atol (columns [idx++]);
    }
    
    if (is_num (idx)) {
        if(is_time(columns[idx], &tim) || is_year(columns[idx], &tim)) {
	idx++;
	 
	if(is_num (idx) && 
	    (is_year(columns[idx], &tim) || is_time(columns[idx], &tim)))
	    idx++; /* time & year or reverse */
	 } /* only time or date */
    }
    else 
        free_and_return (0); /* Nor time or date */

    /* Use resultimg time value */    
    s->st_mtime = mktime (&tim);
    if (s->st_mtime == -1)
        s->st_mtime = 0;
    s->st_atime = s->st_mtime;
    s->st_ctime = s->st_mtime;
    s->st_dev = 0;
    s->st_ino = 0;
#ifdef HAVE_ST_BLKSIZE
    s->st_blksize = 512;
#endif
#ifdef HAVE_ST_BLOCKS
    s->st_blocks = (s->st_size + 511) / 512;
#endif

    for (i = idx + 1, idx2 = 0; i < num_cols; i++ ) 
	if (strcmp (columns [i], "->") == 0){
	    idx2 = i;
	    break;
	}
    
    if (((S_ISLNK (s->st_mode) || 
        (num_cols == idx + 3 && s->st_nlink > 1))) /* Maybe a hardlink? (in extfs) */
        && idx2){
	int p;
	char *s;
	    
	if (filename){
	    p = column_ptr [idx2] - column_ptr [idx];
	    /*s = malloc (p, "filename");*/
	    s = (char*)malloc(p);
	    strncpy (s, p_copy + column_ptr [idx], p - 1);
	    s[p - 1] = '\0';
	    *filename = s;
	}
	if (linkname){
	    s = strdup (p_copy + column_ptr [idx2+1]);
	    p = strlen (s);
	    if (s [p-1] == '\r' || s [p-1] == '\n')
		s [p-1] = 0;
	    if (s [p-2] == '\r' || s [p-2] == '\n')
		s [p-2] = 0;
		
	    *linkname = s;
	}
    } else {
	/* Extract the filename from the string copy, not from the columns
	 * this way we have a chance of entering hidden directories like ". ."
	 */
	if (filename){
	    /* 
	    *filename = strdup (columns [idx++]);
	    */
	    int p;
	    char *s;
	    
	    s = strdup (p_copy + column_ptr [idx++]);
	    p = strlen (s);
	    if (s [p-1] == '\r' || s [p-1] == '\n')
	        s [p-1] = 0;
	    if (s [p-2] == '\r' || s [p-2] == '\n')
		s [p-2] = 0;
	    
	    *filename = s;
	}
	if (linkname)
	    *linkname = NULL;
    }
    free_and_return (1);
}
