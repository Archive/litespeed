#include <sys/param.h>
#include <NCS.h>
#include <support/url.h>
/*#include "../NFTP/NFTP.h"*/
#include <sys/types.h>
#include <regex.h>
#include <stdio.h>
#include <sys/stat.h>
#include <dirent.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <pwd.h>
#include <ctype.h>
#include <sys/types.h>
#include <regex.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/param.h>
#include <dirent.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <pwd.h>
#include <ctype.h>
#include <limits.h>
#include <strings.h>
#include <fcntl.h>
#include <sys/times.h>
#include <sys/mman.h>

#include <orb/orbit.h>
#include <glib.h>
#include <gdk/gdk.h>

#define NCS_DIRECTORY ( 1 << 0 )
#define NCS_ALIAS     ( 1 << 1 )
#define NCS_REGULAR   ( 1 << 2 )
#define NCS_IRREGULAR ( 1 << 3 )
#define NCS_STREAMING ( 1 << 4 )

#define N_EOF     ( -1 )
#define N_BADDESC ( -2 )

#define LSLINESIZE 34  /*Found by finding the smallest possible ls -lag line*/

typedef struct 
{
  struct stat s;
  char* file;
  char* link;
  char* perms;
} ftpitem;


InfoList* InfoList_clone(InfoList* list);
Info* Info_clone(Info* info);
URL* clone_url(URL* url);

char* ncs_get_default_ior_filename();

void CS_parse_url(URL* url);
void add_slash(char *path); 

void orb_remove_connection(GIOPConnection *cnx);
void orb_add_connection(GIOPConnection *cnx);

