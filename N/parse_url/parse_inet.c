/* parse an internet, or local, URL */

#include "url.h"


struct URL * parse_inet(char * url)
{
  struct URL * this_url;
  int length;
  int i; 
  

  this_url = (struct URL) malloc( sizeof(struct URL) );
  i = 0; 


  /* check for a begining 'URL:' on the url string */
  if ( strncasecmp(url, "URL:", 4) == 0)  /* begins with "URL:" */
  {
    url += 4;
  }

  /* calculate the size of the url string */
  length = strlen(url);

  /* if the string starts with letters follwed by a colon, then that's the
   * protocol, or the username
  */
  for (i = 0; i < length; )
  {
    if ( isalpha(url[i]) )
    {
       i ++;
       continue;
    }
    if (url[i] == ':') 
       break;
  }
  
