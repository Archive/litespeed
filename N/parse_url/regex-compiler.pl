#!/usr/bin/perl
#  regex-compiler.pl
#
# usage:
#  regex-compiler.pl <pattern-file> <regex-file>
#
# script for building the regular expressions necessary to match URL
# patterns.
#
# Author: Scott Wimer <scottw@cgibuilder.com>
# Date: 1998 09 27
#
######################################################################


use strict;


### check that we were called properly
usage()  if (@ARGV != 2);
###

### open the input and output files
open(IN, "<$ARGV[0]") || die "read failed on '$ARGV[0]': $!\n";
open(REGEX, ">$ARGV[1]") || die "write failed on '$ARGV[1]': $!\n";
###

### Here's the translation database:
my %Tdb = (
            proto    =>  "([a-z][a-z0-9]+)",
            user     =>  "([^-][^:@/]*?)",
            pass     =>  "([^/]+?)",
            host     =>  "([a-z0-9#-][a-z0-9.#-]*?)",
            machine  =>  "([a-z0-9_-][a-z0-9._-]*?)",
            port     =>  "([0-9]+)",
            resource =>  "(|/*?.+)",
            mailto   =>  "mailto",
            file     =>  "(file)",
          );
###

### add a bit of header code that will let us require in the file full of
##- regular expressions.
print REGEX "\@", "Regexes = (\n";
###

### run down the input line, building regexes for each pattern line
my $line;
while ( chomp($line=<IN>) )
{
  my $regex = '';

  ### skip blank and comment lines
  next if ( length($line) == 0 );
  next if ($line=~/^#/);
  ###

  ### Split the line on the first whitespace, this lets us define the
  ##- error flags for missing parts of the URL from within the source
  ##- that we're reading.
  my ($flags, @error_flags);
  ($line, $flags) = split /\s+/, $line, 2;
  @error_flags = split /\s+|\s*,\s*/, $flags;
  my $error_flag = join " | ", @error_flags;
  ###

  ### make a name for each pattern type
  my $a_name = $line;
  $a_name =~s!(\w+)!substr($1, 0, 2)!eg;   # first 2 letters of a word
  $a_name =~s!:!C!g;                       # colons -> C
  $a_name =~s!\@!AT!g;                     # @ -> AT
  $a_name =~s!~!T!g;                       # ~ -> T
  $a_name =~s!\.!D!g;                      # . -> D
  $a_name =~s!\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\!SMB!g;      # // -> SMB
  $a_name =~s!\W!!g;                       # blow away everything else
  $a_name = "NURL_$a_name";                # prepend 'NURL_' to the name
  ###

  ### build an array of name $a_name that has the number and positions,
  ##- (in the general overall url format) that this particular pattern
  ##- will match.
  ##- we're building an array of size 7 here (1st position is for the total
  ##- number of matches made) to hold the place number of each matched
  ##- subpattern.
  ##-  Here's how the place numberings work out:
  ##- proto     - 1
  ##- file      - 1   -- A specific protocol, handled differently --
  ##- user      - 2
  ##- pass      - 3
  ##- host      - 4
  ##- machine   - 4   -- specific for SMB stuff --
  ##- port      - 5
  ##- resource  - 6 
  my $array_def;
  my @positions;
  my $ct = 0;
  if ($line =~/proto/)    { push @positions, 1; $ct++; }
  if ($line =~/file/)     { push @positions, 1; $ct++; }
  if ($line =~/user/)     { push @positions, 2; $ct++; }
  if ($line =~/pass/)     { push @positions, 3; $ct++; }
  if ($line =~/host/)     { push @positions, 4; $ct++; }
  if ($line =~/machine/)  { push @positions, 4; $ct++; }
  if ($line =~/port/)     { push @positions, 5; $ct++; }
  if ($line =~/resource/) { push @positions, 6; $ct++; }
  $array_def .= "{ $ct, ";
  $array_def .= join ", ", @positions;
  $array_def .= " }";
  ###

  ### set regex to equal the line
  ##- then, change all words in it to something it found in the %Tdb
  $regex = $line;
  $regex =~s!(\w+)!$Tdb{$1}!eg;
  ###

  ### write the regex to the regex file
  print REGEX qq! [ "$a_name",\n!,
                "   '^$regex\$',\n",
              qq!   "$array_def",\n!,
              qq!   "$error_flag",\n!,
              qq! ],\n!;
  ###
}
###

print REGEX ");\n1;\n";

close(IN);
close(REGEX);




### print usage information
sub usage
{
  print STDERR "$0: <pattern-file> <regex-file>\n";
  exit(0);
}
###
