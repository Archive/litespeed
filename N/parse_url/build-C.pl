#!/usr/local/bin/perl
# build-C.pl
#
#  spit out the C source for doing the regex comparisons to break a URL into
#  it's component pieces
#
# The basic approach for this is:
#  build several url_regex structures with the following format:
#    struct url_regex {
#      char * pattern;      /* the non-compiled regular expression */
#      pcre * regex;        /* the compiled regex */
#      char * name;         /* the variable name for this regex */
#      int positions[7];    /* array of match positions in the url string */
#      int errors;          /* for oring error flags into */
#    };
#
#
# The functions: URL_init(), URL_parse(), and URL_free() are produced by this 
# program.
#
# The regular expressions used come from the data file in the first command
# line argument.  The output file is given as the second command line 
# arguement.
# Usage:  build-C.pl <regex structures file> <output file>
#
# Author: Scott Wimer <scottw@cgibuilder.com>
# Date: 1998 10 04
#
######################################################################

### define some variables up here where they'll be easy to find
use vars qw($VERSION $options @Regexes);
$VERSION = '0.0.1';
$options = 'PCRE_CASELESS';
###

use strict;

require 5.003;   # minimal daily requirements for thier perl

### check command line arguments
unless (@ARGV == 2)
{
  print "Usage:  build-C.pl <regex structures file> <output file>\n";
  exit(1);
}
###

### bring in the regex structures file
require $ARGV[0]  || die "load of $ARGV[0] failed: $!\n";
###

### open C code file for output
open (CODE, ">$ARGV[1]") || die "can't write to $ARGV[1]: $!\n";
###


### print some header stuff to the code file
print CODE << "CODE";
/*  --- This is a generated file! ---
 * All modifications will be lost after the next run of build-C.pl.
 * See the file README for information on how to rebuild your URL_parse
 * lib from scratch.
 * Generally, you won't need to modify this file.
 */

#include "url.h"

/* a structure for holding the regex and assorted data for it */
struct url_regex {
  char * pattern;      /* the non-compiled regular expression */
  pcre * regex;        /* the compiled regex */
  char * name;         /* the variable name for this regex */
  int positions[7];    /* array of match positions in the url string */
  int errors;          /* for oring error flags into */
};

CODE
###


### build one url_regex structure for each pattern that we pulled in
##- via require.
my ($pat_ref);
my (@all_regexes);
foreach  $pat_ref (@Regexes)
{
  ### build an array of all the regex names
  push @all_regexes, $pat_ref->[0];
  ###

  $pat_ref->[3] = "0" unless $pat_ref->[3];

  print CODE << "CODE";
static struct url_regex $pat_ref->[0] = {
    "$pat_ref->[1]",
    NULL,   /* place holder for the compiled regex */
    "$pat_ref->[0]",
    $pat_ref->[2],
    0 | $pat_ref->[3]
};

CODE
}
###

### make the all_regexes array contain the ADDRESS OF a url_regex pattern
##- structure
foreach (@all_regexes) { $_ = "&$_"; }
###

my $all_size = @all_regexes;

print CODE
"/* The array of all the patterns. */
static struct url_regex * all_regexes[$all_size] =
   { \n    ", join (",\n    ", @all_regexes), "\n   };\n\n";


### The Guts of the library: URL_parse(), URL_init(), URL_free()
print CODE << "CODE";


/* we have to compile each of the regular expressions, and store the
 * compiled version off into it's structure.
 */
void URL_init()
{
  /* stuff that's gonna be used when doing the regex compile */
  int error_ptr;
  const char * error;
  int options;
  int i;

  options = 0 | $options;

  /* run down the global array 'all_regexes' and initialize the
   * regex value of each url_regex structure in it.
   */
  for (i = 0; i < $all_size; i++)
  {
    all_regexes[i]->regex = pcre_compile(all_regexes[i]->pattern,
                  options, &error, &error_ptr);

    if (all_regexes[i]->regex == NULL)
    {
       fprintf(stderr, "Error in regex '%s'\\n   at offset %d: %s.\\n", 
               all_regexes[i]->pattern, error_ptr, error);
       exit(2);
    }
  }
}

/* ********************************************************************** */

/* parse the URL into it's various component pieces
 * any string that doesn't match one of the regexes that defined above
 * isn't a url that we can recognize, and we return an error accordingly
 */
struct URL * URL_parse(char * url_string)
{
  struct URL * this_URL;
  int match = 0;
  int offsets[30];
  int string_size;
  int start_position;   /* start position in the array of regexex */
  int i, j, k, m;       /* intermediate counter variables */
  int count;
  char * url_pieces[7];

  /* initialize some stuff to zero */
  start_position = 0;
  bzero(url_pieces, 7 * (sizeof(char *)));
  this_URL = (struct URL *) malloc( sizeof(struct URL) );
  bzero(this_URL, sizeof(struct URL));
  string_size = strlen(url_string);


  /* copy the url string into the URL structure we'll return */
  this_URL->url = malloc( string_size + 1);
  strcpy(this_URL->url, url_string);

  /* search through the all_regexes array */
  if (! match )
  {
    for (i = start_position; i < $all_size; i++)
    {
      match = pcre_exec(all_regexes[i]->regex, NULL, url_string, string_size,
                        0, offsets, 30);
      if (match > 0)
      {
        printf("Match found! --  Name = %s.\\n", all_regexes[i]->name);
        printf("Matching substrings = %d.    ", match - 1 );
        printf("Positions = %d.\\n", all_regexes[i]->positions[0]);
        if (match -1 != all_regexes[i]->positions[0])
        {
          printf("ERROR:  match did not have number of positions expectd.\\n");
          this_URL->errors |= NURL_PARSE_ERROR;
          return(this_URL);
        }

        j = 2;  /* index into offsets past the whole matched string */
        for (count = 1; count < match; count++)
        {
          printf("Offsets: %d, %d.\\n", j, j+1);
          printf("Start: '%d', end: '%d'.\\n", offsets[j], offsets[j+1]);
          if ( ! (offsets[j] == -1 || offsets[j+1] == -1) )
          {
            /* prepend the '/' to the resource, we need one more byte for 
             * the piece, so this situation is a bit different than the rest.
             */
            if (all_regexes[i]->positions[count] == 6)
            {
              printf("prepending a '/'.\\n");
              url_pieces[ all_regexes[i]->positions[count] ] = 
                                         malloc( offsets[j+1] - offsets[j] +2 );
              url_pieces[ all_regexes[i]->positions[count] ][0] = '/';
              k = 1;
            }
            else
            {
              url_pieces[ all_regexes[i]->positions[count] ] = 
                                         malloc( offsets[j+1] - offsets[j] +1 );
              k = 0;
            }
            printf("Match = '");
            for (m=offsets[j]; m < offsets[j+1]; m++, k++)
            {
              url_pieces[ all_regexes[i]->positions[count] ][k] = url_string[m];
/*              printf("%c", url_string[m]);  */
            }
            url_pieces[ all_regexes[i]->positions[count] ][k] = '\0';
            printf("%s'\\n", url_pieces[ all_regexes[i]->positions[count] ]);
          }
          printf("URL piece index: %d.\\n", all_regexes[i]->positions[count]);
          printf("Error value: %d.\\n", all_regexes[i]->errors);
          j += 2;
        }
        break;
      }
    }
  }

  if (match < 1)
  {
    printf("Error: No matching regex found for '%s'.\\n", url_string);
    return(this_URL);
  }
  else
  {
    /* store off the pieces of this match into the URL struct */
    for (k = 1; k <= all_regexes[i]->positions[0]; k++)
    {
      switch(all_regexes[i]->positions[k])
      {
        case NURL_PROTO:
          this_URL->proto = url_pieces[NURL_PROTO];
          break;
        case NURL_USER:
          this_URL->user = url_pieces[NURL_USER];
          break;
        case NURL_PASS:
          this_URL->password = url_pieces[NURL_PASS];
          break;
        case NURL_HOST:
          this_URL->host = url_pieces[NURL_HOST];
          break;
        case NURL_PORT:
          this_URL->port = atoi(url_pieces[NURL_PORT]);
          break;
        case NURL_RESOURCE:
          this_URL->resource = url_pieces[NURL_RESOURCE];
          break;
        default:
          printf("Very wierd position value: %d.\\n",  
                  all_regexes[i]->positions[k]);
          break;
      }
    }

    /* store off various other info into the URL struct: name and flags */
    this_URL->name = malloc( strlen(all_regexes[i]->name) + 1);
    strcpy(this_URL->name, all_regexes[i]->name);
    this_URL->errors = all_regexes[i]->errors;
  }

  return(this_URL);
}


/* ********************************************************************** */

/* reclaim the space used by a URL structure */
void URL_free(struct URL * this_url)
{
    if (this_url->url       != NULL)    free(this_url->url);
    if (this_url->proto     != NULL)    free(this_url->proto);
    if (this_url->user      != NULL)    free(this_url->user);
    if (this_url->password  != NULL)    free(this_url->password);
    if (this_url->host      != NULL)    free(this_url->host);
    if (this_url->resource  != NULL)    free(this_url->resource);
    if (this_url->name      != NULL)    free(this_url->name);

    free(this_url);
}


CODE
###


