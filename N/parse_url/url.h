#include <ctype.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "pcre/pcre.h"
#include <sys/errno.h>
#include "../NCS.h"

/* --- Error and Warning Conditions --- */
#define  NURL_EMPTY         0x0001  /* Empty url passed. */
#define  NURL_NO_TYPE       0x0002  /* No Type specified */
#define  NURL_BAD_TYPE      0x0004  /* Unknown Type; not Inet, SMB, or local. */
#define  NURL_BAD_PROTO     0x0008  /* Bad Protocol name. */
#define  NURL_NO_PROTO      0x0010  /* No Protocol name specified. */
#define  NURL_NO_HOST       0x0020  /* No Host specified. */
#define  NURL_BAD_PORT      0x0040  /* Bad Port number. */
#define  NURL_NO_PORT       0x0080  /* No Port number specified. */
#define  NURL_NO_RESOURCE   0x0100  /* No Resource specified. */
#define  NURL_NO_AUTH       0x0200  /* Uname and Passwd expected; not given. */
#define  NURL_NO_USER       0x0400  /* Username expected, but not specified. */
#define  NURL_NO_PASS       0x0800  /* Password expected, but not specified. */
#define  NURL_PARSE_ERROR   0x1000  /* Error Parsing url. */
#define  NURL_NO_MEM        0x2000  /* Ran out of memory while parsing */


/* --- #defines for members of the URL structure filled from the regex */
#define NURL_PROTO     1
#define NURL_USER      2
#define NURL_PASS      3
#define NURL_HOST      4
#define NURL_PORT      5
#define NURL_RESOURCE  6



/* IDL definition of a URL
struct URL
{
  string url;
  string proto;
  string user;
  string password;
  string host;
  string resource;
  short  port;
  string name;
  int    errors;
};
*/

/* C implementaion of IDL for URL struct 
struct URL {
  char * url;
  char * proto;
  char * user;
  char * password;
  char * host;
  char * resource;
  short  port;
  char * name;
  int errors;
};
*/

struct URL *   URL_parse(char * url);
struct URL *   parse_smb(char * url,  struct URL * this_url);
struct URL *   parse_inet(char * url, struct URL * this_url);
struct URL *   parse_file(char * url, struct URL * this_url);
void           URL_free(struct URL * this_url);
void           URL_init(void);
