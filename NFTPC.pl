#!/usr/bin/perl

##Make sure we start with fresh copy of the impl.c file.

#print `orbit-idl -Eskeleton_impl NCS.idl`;
print `orbit-idl -Eskeleton_impl N/NFTPC.idl`;

open (IMPL_H, ">NFTPC-impl.h");
open (IMPL_C, "<NFTPC-impl.c");
open (TEMP_IMPL_C, ">TEMP-impl.c");

$in_impl = 0;
$here = 0;

print TEMP_IMPL_C "
#include \"NFTPC-impl.h\"
                  ";


while( <IMPL_C> )
{
  s/static//g;
  s/\(gpointer\).impl_NCC/(gpointer)&impl_NFTPC/;

  if(/epv structures/)
  {
    $in_impl = 1;
  }

  if(!$in_impl)
  {
    print IMPL_H $_;

    if(/App-specific servant structures/)
    {
      print IMPL_H "
#include <glib.h>
                        ";
    }

    if(/CORBA_char \* attr_response/)
    {
      print IMPL_H "
                       ";
     $here = 1;
    }
  }
  else
  {
    print TEMP_IMPL_C $_;
  }
}


print TEMP_IMPL_C "
NFTPC impl_NFTPC__create_mine(  
        PortableServer_POA poa, 
        impl_POA_NFTPC **servant,
        CORBA_Environment *ev)
{
  NFTPC retval;
  impl_POA_NFTPC *newservant;
  PortableServer_ObjectId *objid;
  newservant = g_new0(impl_POA_NFTPC, 1);
  newservant->servant.vepv = &impl_NFTPC_vepv;
  newservant->poa = poa;
  POA_NFTPC__init((PortableServer_Servant)newservant, ev);
  objid = PortableServer_POA_activate_object(poa, newservant, ev);
  CORBA_free(objid);
  retval = PortableServer_POA_servant_to_reference(poa, newservant, ev);
  *servant = newservant;
  return retval;
}
";

`mv TEMP-impl.c NFTPC-impl.c`;
