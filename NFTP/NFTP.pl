#!/usr/bin/perl

##Make sure we start with fresh copy of the impl.c file.

print `orbit-idl -Eskeleton_impl NFTP.idl`;

open (NFTP_IMPL_H, ">NFTP-impl.h");
open (NFTP_IMPL_C, "<NFTP-impl.c");
open (TEMP_IMPL_C, ">TEMP-NFTP-impl.c");

$in_impl = 0;
$here = 0;

print TEMP_IMPL_C "
#include \"NFTP-impl.h\"
                  ";

while( <NFTP_IMPL_C> )
{
  s/static//g;
  s/\(gpointer\).impl_NCS_Loc/(gpointer)&impl_NFTPLoc/;

  if(/Stub implementations/)
  {
    $in_impl = 1;
  }

  if(!$in_impl)
  {

    if(/App-specific servant structures/)
    {
      print NFTP_IMPL_H "
#include <glib.h>
#include \"NCC.h\"
#include <pthread.h>
#include \"ftplib.h\"
                        ";
    }

    if(/} impl_POA_NCS_Loc;/ || /} impl_POA_NFTPLoc;/)
    {
      print NFTP_IMPL_H "

	URL*		url;
  	char* 		resource;

        int             working;
        int             done;

        /*My own ftp stuff*/
        netbuf          *control;
        netbuf          *dataconnect;
        pthread_mutex_t *control_mx;

        char            *tempfile;
        NCC             ncc;
                       ";
      $here = 1;
    }

    print NFTP_IMPL_H $_;
  }
  else
  {
    print TEMP_IMPL_C $_;
  }
}

`mv TEMP-NFTP-impl.c NFTP-impl.c`;
