#ifndef NFTP_SERVER_H
#define NFTP_SERVER_H

typedef struct _n_read_desc n_read_desc;
typedef struct _n_write_desc n_write_desc;

struct _n_read_desc
{
  char* local;  /*Local file name*/
  int fd;       /*Local file descriptor*/
  int whence;   /*current whence type*/
  int seek;     /*current seek loc*/
  int done;     /*Done reading file from remore host*/
  int id;       /*id of the current n_read_desc*/
};

/*Calling convention structs*/
typedef struct _nc_getmetadata_pt_st nc_getmetadata_pt_st;
typedef struct _nc_setmetadata_pt_st nc_setmetadata_pt_st;

typedef struct _nc_open_pt_st nc_open_pt_st;
typedef struct _nc_close_pt_st nc_close_pt_st;

typedef struct _nc_startread_pt_st nc_startread_pt_st;
typedef struct _nc_startwrite_pt_st nc_startwrite_pt_st;

typedef struct _nc_read_pt_st nc_read_pt_st;
typedef struct _nc_write_pt_st nc_write_pt_st;

typedef struct _nc_list_pt_st nc_list_pt_st;

typedef struct _nc_create_pt_st nc_create_pt_st;
typedef struct _nc_delete_pt_st nc_delete_pt_st;

typedef struct _nc_current_pt_st nc_current_pt_st;
typedef struct _nc_aliases_pt_st nc_aliases_pt_st;

typedef struct _nc_pause_pt_st nc_pause_pt_st;
typedef struct _nc_resume_pt_st nc_resume_pt_st;
typedef struct _nc_abort_pt_st nc_abort_pt_st;

typedef struct _nc_newlocation_pt_st nc_newlocation_pt_st;
typedef struct _nc_newmetadata_pt_st nc_newmetadata_pt_st;

typedef struct _nc_getrecur_pt_st nc_getrecur_pt_st;
typedef struct _nc_putrecur_pt_st nc_putrecur_pt_st;


struct _nc_open_pt_st
{
  impl_POA_NCS_Connection * servant;
  CORBA_Object handle;
  URL * url;
  CORBA_char * passwd;
  CORBA_long pointer;
};

struct _nc_close_pt_st
{
};

struct _nc_read_pt_st
{
   impl_POA_NFTPLoc * servant;
   CORBA_long id;
   CORBA_long pointer;
};

struct _nc_startread_pt_st
{
   impl_POA_NFTPLoc * servant;
   CORBA_char* resource;
   CORBA_long pointer;

   n_read_desc* desc;
};

struct _nc_write_pt_st
{
   impl_POA_NFTPLoc * servant;
   CORBA_long id;
   Buffer *buf;
   CORBA_long pointer;
};

struct _nc_list_pt_st
{
   impl_POA_NFTPLoc* servant;
   CORBA_char * resource;
   CORBA_long pointer;
   InfoList *list;
   
   int dont_callback;;
};

struct _nc_create_pt_st
{
   impl_POA_NFTPLoc * servant;
   CORBA_char * resource;
   CORBA_long pointer;
};

struct _nc_delete_pt_st
{
   impl_POA_NFTPLoc * servant;
   CORBA_char * resource;
   CORBA_long pointer;
};

struct _nc_getrecur_pt_st
{
   impl_POA_NFTPLoc * servant;
   Info* localcwd;
   InfoList * list;
   CORBA_long pointer;
};

struct _nc_putrecur_pt_st
{
   impl_POA_NFTPLoc * servant;
   Info* remotecwd;
   InfoList * list;
   CORBA_long pointer;
};

struct _nc_newlocation_pt_st
{
   impl_POA_NFTPLoc * servant;
   CORBA_char * resource;
   CORBA_long pointer;
};

#endif /*NFTP_SEVER_H*/
