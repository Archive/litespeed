#include "../N/support/CS.h"
#include <libgnorba/gnorba.h>
#include <gtk/gtk.h>
#include <orb/orbit.h>
#include <stdio.h>
#include <sys/errno.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <sys/time.h>
#include <sys/unistd.h>
#include <string.h>

/*For calling the stub functions for the callback.*/
#include "NCC.h"

/*Implement the functions defined in the NFTP.idl*/
#include "NFTP.h"
#include "NFTP-impl.h"
#include "NFTP-server.h"

#define FTPLIB_BUFSIZ 8192
#define ACCEPT_TIMEOUT 30

#define FTPLIB_CONTROL 0
#define FTPLIB_READ 1
#define FTPLIB_WRITE 2

typedef void* (*PT_FUNC) (void*);
void nlfs_pt_list(nc_list_pt_st* list_pt_st);

NCS_Connection NFTP = CORBA_OBJECT_NIL;
NCS_Connection NCS = CORBA_OBJECT_NIL;

CORBA_ORB orb;
PortableServer_POA poa;
CORBA_Environment ev;

GHashTable* read_hash;
GHashTable* write_hash;

int main(int argc, char** argv)
{
  char *retval;
  char IOR[4000];
  FILE* host_ior_fp;
  PortableServer_ObjectId NCSID = {0, sizeof("NFTP"), "NFTP"};
  char *name = "NFTP";                     
  int n = 1;                     
  
  IIOPAddConnectionHandler = orb_add_connection;
  IIOPRemoveConnectionHandler = orb_remove_connection;

  read_hash  = g_hash_table_new(g_int_hash, g_int_equal);
  write_hash = g_hash_table_new(g_int_hash, g_int_equal);

  CORBA_exception_init(&ev);
/*
  orb = gnome_CORBA_init("NFTP", NULL, &argc, argv,
	  0, &ev);
*/

  orb = CORBA_ORB_init(&n, &name /* dummy */, "orbit-local-orb", &ev);

  poa = (PortableServer_POA)
    CORBA_ORB_resolve_initial_references(orb, "RootPOA", &ev);
  PortableServer_POAManager_activate
    (PortableServer_POA__get_the_POAManager(poa, &ev), &ev);
  
  NFTP = impl_NCS_Connection__create(poa,  &ev);
  
  host_ior_fp = fopen(ncs_get_default_ior_filename(), "r");
  fscanf(host_ior_fp, "%s", IOR);
  fclose(host_ior_fp);

  NCS = CORBA_ORB_string_to_object(orb, IOR, &ev);

  fprintf(stderr, "Trying to register...\n");
  NCS_Connection_Register(NCS, NFTP, "ftp", "passwd", &ev);
  fprintf(stderr, "Registered with NCS.\n");

  gtk_main ();
  gdk_exit (0);
}


/*********************************************************
 * impl_NCS_Connection_Open and nftp_pt_open
 *
 * Create a socket object that can be used in susequent
 * requests of the server.
 *
 * If the protocol is handled by a different ORB, connect
 * to that ORB and use NCS_Connection_Open to get the 
 * socket.
 *
 * This ORB handles ftp:// requests.
 * We need to authenticate the user, before handing out a
 * socket. (update:we let gnorba do that for us, for now.)
 *
 *********************************************************/

nftp_pt_open(nc_open_pt_st* open_pt_st)
{
  CORBA_Environment ev;
  NCS_Loc loc;
  PortableServer_ObjectId *objid;
  impl_POA_NFTPLoc *newservant;

  char lhost[MAXHOSTNAMELEN+6]; // host:port,  port could be big
  char buffer[MAXHOSTNAMELEN+6]; // host:port,  port could be big

  int urlok = 1; /*Good for now.*/

  /*CS_parse_url(open_pt_st->url);*/

  /*
  fprintf("st->passwd = %s\n", open_pt_st->passwd);

  fprintf("url->url = %s\n", open_pt_st->url->url);
  fprintf("url->resource = %s\n", open_pt_st->url->resource);
  fprintf("url->proto = %s\n", open_pt_st->url->proto);
  fprintf("url->host = %s\n", open_pt_st->url->host);
  fprintf("url->user = %s\n", open_pt_st->url->user);
  fprintf("url->password = %s\n", open_pt_st->url->password);
  fprintf("url->port = %i\n", open_pt_st->url->port);
  */

  strcpy(buffer, open_pt_st->url->host);
  strcat(buffer, ":");
  sprintf(lhost, "%s%i", buffer, open_pt_st->url->port);

  /*
  fprintf("lhost = %s\n", lhost);
  */
  
  if(1) /* can we handle this url? */
  {
    newservant = g_new0(impl_POA_NFTPLoc, 1);
    newservant->servant.vepv = &impl_NFTPLoc_vepv;
    newservant->poa = poa;
    POA_NFTPLoc__init((POA_NFTPLoc*)newservant, &ev);
    objid = PortableServer_POA_activate_object(poa, newservant, &ev);
    CORBA_free(objid);

    loc = PortableServer_POA_servant_to_reference(poa, newservant, &ev);

    newservant->resource = g_strdup(open_pt_st->url->resource);
    newservant->url = clone_url(open_pt_st->url);
    
    /*Do work here*/
    newservant->control_mx = g_new0(pthread_mutex_t, 1);
    pthread_mutex_init(newservant->control_mx, NULL);

    FtpConnect(lhost, &newservant->control);
    FtpLogin(open_pt_st->url->user, open_pt_st->url->password, 
	    newservant->control);
    fprintf(stderr, "resp = %s\n", FtpLastResponse(newservant->control) );
    /*Done with work*/
  }
  
  newservant->ncc = open_pt_st->handle;

  fprintf(stderr, "making callback call for the Open_cb\n");
  NCC_Open_cb(open_pt_st->handle, 1, loc, open_pt_st->pointer, &ev);
  fprintf(stderr, "done making callback call for the Open_cb\n");
}

void
impl_NCS_Connection_Open(
        impl_POA_NCS_Connection * servant,
	CORBA_Object handle,
        URL * url,
        CORBA_char* passwd,
        CORBA_long pointer,
        CORBA_Environment * ev)
{
  pthread_t* threadptr;
  nc_open_pt_st* open_pt_st;
  pthread_attr_t d_attr;

  threadptr = g_new0(pthread_t,1);
  open_pt_st = g_new0(nc_open_pt_st,1);
  open_pt_st->url = URL__alloc();

  /*
  fprintf(stderr, "NCC IOR = %s\n", handle);
  */

  open_pt_st->servant = servant;
  open_pt_st->handle = handle;
  open_pt_st->url = clone_url(url);
  open_pt_st->passwd = passwd;
  open_pt_st->pointer = pointer;
  

  /*
  pthread_setdetachstate(&d_attr, PTHREAD_CREATE_DETACHED);
  pthread_create(threadptr, &d_attr,
                 (PT_FUNC)nftp_pt_open,
                 (void*) open_pt_st);
   */
  pthread_create(threadptr, NULL,
                 (PT_FUNC)nftp_pt_open,
                 (void*) open_pt_st);

  fprintf(stderr, "returning from impl_Open\n");
}

void
nftp_pt_newloc(nc_newlocation_pt_st* newloc_pt_st)
{
  CORBA_Environment ev;

  NCS_Loc loc;
  impl_POA_NFTPLoc *newservant;
  impl_POA_NFTPLoc *servant = newloc_pt_st->servant;
  
  newservant = g_new0(impl_POA_NFTPLoc, 1);
  newservant->servant.vepv = &impl_NFTPLoc_vepv;
  newservant->poa = poa;
  POA_NFTPLoc__init((PortableServer_Servant) newservant, &ev);
  loc = PortableServer_POA_servant_to_reference(poa, newservant, &ev);


  newservant->resource = g_strdup(newloc_pt_st->resource);
  newservant->url = newloc_pt_st->servant->url;
  newservant->control = newloc_pt_st->servant->control;

  NCC_NewLocation_cb(servant->ncc, 1, loc, newloc_pt_st->pointer, &ev);
}

	       
void
impl_NFTPLoc_NewLocation(impl_POA_NFTPLoc *servant,
		    CORBA_char * resource,
		    CORBA_long pointer,
		    CORBA_Environment *ev)
{
  pthread_t* threadptr;
  nc_newlocation_pt_st* newloc_pt_st;
  pthread_attr_t d_attr;

  threadptr = g_new0(pthread_t,1);
  newloc_pt_st = g_new0(nc_newlocation_pt_st,1);

  newloc_pt_st->servant = servant;
  newloc_pt_st->resource = g_strdup(resource);
  newloc_pt_st->pointer = pointer;

  /*
  pthread_setdetachstate(&d_attr, PTHREAD_CREATE_DETACHED);
  pthread_create(threadptr, NULL,
                 (PT_FUNC)nftp_pt_newloc,
                 (void*) newloc_pt_st);
   */

  pthread_create(threadptr, NULL,
                 (PT_FUNC)nftp_pt_newloc,
                 (void*) newloc_pt_st);
}

void
nftp_pt_read(nc_read_pt_st* read_pt_st)
{
  CORBA_Environment ev;

  char* resp;
  char *data;

  int fd;
  n_read_desc* desc;
  fd_set set;
  struct timeval tlen;

  int len, offset;
  int current, size;
  int count=0;
  int ret;

  impl_POA_NFTPLoc* servant;
  Buffer* buf;

  buf = Buffer__alloc();
  buf->_release = 1;

  servant = read_pt_st->servant;
  desc = g_hash_table_lookup(read_hash, (gpointer)read_pt_st->id);
  
  if(!desc)
  {
    NCC_Read_cb(servant->ncc, N_BADDESC, buf, read_pt_st->pointer, &ev);
  }
  
  fd = desc->fd;
  
  /*Make sure the user doesn't read beyond file.*/
  current = lseek(fd, 0, SEEK_CUR);
  size = lseek(fd, 0, SEEK_END);
  /*reset current seek position*/
  lseek(fd, current, SEEK_SET);

  /*
     Whence and seek.  we will worry about these later
     if at all.
   */
  if(desc->whence == SEEK_SET && desc->seek > size)
  {
    /*error*/
  }
  if(desc->whence == SEEK_END && desc->seek+size > size)
  {
    /*error*/
  }
  if(desc->whence == SEEK_CUR && desc->seek+current > size)
  {
    /*error*/
  }
  /*lseek(fd, desc->seek, desc->whence);*/

  data = g_new(char, FTPLIB_BUFSIZ);
  if(desc->seek < size)
  {
    len = read(fd, data, FTPLIB_BUFSIZ);
  }
  else
  {
    tlen.tv_sec = 1;
    while(1)
    {
      FD_ZERO(&set);
      FD_SET(fd, &set);
      select(fd, &set, NULL, NULL, &tlen);
      if( FD_ISSET(fd, &set) || (count > 2) )
      {
	len = read(fd, data, FTPLIB_BUFSIZ);
	break;
      }
      count++;
    }
  }

  if(desc->done)
  {
    desc->seek = lseek(fd, 0, SEEK_CUR);  /*Find size and current*/
    size = lseek(fd, 0, SEEK_END);      /*of file's seek*/
    lseek(fd, desc->seek, SEEK_SET);

    if(len == EOF && desc->seek == size)
    {
      ret = N_EOF;
    }
  }
  else
  {
    ret = len;
  }

  buf->_maximum = len;
  buf->_length = len;
  buf->_buffer = data;
  buf->_release = 1;
  
  NCC_Read_cb(servant->ncc, ret, buf, read_pt_st->pointer, &ev);
}

/**************************************************************
 * impl_NFTPLoc_Read
 * 
 * Read data from the file that the actual ftp code is writing
 * or has written to disk.
 *
 **************************************************************/
void
impl_NFTPLoc_Read(impl_POA_NFTPLoc * servant,
	CORBA_long id,
	CORBA_long pointer,
	CORBA_Environment * ev)
{
  pthread_t* threadptr;
  nc_read_pt_st* read_pt_st;
  pthread_attr_t d_attr;

  threadptr = g_new0(pthread_t,1);
  read_pt_st = g_new0(nc_read_pt_st,1);

  read_pt_st->servant = servant;
  read_pt_st->id = id;
  read_pt_st->pointer = pointer;

  /*
  pthread_setdetachstate(&d_attr, PTHREAD_CREATE_DETACHED);
  pthread_create(threadptr, &d_attr,
                 (PT_FUNC)nftp_pt_read,
                 (void*)read_pt_st);
  */
  pthread_create(threadptr, NULL,
	  (PT_FUNC)nftp_pt_read,
	  (void*)read_pt_st);
}
/*Done with READ code.*/

void
nftp_pt_startread(nc_startread_pt_st* startread_pt_st)
{
  CORBA_Environment ev;

  n_read_desc* desc = startread_pt_st->desc;
  impl_POA_NFTPLoc *servant = startread_pt_st->servant;
  
  NCC_StartRead_cb(servant->ncc, 1, desc->id, desc->local, 
	  startread_pt_st->pointer, &ev);

  pthread_mutex_lock(servant->control_mx); 
  FtpGet(desc->local, startread_pt_st->resource, FTPLIB_IMAGE, 
	  servant->control);
  pthread_mutex_unlock(servant->control_mx);
  desc->done = TRUE;
}

void
impl_NFTPLoc_StartRead(impl_POA_NFTPLoc *servant,
	CORBA_char * resource,
	CORBA_long pointer,
	CORBA_Environment *ev)
{
  n_read_desc* desc;

  pthread_t* threadptr;
  nc_startread_pt_st* startread_pt_st;
  pthread_attr_t d_attr;

  desc = g_new0(n_read_desc, 1);

  desc->id = GPOINTER_TO_INT(desc); /*key the desc into desc->id*/
  desc->local = g_strdup("/tmp/tmp.nftp.XXXXXX");
  desc->fd = mkstemp(desc->local);  /*local now changed*/
  g_hash_table_insert(read_hash, (gpointer)desc->id, (gpointer)desc);

  threadptr = g_new0(pthread_t,1);
  startread_pt_st = g_new0(nc_startread_pt_st,1);

  startread_pt_st->servant = servant;
  startread_pt_st->resource = g_strdup(resource);
  startread_pt_st->pointer = pointer;

  startread_pt_st->desc = desc;

  /*
     pthread_setdetachstate(&d_attr, PTHREAD_CREATE_DETACHED);
     pthread_create(threadptr, &d_attr,
     (PT_FUNC)nftp_pt_read,
     (void*)read_pt_st);
   */
  pthread_create(threadptr, NULL,
	  (PT_FUNC)nftp_pt_startread,
	  (void*)startread_pt_st);
}
/*Done with STARTREAD code.*/

/**************************************************************
 *
 *
 *
 **************************************************************/
void
nftp_pt_write(nc_write_pt_st* write_pt_st)
{
  CORBA_Environment ev;

  int len,fd;
  impl_POA_NFTPLoc * servant;
  Buffer *buf;

  
  servant = write_pt_st->servant;
  buf = write_pt_st->buf;
  
  write(fd, buf->_buffer, buf->_length);
  /*
  printf("buf->_buffer, %s\n", buf->_buffer);
  printf("buf->_length, %i\n", buf->_length);
  fflush(stdout);
  */
}

void
impl_NFTPLoc_Write(impl_POA_NFTPLoc * servant,
	CORBA_long id,
	Buffer * buf,
	CORBA_long pointer,
	CORBA_Environment * ev)
{       
  pthread_t* threadptr;
  nc_write_pt_st* write_pt_st;
  pthread_attr_t d_attr;

  threadptr = g_new0(pthread_t,1);
  write_pt_st = g_new0(nc_write_pt_st,1);

  write_pt_st->servant = servant;
  write_pt_st->buf = buf;
  write_pt_st->pointer = pointer;

  /*
  pthread_setdetachstate(&d_attr, PTHREAD_CREATE_DETACHED);
  pthread_create(threadptr, &d_attr,
                 (PT_FUNC)nftp_pt_write,
                 (void*)write_pt_st);
  */
  pthread_create(threadptr, NULL,
                 (PT_FUNC)nftp_pt_write,
                 (void*)write_pt_st);

}

void
nftp_pt_list(nc_list_pt_st* list_pt_st)
{
  CORBA_Environment ev;
  char tempfile[MAXPATHLEN];
  char concatfile[MAXPATHLEN];
  char *local, *remote;
  char* dirlist, *curpos, *temp;
  ftpitem *listing;
  int i=0, ret, fd, guess, count=0;
  struct stat dirstat;
  impl_POA_NFTPLoc * servant;
  InfoList *list;
  int size_in_bytes;

  servant = list_pt_st->servant;

  fprintf(stderr, "in nftp_pt_list\n");

  sprintf(tempfile, "%stmp.%i", "/tmp/ftp.", servant->control);
  servant->tempfile = g_strdup(tempfile);

  list = InfoList__alloc();
  list->_maximum = 0;
  list->_length = 0;
  list->_buffer = NULL;
  list->_release = 0;

  fprintf(stderr, "tempfile =  %s\n", tempfile);
  fprintf(stderr, "resource =  %s\n", list_pt_st->resource);

  pthread_mutex_lock(servant->control_mx);
  FtpDir(tempfile, list_pt_st->resource, servant->control);
  pthread_mutex_unlock(servant->control_mx);

  fprintf(stderr, "resp = %s\n", FtpLastResponse(servant->control) );

  /*Open dirlist for reading*/
  fd = open(tempfile, O_RDWR);
  fstat(fd, &dirstat);
  dirlist = mmap(0, dirstat.st_size+1, PROT_READ|PROT_WRITE, 
		 MAP_FILE|MAP_PRIVATE, fd, 0);

  curpos = dirlist;
  guess = dirstat.st_size/LSLINESIZE;
  listing = g_new0(ftpitem, guess);

  printf("guess %i\n", guess);
  fflush(stdout);

  if(guess)
  {
    while(dirlist+ dirstat.st_size > curpos)
    {

      temp = strchr(curpos, '\n');
      *temp = '\0';
      ret = parse_ls_lga(curpos, &listing[i]);

      fprintf(stderr, "filename = %s, i = %i\n", listing[i].file, i);

      curpos = temp+1;

      if(ret == 0)
	continue;  /*Don't increment, just try the next line in dirlist*/
      i++;
    }
  }
  
  /*Fill the NCS_LocList and ItemList*/
  list->_buffer = g_new0(Info, i+1);
  list->_maximum = 0;
  list->_length = i;
  for(count=0; count < i; count++)
  {
    size_in_bytes += 4;
    list->_buffer[count].perms = g_strdup("");
    list->_buffer[count].user = g_strdup("");
    list->_buffer[count].group =g_strdup("");
    list->_buffer[count].loc = NFTP;

    list->_buffer[count].url.url = servant->url->url;
    list->_buffer[count].url.proto = servant->url->proto;
    list->_buffer[count].url.user = servant->url->user;
    list->_buffer[count].url.password = servant->url->password;
    list->_buffer[count].url.host = servant->url->host;

    list->_buffer[count].url.name = listing[count].file;
    list->_buffer[count].url.resource = g_strconcat(list_pt_st->resource,
	    "/", listing[count].file, NULL);
    
    list->_buffer[count].url.port = servant->url->port;
    list->_buffer[count].url.errors = servant->url->errors;

    size_in_bytes += strlen(servant->url->url);
    size_in_bytes += strlen(servant->url->proto);
    size_in_bytes += strlen(servant->url->user);
    size_in_bytes += strlen(servant->url->password);
    size_in_bytes += strlen(servant->url->host);
    size_in_bytes += strlen(listing[count].file);
    size_in_bytes += strlen(list->_buffer[count].url.resource);
    size_in_bytes += 8;

    if(listing[count].s.st_mode & S_IFREG)
    {
      list->_buffer[count].Ntype = NCS_REGULAR;
    }
    if(listing[count].s.st_mode & S_IFDIR)
    {
      list->_buffer[count].Ntype = NCS_DIRECTORY;
    }

    size_in_bytes += 8;

    /*Info_print( &(list->_buffer[count]) );*/
    fprintf(stderr, "name = %s, i = %i\n", list->_buffer[count].url.name, 
	    count);
    
    /*need to set the metadata about each location.*/
  }
  
  fprintf(stderr, "size_in_bytes = %i\n", size_in_bytes);
  
  if(list_pt_st->dont_callback)
  { /*called internally*/
    list_pt_st->list = list;
  }
  else
  {
    NCC_List_cb(servant->ncc, 1, list, list_pt_st->pointer, &ev);
  }
}

void
impl_NFTPLoc_List(impl_POA_NFTPLoc* servant,
	CORBA_char* resource,
	CORBA_long pointer,
	CORBA_Environment* ev)
{
  pthread_t* threadptr;
  nc_list_pt_st* list_pt_st;
  pthread_attr_t d_attr;

  fprintf(stderr, "resource = %s\n", resource);
  fflush(stderr);
  
  threadptr = g_new0(pthread_t,1);
  list_pt_st = g_new0(nc_list_pt_st,1);

  list_pt_st->servant = servant;
  list_pt_st->resource = g_strdup(resource);
  list_pt_st->pointer = pointer;

  /*
  pthread_setdetachstate(&d_attr, PTHREAD_CREATE_DETACHED);
  pthread_create(threadptr, &d_attr,
                 (PT_FUNC)nftp_pt_list,
                 (void*)list_pt_st);
		 */
  fprintf(stderr, "in impl_NFTPLoc_List\n");
  pthread_create(threadptr, NULL,
                 (PT_FUNC)nftp_pt_list,
                 (void*)list_pt_st);
  fprintf(stderr, "ending impl_NFTPLoc_List\n");
}

void
nftp_pt_create(nc_create_pt_st* create_pt_st)
{
  int len;
  impl_POA_NFTPLoc * servant;

  servant = create_pt_st->servant;
  len = strlen(servant->resource);

  printf("resource[len] = %c\n", servant->resource[len-1]);

  if(servant->resource[len-1] == '/')
  {
    printf("resource = %s\n", servant->resource);
    fflush(stdout);
    pthread_mutex_lock(servant->control_mx);
    FtpMkdir(servant->resource, servant->control);
    pthread_mutex_unlock(servant->control_mx);
  }
  else
  {
    /*
       @ create empty file
       @ upload the empty file
     */       
  }
}

void
impl_NFTPLoc_Create(impl_POA_NFTPLoc * servant,
                    CORBA_char * resource, 
                    CORBA_long pointer,
                    CORBA_Environment * ev)
{       
  pthread_t* threadptr;
  nc_create_pt_st* create_pt_st;
  pthread_attr_t d_attr;

  threadptr = g_new0(pthread_t,1);
  create_pt_st = g_new0(nc_create_pt_st,1);

  create_pt_st->servant = servant;
  create_pt_st->resource = resource;
  create_pt_st->pointer = pointer;

  /*
  pthread_setdetachstate(&d_attr, PTHREAD_CREATE_DETACHED);
  pthread_create(threadptr, &d_attr,
                 (PT_FUNC)nftp_pt_create,
                 (void*)create_pt_st);
  */
  pthread_create(threadptr, NULL,
                 (PT_FUNC)nftp_pt_create,
                 (void*)create_pt_st);
  
}
void
nftp_pt_delete(nc_delete_pt_st* delete_pt_st)
{
  CORBA_Environment ev;

  CORBA_long retval;
  impl_POA_NFTPLoc* servant;
  int len;

  servant = delete_pt_st->servant;

  len = strlen(servant->resource);

  fprintf(stderr, "resource[len] = %c\n", servant->resource[len-1]);
  
  if(servant->resource[len-1] == '/')
  {
    /*
       @ Probably sould recursively rmdir the directory
     */	

    printf("resource = %s\n", servant->resource);
    fflush(stdout);
    pthread_mutex_lock(servant->control_mx);
    FtpRmdir(servant->resource, servant->control);
    pthread_mutex_unlock(servant->control_mx);
  }
  else
  {
    pthread_mutex_lock(servant->control_mx);
    FtpDelete(servant->resource, servant->control);
    pthread_mutex_unlock(servant->control_mx);
  }
}

void
impl_NFTPLoc_Delete(impl_POA_NFTPLoc * servant, 
	CORBA_char * resource,
	CORBA_long pointer,
	CORBA_Environment * ev)
{
  pthread_t* threadptr;
  nc_delete_pt_st* delete_pt_st;
  pthread_attr_t d_attr;

  threadptr = g_new0(pthread_t,1);
  delete_pt_st = g_new0(nc_delete_pt_st,1);

  delete_pt_st->servant = servant;
  delete_pt_st->pointer = pointer;

  /*
  pthread_setdetachstate(&d_attr, PTHREAD_CREATE_DETACHED);
  pthread_create(threadptr, &d_attr,
                 (PT_FUNC)nftp_pt_delete,
                 (void*)delete_pt_st);
  */
  pthread_create(threadptr, NULL,
                 (PT_FUNC)nftp_pt_delete,
                 (void*)delete_pt_st);


}

void    
impl_NFTPLoc__set_resource(impl_POA_NFTPLoc * servant,
                                      CORBA_char * value,
                                      CORBA_Environment * ev)
{       
  if(servant->resource)
  {
    free(servant->resource);
  }
  servant->resource = g_strdup(value);
  printf("resource = %s\n", servant->resource);
}

void nftp_pt_getrecur(nc_getrecur_pt_st* getrecur_pt_st)
{
  CORBA_Environment ev;

  nc_list_pt_st *list_pt_st;
  int mask, i;
  InfoList *list;
  impl_POA_NFTPLoc* servant;

  Info* localcwd;
  char* local, *remote, *localdir;
  char tempfile[MAXPATHLEN];
  
  
  static int number =0;

  number++;
  fprintf(stderr, "in nftp_pt_getrecur %i times.\n", number);

  servant = getrecur_pt_st->servant;
  list = getrecur_pt_st->list;
  localcwd = getrecur_pt_st->localcwd;
  localdir = localcwd->url.resource;
  
  if(localcwd->Ntype == NCS_DIRECTORY)
  {
    mkdir(localdir, 0755);
  }

  sprintf(tempfile, "/tmp/ftp.tmp.%i", servant->control);
  servant->tempfile = g_strdup(tempfile);

  fprintf(stderr, "tempfile = %s\n", tempfile);
  fprintf(stderr, "list = %i, list->_length = %i\n", list, list->_length);

  /*
  fprintf(stderr, "localcwd = %s\n", localcwd);
  */

  for(i=0; i < list->_length ; i++)
  {
    fprintf(stderr, "url.resource = %s\n", list->_buffer[i].url.resource);

    remote = g_strdup(list->_buffer[i].url.resource);

    /*
    strcat(local, list->_buffer[i].name);
    strcat(remote, list->_buffer[i].name);
    */

    fprintf(stderr, "localdir = %s\n", localdir);
    fprintf(stderr, "remote = %s\n", remote);

    if(list->_buffer[i].Ntype == NCS_DIRECTORY)
    {
      char* list_resource;
      list_pt_st = g_new0(nc_list_pt_st,1);

      list_resource = g_strconcat(remote, "/",
	      list->_buffer[i].url.name, NULL);	

      fprintf(stderr, "resource = %s\n", list_resource);
      list_pt_st->servant = servant;
      list_pt_st->resource = list_resource;
      list_pt_st->pointer = NULL;
      list_pt_st->list = NULL;
      list_pt_st->dont_callback = 1;

      nftp_pt_list(list_pt_st);
      getrecur_pt_st->list = list_pt_st->list;
      
      getrecur_pt_st->localcwd = g_new0(Info, 1);
      getrecur_pt_st->localcwd->Ntype = NCS_DIRECTORY;
      getrecur_pt_st->localcwd->url.resource = g_strconcat(localdir, "/", 
	      list->_buffer[i].url.name, NULL);
      
      fprintf(stderr, "url.resource = %s", 
	      getrecur_pt_st->localcwd->url.resource);
      nftp_pt_getrecur(getrecur_pt_st);
    }
    else if(list->_buffer[i].Ntype == NCS_REGULAR)
    {
      if(1/*FIXME*/)
      {
	local = g_strconcat(localdir, "/", list->_buffer[i].url.name, NULL);

	pthread_mutex_lock(servant->control_mx);
	FtpGet(local, remote, FTPLIB_IMAGE, servant->control);
	pthread_mutex_unlock(servant->control_mx);

	fprintf(stderr, "local = %s, remote = %s\n", local, remote);
      }
      else
      {
	 /*File is up to date on local system; no need to get the file*/
      }
    }
  }
}


void
impl_NFTPLoc_GetRecur(impl_POA_NFTPLoc * servant,
	Info* localcwd,   
	InfoList * list,
	CORBA_long pointer,
	CORBA_Environment * ev)
{                      
  pthread_t* threadptr;
  nc_getrecur_pt_st* getrecur_pt_st;
  pthread_attr_t d_attr;

  threadptr = g_new0(pthread_t,1);
  getrecur_pt_st = g_new0(nc_getrecur_pt_st,1);

  getrecur_pt_st->servant = servant;
  getrecur_pt_st->localcwd = Info_clone(localcwd);
  getrecur_pt_st->list = InfoList_clone(list);
  getrecur_pt_st->pointer = pointer;
  
  fprintf(stderr, "list->_length = %i\n", list->_length);
  fprintf(stderr, "url.resource = %s\n", localcwd->url.resource);

  /*
  pthread_setdetachstate(&d_attr, PTHREAD_CREATE_DETACHED);
  pthread_create(threadptr, &d_attr,
                 (PT_FUNC)nftp_pt_getrecur,
                 (void*)getrecur_pt_st);
		 */
  fprintf(stderr, "in impl_NFTPLoc_GetRecur\n");
  pthread_create(threadptr, NULL,
                 (PT_FUNC)nftp_pt_getrecur,
                 (void*)getrecur_pt_st);
  fprintf(stderr, "exiting impl_NFTPLoc_GetRecur\n");

}

void nftp_pt_putrecur(nc_putrecur_pt_st* putrecur_pt_st)
{
  CORBA_Environment ev;

  nc_list_pt_st *list_pt_st;
  int mask, i;
  InfoList *list;
  impl_POA_NFTPLoc* servant;

  Info* remotecwd;
  char* local, *remote, *remotedir;
  char tempfile[MAXPATHLEN];
  
  
  static int number =0;

  number++;
  fprintf(stderr, "in nftp_pt_putrecur %i times.\n", number);

  servant = putrecur_pt_st->servant;
  list = putrecur_pt_st->list;
  remotecwd = putrecur_pt_st->remotecwd;
  remotedir = remotecwd->url.resource;
  
  if(remotecwd->Ntype == NCS_DIRECTORY)
  {
    pthread_mutex_lock(servant->control_mx);
    FtpMkdir(remotedir, servant->control);
    pthread_mutex_unlock(servant->control_mx);
  }

  sprintf(tempfile, "/tmp/ftp.tmp.%i", servant->control);
  servant->tempfile = g_strdup(tempfile);

  fprintf(stderr, "tempfile = %s\n", tempfile);
  fprintf(stderr, "list = %i, list->_length = %i\n", list, list->_length);

  for(i=0; i < list->_length ; i++)
  {
    fprintf(stderr, "url.resource = %s\n", list->_buffer[i].url.resource);

    remote = g_strdup(list->_buffer[i].url.resource);

    fprintf(stderr, "remotedir = %s\n", remotedir);
    fprintf(stderr, "remote = %s\n", remote);

    if(list->_buffer[i].Ntype == NCS_DIRECTORY)
    {
      char* save_resource;
      list_pt_st = g_new0(nc_list_pt_st,1);

      save_resource = servant->resource;
      servant->resource = g_strconcat(servant->resource, "/",
	      list->_buffer[i].url.name, NULL);	

      fprintf(stderr, "resource = %s\n", servant->resource);
      list_pt_st->servant = servant;
      list_pt_st->pointer = NULL;
      list_pt_st->list = NULL;
      list_pt_st->dont_callback = 1;

      nlfs_pt_list(list_pt_st);
      putrecur_pt_st->list = list_pt_st->list;
      
      putrecur_pt_st->remotecwd = g_new0(Info, 1);
      putrecur_pt_st->remotecwd->Ntype = NCS_DIRECTORY;
      putrecur_pt_st->remotecwd->url.resource = g_strconcat(remotedir, "/", 
	      list->_buffer[i].url.name, NULL);
      
      fprintf(stderr, "url.resource = %s", putrecur_pt_st->remotecwd->url.resource);
      nftp_pt_putrecur(putrecur_pt_st);
      servant->resource = save_resource;
    }
    else if(list->_buffer[i].Ntype == NCS_REGULAR)
    {
      if(1/*FIXME: only if out of date*/)
      {
	remote = g_strconcat(remotedir, "/", list->_buffer[i].url.name, NULL);
	FtpPut(local, remote, FTPLIB_IMAGE, servant->control);
	fprintf(stderr, "local = %s, remote = %s\n", local, remote);
      }
      else
      {
	 /*File is up to date on local system; no need to put the file*/
      }
    }

  }/*done proccesing for loop*/

}

void
impl_NFTPLoc_PutRecur(impl_POA_NFTPLoc * servant,
	Info* remotecwd,
	InfoList * list,
	CORBA_long pointer,
	CORBA_Environment * ev)
{
  pthread_t* threadptr;
  nc_putrecur_pt_st* putrecur_pt_st;
  pthread_attr_t d_attr;

  threadptr = g_new0(pthread_t,1);
  putrecur_pt_st = g_new0(nc_putrecur_pt_st,1);

  putrecur_pt_st->servant = servant;
  putrecur_pt_st->remotecwd = remotecwd;
  putrecur_pt_st->list = list;
  putrecur_pt_st->pointer = pointer;

  /*
  pthread_setdetachstate(&d_attr, PTHREAD_CREATE_DETACHED);
  pthread_create(
	  threadptr, 
	  &d_attr,
	  (PT_FUNC)nftp_pt_putrecur,
	  (void*)putrecur_pt_st);
   */

  pthread_create(
	  threadptr, 
	  NULL,
	  (PT_FUNC)nftp_pt_putrecur,
	  (void*)putrecur_pt_st);

}

void
nlfs_pt_list(nc_list_pt_st* list_pt_st)
{
  CORBA_Environment ev;

  char tempfile[MAXPATHLEN];
  char concatfile[MAXPATHLEN];
  char *local, *remote;
  char* dirlist, *curpos, *temp;
  ftpitem *listing;
  int i=0, ret, fd, guess, count=0;
  struct stat dirstat;
  impl_POA_NFTPLoc * servant;
  InfoList *list;
  char command[MAXPATHLEN];


  servant = list_pt_st->servant;
  /*
  local = servant->local;
  local = servant->remote;
  */

  fprintf(stderr, "in nlfs_pt_list\n");

  sprintf(tempfile, "%stmp.%i", "/tmp/nlfs.", servant);
  servant->tempfile = g_strdup(tempfile);

  list = InfoList__alloc();
  list->_maximum = 0;
  list->_length = 0;
  list->_buffer = NULL;
  list->_release = 0;
  
  printf("tempfile =  %s\n", tempfile);

  strcpy(command, "ls -Alg ");
  strcat(command, servant->resource);
  strcat(command, " > ");
  strcat(command, tempfile);
  
  system(command);

  /*Open dirlist for reading*/
  fd = open(tempfile, O_RDWR);
  fstat(fd, &dirstat);
  dirlist = mmap(0, dirstat.st_size+1, PROT_READ|PROT_WRITE, 
		 MAP_FILE|MAP_PRIVATE, fd, 0);

  curpos = dirlist;
  guess = dirstat.st_size/LSLINESIZE;
  listing = g_new0(ftpitem, guess);

  printf("guess %i\n", guess);
  fflush(stdout);

  if(guess)
  {
    while(dirlist+ dirstat.st_size > curpos)
    {

      temp = strchr(curpos, '\n');
      *temp = '\0';
      ret = parse_ls_lga(curpos, &listing[i]);

      fprintf(stderr, "filename = %s, i = %i\n", listing[i].file, i);

      curpos = temp+1;

      if(ret == 0)
	continue;  /*Don't increment, just try the next line in dirlist*/
      i++;
    }
  }
  
  /*Fill the NCS_LocList and ItemList*/
  list->_buffer = g_new0(Info, i+1);
  list->_maximum = 0;
  list->_length = i;
  for(count=0; count < i; count++)
  {
    
    /*strcpy(concatfile, remote);
    sprintf(concatfile, "%s/%s", concatfile, listing[count].file);
    printf("concat = %s", concatfile);
    printf("resource = %s", remote);
    */

    /*
    list->_buffer[count].loc =
      my_NewLoc(servant, concatfile);
     */

    list->_buffer[count].url.name = listing[count].file;
    list->_buffer[count].url.resource = g_strconcat(servant->resource,
	    "/", listing[count].file, NULL);

    if(listing[count].s.st_mode & S_IFREG)
    {
      list->_buffer[count].Ntype = NCS_REGULAR;
    }
    if(listing[count].s.st_mode & S_IFDIR)
    {
      list->_buffer[count].Ntype = NCS_DIRECTORY;
    }
    
    /*need to set the metadata about each location.*/
  }
  
  if(list_pt_st->dont_callback)
  { /*called internally*/
    list_pt_st->list = list;
  }
  else
  {
    NCC_List_cb(servant->ncc, 1, list, list_pt_st->pointer, &ev);
  }
}
