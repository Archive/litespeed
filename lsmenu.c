#include "lspeed.h"

void menu_cd_cb(GtkWidget* widget, lswin* win)
{
  lssystem* local = win->local;
  lssystem* remote = win->remote;
  GtkWidget* cd_local = win->local->items->ChgDir;
  GtkWidget* cd_remote = win->remote->items->ChgDir;
  char getcwd_buf[2*MAXPATHLEN];
  char *txt;
  int row;

  
  if(widget == cd_local)
  {
    GtkWidget * clist = win->local->clist;
    GList* list = GTK_CLIST(clist)->selection;
    GtkWidget *entry = GTK_COMBO(win->local->directory)->entry;
    while (list)
    {
      row = (gint) list->data;
      gtk_clist_get_text (GTK_CLIST (clist), row, 1, &txt);
      list = list->next;
    } 
    
    if(txt == NULL || chdir(txt) != 0)
    {
      lsStatusText(win, "Can't change to that directory.\n", LC_ERROR);
    }
    else
    {
#if defined(sun) && !defined(__SVR4)
      getwd (getcwd_buf);
#else    
      getcwd (getcwd_buf, MAXPATHLEN);
#endif
      gtk_entry_set_text(GTK_ENTRY(entry),getcwd_buf);
      start_clist_populate (win, local, g_strdup(getcwd_buf), TRUE);
    }
  }
  if(widget == cd_remote)
  {

    /*
    gtk_signal_connect (GTK_OBJECT(win->remote->dialogs->RmDir_d->OK),
			"clicked",
	  		GTK_SIGNAL_FUNC(r_rmdir_confirmed),win);
    gtk_signal_connect (GTK_OBJECT(win->remote->dialogs->RmDir_d->dir),
			"activate",
			GTK_SIGNAL_FUNC(r_rmdir_confirmed),win);
    gtk_signal_connect (GTK_OBJECT(win->remote->dialogs->RmDir_d->Cancel),
			"clicked",
			GTK_SIGNAL_FUNC(r_rmdir_canceled),win);
    */
  }
  
}
