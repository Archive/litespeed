#include "lspeed.h"

void lsStatusText (lswin * win, char *txt, int t)
{
  static inited = FALSE;
  float upper;


  gtk_text_freeze (GTK_TEXT(win->text));    
  gtk_text_insert (GTK_TEXT (win->text), NULL, &win->gopts->LC_Color[t],
		   NULL, txt, strlen(txt));

  /*
  upper = GTK_TEXT(win->text)->vadj->upper;
  cerr << "upper = " << upper << endl;
  gtk_adjustment_set_value(GTK_TEXT(win->text)->vadj, 
			   upper-.001);
  */

  gtk_text_thaw (GTK_TEXT(win->text));  
  
  /*Add the text to the viewlog dialog.*/
  gtk_text_freeze (GTK_TEXT(win->viewlog_d->text));    
  gtk_text_insert (GTK_TEXT (win->viewlog_d->text), NULL, 
		   &win->gopts->LC_Color[t], NULL, txt, strlen(txt));

  /*
  upper = GTK_TEXT(win->viewlog_d->text)->vadj->upper;
  gtk_adjustment_set_value(GTK_TEXT(win->viewlog_d->text)->vadj, 
			   upper-.001);
  */

  gtk_text_thaw (GTK_TEXT(win->viewlog_d->text));  

  /*
  if (opts.logfile)
  {
    addtolog(txt);
  }    
  */

}
