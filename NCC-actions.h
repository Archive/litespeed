#include <glib.h>
#include "NFTPC.h"
#include "NFTPC-impl.h"
/*Be sure these are included after the NCC headers*/
#include "NFTP/NFTP.h"
#include "NCS-types.h"


typedef void* (*N_SIGNAL) (void*);
#define N_ACTION(x) ((n_action*)x)

typedef void (*NSignalFunc) ();
#define N_SIGNAL_FUNC(f)  ((NSignalFunc) f)

typedef void (*NLocListSignalFunc) ();
#define N_LOC_LIST_SIGNAL_FUNC(f)  ((NLocListSignalFunc) f)

typedef void (*NConnectionOpenSignalFunc) ();
#define N_CONNECTION_OPEN_SIGNAL_FUNC(f)  ((NConnectionOpenSignalFunc) f)

/*super types*/
typedef struct _n_action n_action;
typedef struct _n_sig n_sig;



struct _n_sig
{
  char* sig_name;
  NSignalFunc callback;
  void* user_data;
};

struct _n_action
{
  N_TYPE type;
  GSList *signals;
  NCS_Connection obj;
};


/*Sub types*/
typedef struct _n_connection_open_action n_connection_open_action;
typedef struct _n_loc_list_action n_loc_list_action;

struct _n_loc_list_action
{
  N_TYPE type;
  GSList *signals;
  NCS_Connection obj;
  CORBA_char* dir;
};

struct _n_connection_open_action
{
  N_TYPE type;
  GSList *signals;
  NCS_Connection obj;
  CORBA_Object handle;
  URL* url;
  CORBA_char * passwd;
};


/*creation functions*/
n_action*
n_connection_open_new(NCS_Connection obj, 
	const CORBA_Object handle, const URL* url, const CORBA_char * passwd);
n_action*
n_loc_list_new(NCS_Connection obj, const CORBA_char* dir);
