#!/usr/bin/perl

##Make sure we start with fresh copy of the impl.c file.

print `orbit-idl -Eskeleton_impl NLFS.idl`;

open (NLFS_IMPL_H, ">NLFS-impl.h");
open (NLFS_IMPL_C, "<NLFS-impl.c");
open (TEMP_IMPL_C, ">TEMP-NLFS-impl.c");

$in_impl = 0;
$here = 0;

print TEMP_IMPL_C "
#include \"NLFS-impl.h\"
                  ";


while( <NLFS_IMPL_C> )
{
  s/static//g;
  s/\(gpointer\).impl_NCS_Loc/(gpointer)&impl_NLFSLoc/;

  if(/Stub implementations/)
  {
    $in_impl = 1;
  }

  if(!$in_impl)
  {

    if(/App-specific servant structures/)
    {
      print NLFS_IMPL_H "
#include <glib.h>
#include <pthread.h>
#include \"NCC.h\"
                        ";
    }

    if(/} impl_POA_NCS_Loc;/ || /} impl_POA_NLFSLoc;/)
    {
      print NLFS_IMPL_H "

        char*           resource;
        URL*            url;
        long            whence;
        long            seek;

        /*My own ftp stuff*/
        int             working;
        int             done;
        int             fd;
        pthread_t       thread;

        char            *localfile;
        char            *tempfile;
        NCC             ncc;
        CORBA_Environment ev;
                       ";
     $here = 1;
    }
    print NLFS_IMPL_H $_;
  }
  else
  {
    print TEMP_IMPL_C $_;
  }
}

`mv TEMP-NLFS-impl.c NLFS-impl.c`;
