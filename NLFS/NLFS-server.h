#ifndef NLFS_SERVER_H
#define NLFS_SERVER_H

#
/*
 * This code it public domain. Matt Wimer thinks he has the
 * credit of creating it.  It can be used any way you want, but
 * Matt Wimer is not responsible for your actions.
 */

typedef struct _nc_getmetadata_pt_st nc_getmetadata_pt_st;
typedef struct _nc_setmetadata_pt_st nc_setmetadata_pt_st;

typedef struct _nc_open_pt_st nc_open_pt_st;
typedef struct _nc_close_pt_st nc_close_pt_st;

typedef struct _nc_read_pt_st nc_read_pt_st;
typedef struct _nc_write_pt_st nc_write_pt_st;

typedef struct _nc_list_pt_st nc_list_pt_st;

typedef struct _nc_create_pt_st nc_create_pt_st;
typedef struct _nc_delete_pt_st nc_delete_pt_st;

typedef struct _nc_current_pt_st nc_current_pt_st;
typedef struct _nc_aliases_pt_st nc_aliases_pt_st;

typedef struct _nc_pause_pt_st nc_pause_pt_st;
typedef struct _nc_resume_pt_st nc_resume_pt_st;
typedef struct _nc_abort_pt_st nc_abort_pt_st;

typedef struct _nc_newlocation_pt_st nc_newlocation_pt_st;
typedef struct _nc_newmetadata_pt_st nc_newmetadata_pt_st;


struct _nc_open_pt_st
{
  impl_POA_NCS_Connection * servant;
  CORBA_Object handle;
  URL * url;
  CORBA_char * passwd;
  CORBA_long pointer;
  CORBA_Environment * ev;
};

struct _nc_close_pt_st
{
};

struct _nc_read_pt_st
{
   impl_POA_NCS_Loc * servant;
   CORBA_long pointer;
   CORBA_Environment * ev;
};

struct _nc_write_pt_st
{
   impl_POA_NCS_Loc * servant;
   Buffer *buf;
   CORBA_long pointer;
   CORBA_Environment * ev;
};

struct _nc_list_pt_st
{
   impl_POA_NCS_Loc * servant;
   CORBA_char* resource;
   CORBA_long pointer;
   int dont_callback;;
   InfoList *list;
   CORBA_Environment * ev;
};

struct _nc_create_pt_st
{
   impl_POA_NCS_Loc * servant;
   CORBA_char * resource;
   CORBA_long pointer;
   CORBA_Environment * ev;
};

struct _nc_delete_pt_st
{
   impl_POA_NCS_Loc * servant;
   CORBA_long pointer;
   CORBA_Environment * ev;
};

struct _nc_newlocation_pt_st
{
   impl_POA_NCS_Loc * servant;
   CORBA_char * resource;
   CORBA_long pointer;
   CORBA_Environment * ev;
};

#endif /*NLFS_SEVER_H*/
