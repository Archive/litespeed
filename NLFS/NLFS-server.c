#include "../N/support/CS.h"
#include <libgnorba/gnorba.h>
#include <gtk/gtk.h>
#include <orb/orbit.h>
#include <stdio.h>
#include <sys/errno.h>
#include <string.h>

/*For calling the stub functions for the callback.*/
#include "NCC.h"


/*Implement the functions defined in the NLFS.idl*/
#include "NLFS.h"
#include "NLFS-impl.h"
#include "NLFS-server.h"

typedef void* (*PT_FUNC) (void*);

NCS_Connection NLFS = CORBA_OBJECT_NIL;
NCS_Connection NCS = CORBA_OBJECT_NIL;

CORBA_ORB orb;
PortableServer_POA poa;
CORBA_Environment ev;


int main(int argc, char** argv)
{
  char *retval;
  PortableServer_ObjectId NCSID = {0, sizeof("NLFS"), "NLFS"};
  char IOR[4000];
  FILE* host_ior_fp;

  char *name = "NLFS";                     
  int n = 1;                     
  
  IIOPAddConnectionHandler = orb_add_connection;
  IIOPRemoveConnectionHandler = orb_remove_connection;


  CORBA_exception_init(&ev);
/*
  orb = gnome_CORBA_init("NLFS", NULL, &argc, argv,
	  0, &ev);
*/
  orb = CORBA_ORB_init(&n, &name /* dummy */, "orbit-local-orb", &ev);



  poa = (PortableServer_POA)
    CORBA_ORB_resolve_initial_references(orb, "RootPOA", &ev);
  PortableServer_POAManager_activate
    (PortableServer_POA__get_the_POAManager(poa, &ev), &ev);
  
  NLFS = impl_NCS_Connection__create(poa,  &ev);
  printf("NLFS = %i\n", NLFS);

  
  /* 
  Unused now that this object is passed to the root_ncs
  retval = CORBA_ORB_object_to_string(orb, NLFS , &ev);
  printf("retval = %s\n", retval);
  */
  host_ior_fp = fopen(ncs_get_default_ior_filename(), "r");
  fscanf(host_ior_fp, "%s", IOR);
  fclose(host_ior_fp);

  NCS = CORBA_ORB_string_to_object(orb, IOR, &ev);


  fprintf(stderr, "Trying to register...\n");
  NCS_Connection_Register(NCS, NLFS, "file", "passwd", &ev);
  fprintf(stderr, "Registered with NCS.\n");


  gtk_main ();
  gdk_exit (0);

}



/*********************************************************
 * impl_NCS_Connection_Open and nlfs_pt_open
 *
 * Create a socket object that can be used in susequent
 * requests of the server.
 *
 * If the protocol is handled by a different ORB, connect
 * to that ORB and use NCS_Connection_Open to get the 
 * socket.
 *
 * This ORB handles ftp:// requests.
 * We need to authenticate the user, before handing out a
 * socket.
 *
 *********************************************************/

nlfs_pt_open(nc_open_pt_st* open_pt_st)
{
  PortableServer_ObjectId *objid;
  impl_POA_NLFSLoc *newservant;
  NCS_Loc loc;
  char* loc_ior;
  CORBA_Environment ev;
  NCC NCC_server;

  int urlok = 1; /*Good for now.*/

  /*CS_parse_url(open_pt_st->url);*/

  fprintf(stderr, "st->passwd = %s\n", open_pt_st->passwd);
  fprintf(stderr, "url->url = %s\n", open_pt_st->url->url);
  fprintf(stderr, "url->resource = %s\n", open_pt_st->url->resource);
  fprintf(stderr, "url->proto = %s\n", open_pt_st->url->proto);
  fprintf(stderr, "url->host = %s\n", open_pt_st->url->host);
  fprintf(stderr, "url->user = %s\n", open_pt_st->url->user);
  fprintf(stderr, "url->password = %s\n", open_pt_st->url->password);
  fprintf(stderr, "url->port = %i\n", open_pt_st->url->port);
  
  if(1) /* can we handle this url? */
  {
    newservant = g_new0(impl_POA_NLFSLoc, 1);
    newservant->servant.vepv = &impl_NLFSLoc_vepv;
    newservant->poa = poa;
    POA_NLFSLoc__init((POA_NLFSLoc*)newservant, &ev);
    objid = PortableServer_POA_activate_object(poa, newservant, &ev);
    CORBA_free(objid);

    loc = PortableServer_POA_servant_to_reference(poa, newservant, &ev);

    /*
    impl_NCS_Loc__set_resource((impl_POA_NCS_Loc*)newservant, 
	    open_pt_st->url->resource, &ev);
	    */
    
    /*Do work here*/

    /*No work to do.  file:// is a connectionless protocol*/
    newservant->url = clone_url(open_pt_st->url);
    /*Done with work*/
  }
  newservant->ncc = (NCC)open_pt_st->handle;
  fprintf(stderr, "making callback call for the Open_cb\n");
  NCC_Open_cb(newservant->ncc, 1/*success*/, loc, open_pt_st->pointer, &ev);
  fprintf(stderr, "done making callback call for the Open_cb\n");
}

void
impl_NCS_Connection_Open(
        impl_POA_NCS_Connection * servant,
	CORBA_Object handle,
        URL * url,
        CORBA_char* passwd,
        CORBA_long pointer,
        CORBA_Environment * ev)
{
  pthread_t* threadptr;
  nc_open_pt_st* open_pt_st;
  pthread_attr_t d_attr;

  threadptr = g_new0(pthread_t,1);
  open_pt_st = g_new0(nc_open_pt_st,1);
  open_pt_st->url = URL__alloc();


  open_pt_st->servant = servant;
  open_pt_st->handle = handle;
  open_pt_st->url = clone_url(url);
  open_pt_st->passwd = passwd;
  open_pt_st->pointer = pointer;
  open_pt_st->ev = ev;
  

  /*
  pthread_attr_setdetachstate(&d_attr, PTHREAD_CREATE_DETACHED);
  pthread_create(threadptr, &d_attr,
                 (PT_FUNC)nlfs_pt_open,
                 (void*) open_pt_st);
		 */
  pthread_create(threadptr, NULL,
                 (PT_FUNC)nlfs_pt_open,
                 (void*) open_pt_st);
}

void
nlfs_pt_newloc(nc_newlocation_pt_st* newloc_pt_st)
{
  NCS_Loc loc;
  impl_POA_NLFSLoc *newservant;
  PortableServer_ObjectId *objid;
  
  newservant = g_new0(impl_POA_NLFSLoc, 1);
  newservant->servant.vepv = &impl_NLFSLoc_vepv;
  newservant->poa = poa;
  POA_NLFSLoc__init((PortableServer_Servant) newservant, &ev);
  objid = PortableServer_POA_activate_object(poa, newservant, &ev);
  CORBA_free(objid);
  loc = PortableServer_POA_servant_to_reference(poa, newservant, &ev);


  newservant->resource = g_strdup(newloc_pt_st->resource);
  newservant->url = newloc_pt_st->servant->url;

  printf("Calling loc's Resource = %s\n", 
	 newloc_pt_st->servant->resource);
  fflush(stdout);
  
  /*return loc;*/
}

	       
void
impl_NCS_Loc_NewLocation(impl_POA_NCS_Loc *servant,
		    CORBA_char * resource,
		    CORBA_long pointer,
		    CORBA_Environment *ev)
{
  pthread_t* threadptr;
  nc_newlocation_pt_st* newloc_pt_st;
  pthread_attr_t d_attr;

  threadptr = g_new0(pthread_t,1);
  newloc_pt_st = g_new0(nc_newlocation_pt_st,1);

  newloc_pt_st->servant = servant;
  newloc_pt_st->resource = resource;
  newloc_pt_st->pointer = pointer;
  newloc_pt_st->ev = ev;

  pthread_attr_setdetachstate(&d_attr, PTHREAD_CREATE_DETACHED);
  pthread_create(threadptr, NULL,
                 (PT_FUNC)nlfs_pt_newloc,
                 (void*) newloc_pt_st);
  
}


void
nlfs_pt_list(nc_list_pt_st* list_pt_st)
{
  char tempfile[MAXPATHLEN];
  char concatfile[MAXPATHLEN];
  char *local, *remote;
  char* dirlist, *curpos, *temp;
  ftpitem *listing;
  int i=0, ret, fd, guess, count=0;
  struct stat dirstat;
  impl_POA_NCS_Loc * servant;
  InfoList *list;
  char command[MAXPATHLEN];


  servant = list_pt_st->servant;
  /*
  local = servant->local;
  local = servant->remote;
  */

  fprintf(stderr, "in nlfs_pt_list\n");

  sprintf(tempfile, "%stmp.%i", "/tmp/nlfs.", servant);
  servant->tempfile = g_strdup(tempfile);

  list = InfoList__alloc();
  list->_maximum = 0;
  list->_length = 0;
  list->_buffer = NULL;
  list->_release = 0;
  
  printf("tempfile =  %s\n", tempfile);

  strcpy(command, "ls -Alg ");
  strcat(command, list_pt_st->resource);
  strcat(command, " > ");
  strcat(command, tempfile);
  
  system(command);

  /*Open dirlist for reading*/
  fd = open(tempfile, O_RDWR);
  fstat(fd, &dirstat);
  dirlist = mmap(0, dirstat.st_size+1, PROT_READ|PROT_WRITE, 
		 MAP_FILE|MAP_PRIVATE, fd, 0);

  curpos = dirlist;
  guess = dirstat.st_size/LSLINESIZE;
  listing = g_new0(ftpitem, guess);

  printf("guess %i\n", guess);
  fflush(stdout);

  if(guess)
  {
    while(dirlist+ dirstat.st_size > curpos)
    {

      temp = strchr(curpos, '\n');
      *temp = '\0';
      ret = parse_ls_lga(curpos, &listing[i]);

      fprintf(stderr, "filename = %s, i = %i\n", listing[i].file, i);

      curpos = temp+1;

      if(ret == 0)
	continue;  /*Don't increment, just try the next line in dirlist*/
      i++;
    }
  }
  
  /*Fill the NCS_LocList and ItemList*/
  list->_buffer = g_new0(Info, i+1);
  list->_maximum = 0;
  list->_length = i;
  for(count=0; count < i; count++)
  {
    
    /*strcpy(concatfile, remote);
    sprintf(concatfile, "%s/%s", concatfile, listing[count].file);
    printf("concat = %s", concatfile);
    printf("resource = %s", remote);
    */

    /*
    list->_buffer[count].loc =
      my_NewLoc(servant, concatfile);
     */

    
    list->_buffer[count].perms = g_strdup("a");
    list->_buffer[count].user = g_strdup("a");
    list->_buffer[count].group =g_strdup("a");
    list->_buffer[count].loc = NLFS; 

    list->_buffer[count].url.url = servant->url->url;
    list->_buffer[count].url.proto = servant->url->proto;
    list->_buffer[count].url.user = servant->url->user;
    list->_buffer[count].url.password = servant->url->password;
    list->_buffer[count].url.host = servant->url->host;

    list->_buffer[count].url.name = listing[count].file;
    list->_buffer[count].url.resource = g_strconcat(list_pt_st->resource,
	    "/", listing[count].file, NULL);

    list->_buffer[count].url.port = servant->url->port;
    list->_buffer[count].url.errors = servant->url->errors;

    if(listing[count].s.st_mode & S_IFREG)
    {
      list->_buffer[count].Ntype = NCS_REGULAR;
    }
    if(listing[count].s.st_mode & S_IFDIR)
    {
      list->_buffer[count].Ntype = NCS_DIRECTORY;
    }
    Info_print( &(list->_buffer[count]) );
  }
  
  if(list_pt_st->dont_callback)
  { /*called internally*/
    list_pt_st->list = list;
  }
  else
  {
    NCC_List_cb(servant->ncc, 1, list, list_pt_st->pointer, &servant->ev);
  }
}



void
impl_NLFSLoc_List(impl_POA_NLFSLoc * servant,
	CORBA_char * resource,
	CORBA_long pointer,
	CORBA_Environment * ev)
{
  pthread_t* threadptr;
  nc_list_pt_st* list_pt_st;
  pthread_attr_t d_attr;

  threadptr = g_new0(pthread_t,1);
  list_pt_st = g_new0(nc_list_pt_st,1);

  list_pt_st->servant = servant;
  list_pt_st->resource = g_strdup(resource);
  list_pt_st->pointer = pointer;

  /*
  pthread_attr_setdetachstate(&d_attr, PTHREAD_CREATE_DETACHED);
  pthread_create(threadptr, &d_attr,
                 (PT_FUNC)nlfs_pt_list,
                 (void*)list_pt_st);
		 */
  fprintf(stderr, "in impl_NLFSLoc_List\n");
  pthread_create(threadptr, NULL,
                 (PT_FUNC)nlfs_pt_list,
                 (void*)list_pt_st);
  fprintf(stderr, "ending impl_NLFSLoc_List\n");
}

void
nlfs_pt_create(nc_create_pt_st* create_pt_st)
{
  int len;
  impl_POA_NCS_Loc * servant;

  servant = create_pt_st->servant;
  len = strlen(servant->resource);

  printf("resource[len] = %c\n", servant->resource[len-1]);

  if(servant->resource[len-1] == '/')
  {
    printf("resource = %s\n", servant->resource);
    fflush(stdout);
    /*
    FtpMkdir(servant->resource, servant->control);
    */
  }
}

void
impl_NLFSLoc_Create(impl_POA_NLFSLoc *servant,
	CORBA_char * resource,
	CORBA_long pointer,
	CORBA_Environment *ev)
{       
  pthread_t* threadptr;
  nc_create_pt_st* create_pt_st;
  pthread_attr_t d_attr;

  threadptr = g_new0(pthread_t,1);
  create_pt_st = g_new0(nc_create_pt_st,1);

  create_pt_st->servant = servant;
  create_pt_st->resource = resource;
  create_pt_st->pointer = pointer;
  create_pt_st->ev = ev;

  pthread_attr_setdetachstate(&d_attr, PTHREAD_CREATE_DETACHED);
  pthread_create(threadptr, &d_attr,
                 (PT_FUNC)nlfs_pt_create,
                 (void*)create_pt_st);
  
}
void
nlfs_pt_delete(nc_delete_pt_st* delete_pt_st)
{
  CORBA_long retval;
  impl_POA_NCS_Loc* servant;
  int len;

  servant = delete_pt_st->servant;

  len = strlen(servant->resource);

  printf("resource[len] = %c\n", servant->resource[len-1]);
  if(servant->resource[len-1] == '/')
  {
    printf("resource = %s\n", servant->resource);
    fflush(stdout);
    /*
     FtpRmdir(servant->resource, servant);
     */
  }
  else
  {
    /*
    FtpDelete(servant->resource, servant->control);
    */
  }
}

void
impl_NCS_Loc_Delete(impl_POA_NCS_Loc * servant, 
	CORBA_char * resource,
	CORBA_long pointer,
	CORBA_Environment * ev)
{
  pthread_t* threadptr;
  nc_delete_pt_st* delete_pt_st;
  pthread_attr_t d_attr;

  threadptr = g_new0(pthread_t,1);
  delete_pt_st = g_new0(nc_delete_pt_st,1);

  delete_pt_st->servant = servant;
  delete_pt_st->pointer = pointer;
  delete_pt_st->ev = ev;

  pthread_attr_setdetachstate(&d_attr, PTHREAD_CREATE_DETACHED);
  pthread_create(threadptr, &d_attr,
                 (PT_FUNC)nlfs_pt_delete,
                 (void*)delete_pt_st);


}

/*
impl_NCS_Loc_SetResource(impl_POA_NCS_Loc * servant,
	CORBA_char * value,
	CORBA_Environment * ev)
{       
  if(servant->resource)
  {
    free(servant->resource);
  }
  servant->resource = g_strdup(value);
  printf("resource = %s\n", servant->resource);
  servant->done = 0;
  servant->working = 0;
  servant->whence = SEEK_SET;
  servant->seek = 0;
}
*/


/***Local***/

void
local_pt_list(nc_list_pt_st* list_pt_st)
{
  char tempfile[MAXPATHLEN];
  char concatfile[MAXPATHLEN];
  char *local, *remote;
  char* dirlist, *curpos, *temp;
  ftpitem *listing;
  int i=0, ret, fd, guess, count=0;
  struct stat dirstat;
  impl_POA_NCS_Loc * servant;
  InfoList *list;
  char command[MAXPATHLEN];

  servant = list_pt_st->servant;

  /*
  local = servant->local;
  local = servant->remote;
  */

  fprintf(stderr, "in nlfs_pt_list\n");

  sprintf(tempfile, "%stmp.%i", "/tmp/local.", servant);
  servant->tempfile = g_strdup(tempfile);

  list = InfoList__alloc();
  list->_maximum = 0;
  list->_length = 0;
  list->_buffer = NULL;
  list->_release = 0;
  
  fprintf(stderr, "tempfile =  %s\n", tempfile);
  
  strcpy(command, "ls -Alg ");
  strcat(command, local);
  strcat(command, " > ");
  strcat(command, tempfile);
  
  fprintf(stderr, "command = %s\n", command);
  system(command);

  /*
  FtpDir(tempfile, servant->resource, servant->control);
  fprintf(stderr, "resp = %s\n", FtpLastResponse(servant->control) );
  */

  /*Open dirlist for reading*/
  fd = open(tempfile, O_RDWR);
  fstat(fd, &dirstat);
  dirlist = mmap(0, dirstat.st_size+1, PROT_READ|PROT_WRITE, 
		 MAP_FILE|MAP_PRIVATE, fd, 0);

  curpos = dirlist;
  guess = dirstat.st_size/LSLINESIZE;
  listing = g_new0(ftpitem, guess);

  printf("guess %i\n", guess);
  fflush(stdout);

  if(guess)
  {
    while(dirlist+ dirstat.st_size > curpos)
    {

      temp = strchr(curpos, '\n');
      *temp = '\0';
      ret = parse_ls_lga(curpos, &listing[i]);

      fprintf(stderr, "filename = %s, i = %i\n", listing[i].file, i);
      fflush(stdout);

      curpos = temp+1;

      if(ret == 0)
	continue;  /*Don't increment, just try the next line in dirlist*/
      i++;
    }
  }
  
  /*Fill the NCS_LocList and ItemList*/
  list->_buffer = g_new0(Info, i+1);
  list->_maximum = 0;
  list->_length = i;
  for(count=0; count < i; count++)
  {
    
    /*strcpy(concatfile, remote);
    sprintf(concatfile, "%s/%s", concatfile, listing[count].file);
    printf("concat = %s", concatfile);
    printf("resource = %s", remote);
    */

    /*
    list->_buffer[count].loc =
      my_NewLoc(servant, concatfile);
     */

    list->_buffer[count].url.name = listing[count].file;

    if(listing[count].s.st_mode & S_IFREG)
    {
      list->_buffer[count].Ntype = NCS_REGULAR;
    }
    if(listing[count].s.st_mode & S_IFDIR)
    {
      list->_buffer[count].Ntype = NCS_DIRECTORY;
    }
    
    /*need to set the metadata about each location.*/
  }
  
  if(list_pt_st->dont_callback)
  { /*called internally*/
    list_pt_st->list = list;
  }
  else
  {
    /*NCC_List_cb(servant->ncc, 1, list, list_pt_st->pointer, &servant->ev);*/
  }
}

