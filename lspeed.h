#define PACKAGE "litespeed"
#define VERSION "0.00"


#include "N/support/CS.h"
#include <orb/orbit.h>
#include <strings.h>
#include <sys/types.h>
#include <regex.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <ctype.h>
#include <gnome.h>

#ifdef N_
#undef N_
#endif /*N_*/
#define N_(x) x

#include <libgnorba/gnorba.h>
#include <gdk/gdkkeysyms.h>
#include <gdk/gdk.h>
#include <sys/types.h>

#include <proplist.h>


#include "lsconfig.h"

/*This must be included before the NFTP headers
#include "NFTPC.h"
#include "NFTPC-impl.h"

Be sure these are included after the NCC headers
#include "NFTP/NFTP.h"
*/

#include "NCC-actions.h"

#define LC_NORMAL  0
#define LC_SUCCESS 1
#define LC_WARNING 2
#define LC_ERROR   3

#define LP_FAST   0
#define LP_MEDIUM 1
#define LP_SLOW   2

#define LA_NOTHING 0
#define LA_LIST 1
#define LA_FILL_CLIST 2
#define LA_POPULATE_CLIST 3

#define TYPE 0
#define NAME 1
#define DATE 2
#define SIZE 3

#define DIRECTORY 0
#define LINK      1
#define REGULAR   2

#define ALPHA   (1 << 0)
#define RALPHA  (1 << 1)
#define DIRFILE (1 << 2)
#define FILEDIR (1 << 3)
#define my_DATE (1 << 4)
#define RDATE   (1 << 5)
#define my_SIZE (1 << 6)
#define RSIZE   (1 << 7)

#define MAXTYPE 6
#define MAXNAME 6
#define MAXDATE 4
#define MAXSIZE 4

#define MAXSORTS 6

#define HOSTTYPES 3

/*Host types.*/
static char *hosttypes[] = {"Auto", "Unix", "Other"};

typedef struct _lswin lswin;
typedef struct _lssystem lssystem;
typedef struct _lsbuttons lsbuttons;
typedef struct _lsmove lsmove;
typedef struct _lstransfer lstransfer;
typedef struct _lscontrol lscontrol;
typedef struct _lsitems lsitems;
typedef struct _lsftpcmd lsftpcmd;
typedef struct _lsdialogs lsdialogs;
typedef struct _lsprogress_d lsprogress_d;

typedef struct _lssort lssort;

typedef struct _lsaction lsaction;
typedef struct _lsNCS lsNCS;
typedef struct _lsNFTPC lsNFTPC;

typedef struct _chgdir_d chgdir_d;
typedef struct _mkdir_d mkdir_d;
typedef struct _rename_d rename_d;
typedef struct _delete_d delete_d;
typedef struct _rmdir_d rmdir_d;

typedef struct _Connect_d Connect_d;
typedef struct _Options_d Options_d;
typedef struct _ViewLog_d ViewLog_d;

typedef struct _lsgeneral lsgeneral;
typedef struct _lsstartup lsstartup;
typedef struct _lsadvanced lsadvanced;
typedef struct _lsfirewall lsfirewall;

typedef struct _lssession lssession;

typedef struct _lslistitem lslistitem;

typedef struct _lsrename lsrename;

int unshow (GtkWidget *widget, gpointer data);

void add_slash(char path[]);
int inlist(GList *list, char *text);
gint correct_path(char* fullname);
void backup_sites(lswin* win);
void clist_select_row (int row, int state, lssystem *sys);

/*  FIXME: use XDnD
void clist_drag_cb (GtkWidget *widget, GdkEventDragRequest *event, lswin* win);
void clist_drop_highlight_cb (GtkWidget *widget, GdkEvent *event);
void clist_drop_cb (GtkWidget *widget, GdkEventDropDataAvailable *event);
*/

int lsquit(GdkEventAny*);

void about_cb (GtkWidget *widget, void *data);
int clist_event_cb (GtkWidget *widget, GdkEvent *event, lswin *win);
void change_sort_cb (GtkWidget *widget, int col, lswin *thewindow);

opts* new_options(char *lsrc);

Connect_d * new_Connect_d(lswin* win);
ViewLog_d * new_ViewLog_d();
Options_d * new_Options_d();

InfoList* list_all(lswin* win, lssystem* sys, char* path);
void fillclist(lssystem* sys, InfoList* list);
void start_clist_populate(lswin *win, lssystem*, gchar *path, gint complete);

void remote_init (lswin *win);
void local_init (lswin *win);

void lsStatusText (lswin * win, char *txt, int t);
void lsProgressUpdate(lswin* win, int bytesmoved, float kbitspersec,
     int timespent, int timetogo, float percentdone, int color);

void Connect_cb (GtkWidget *widget, lswin *win);
void ViewLog_cb (GtkWidget *widget, lswin *win);
void Options_cb (GtkWidget *widget, lswin *win);

GtkWidget* combo_new(void);
GtkWidget* button_new(char *label);
GtkWidget* check_new(char *label);
GtkWidget* label_new(char *label);
GtkWidget* entry_new();
GtkWidget* radio_new(GSList **group, char *label);

int general_cb (GtkWidget *widget, gpointer* other, lswin *thewindow);
void set_popdown(lswin* win, lsgeneral* general);
int new_cb (GtkWidget *widget, lswin *thewindow);

void localMkDir_cb (GtkWidget *widget, lswin *win);
void remoteMkDir_cb (GtkWidget *widget, lswin *win);
void localDelete_cb (GtkWidget *widget, lswin *win);
void remoteDelete_cb (GtkWidget *widget, lswin *win);
void localRename_cb (GtkWidget *widget, lswin *win);
void remoteRename_cb (GtkWidget *widget, lswin *win);

void ChgDir_cb (GtkWidget *widget, lswin *win);
void Delete_cb (GtkWidget *widget, lswin *win);
void Upload_cb (GtkWidget *widget, lswin *win);
void Download_cb (GtkWidget *widget, lswin *win);
void Refresh_cb (GtkWidget *widget, lswin *win);

/*void general_cb (GtkWidget *widget, lswin *win);*/

void delete_file_confirmed (GtkWidget *widget, lssystem*, lswin* win);
void delete_file_cancel(GtkWidget *widget, lswin* win);

void chgdir_confirmed (GtkWidget *widget, lswin* win);
void chgdir_canceled (GtkWidget *widget, lswin* win);

void r_mkdir_confirmed (GtkWidget *widget, lswin* win);
void r_mkdir_canceled(GtkWidget *widget, lswin* win);

void menu_cd_cb(GtkWidget* widget, lswin* win);

lsNFTPC* ls_cb_CORBA_new(CORBA_ORB orb, CORBA_Environment* ev);
lsNCS* LS_and_CORBA_init(int *argc, char** argv);

int
ls_nftpc_open_cb(n_action* unused,
	CORBA_long ret, CORBA_Object loc, lsaction* list_action);
int
ls_nftpc_list_cb(n_action* unused,
	CORBA_long ret, InfoList *list, lsaction* action);



struct _lsNCS
{
  CORBA_ORB ORB;
  CORBA_Environment ev;
  NCS_Connection NCS;
  char *IOR;
};

struct _lsNFTPC
{
  PortableServer_POA poa;
  impl_POA_NFTPC *servant;
  NFTPLoc NFTPC;
  CORBA_Environment ev;
};

struct _lsaction
{
  lssystem *sys;
  lswin *win;
  int action;
  char* path;
  int complete;
};

/*
struct _lslistitem
{
  char *name;
  dev_t type;
  time_t date;
  size_t size;
  char* perms;
};
*/

struct _lsrecur
{
  GList* list;

  size_t transfered; 

  char* localcwd;
  char* remotecwd;
};

struct _lssort
{
  int type[4];

  int current;
  int state[4];
  
  GHashTable* allfiles;
  GHashTable* backup;
  GList* retlist;
};

struct _lsrename
{
  GtkWidget *entry;
  GtkWidget *hbox;
  GtkWidget *window;
  gint xoff, yoff;
  gint c1;
  gint c2;
  
};

struct _lssystem
{
  /*GUI*/
  lsbuttons *buttons;
  lsitems* items;
  GList* cbitems;
  lsdialogs* dialogs;

  lssort *sort;

  GtkWidget* topbox;
  GtkWidget* directory;
  GtkWidget* hbox;
  GtkWidget* vbox;
  GtkWidget* frame;
  GtkWidget* clist;
  GtkWidget* scwindow;
  GtkWidget* menu;

  /*For double clicking in clists*/
  gint32 lastclick;
  int last_clicked_row;

  /*Connections*/
  InfoList* infolist;
  URL* url;

  NCS_Loc lister;
  CORBA_Environment lister_ev;

  NCS_Loc filer;
  CORBA_Environment filer_ev;
};

struct _lsftpcmd
{
  GtkWidget* menu; 
  
  GtkWidget* STOR; 
};

struct _lsprogress_d
{
  GtkWidget *window;
  GtkWidget *From;
  GtkWidget *To;
  
  GtkWidget *pbar;
  GtkWidget *Percent;
  GtkWidget *Size;
  GtkWidget *ToGo;
  GtkWidget *Rate;

  GtkWidget *Xfered;
  GtkWidget *TotalXfered;
  GtkWidget *TotalRate;

  
  GtkWidget *Cancel;
  GtkWidget *Retry;

  GtkWidget *outer;
  GtkWidget *inner;

  GtkWidget *right;
  GtkWidget *left;

  GtkWidget *table;
};

struct _lsitems
{
  lsftpcmd* ftpcmd;

  GtkWidget *Sort;
  GtkWidget *ChgDir;
  GtkWidget *MkDir;
  GtkWidget *DelDir;
  GtkWidget *Transfer;
  GtkWidget *ViewFile;
  GtkWidget *Execute;
  GtkWidget *Rename;
  GtkWidget *Move;
  GtkWidget *Delete;
  GtkWidget *Chmod;
  GtkWidget *RefreshList;
  GtkWidget *DirList;
  GtkWidget *FTPCommands;
};

struct _lsbuttons
{
  GtkWidget* vbox;
  GtkWidget* ChgDir;
  GtkWidget* MkDir;
  GtkWidget* regex;
  GtkWidget* View;
  GtkWidget* Rename;
  GtkWidget* Delete;
  GtkWidget* Refresh;
  GtkWidget* DirInfo;

  regex_t * reg;
};

struct _lsdialogs
{
  chgdir_d* ChgDir_d;
  mkdir_d*  MkDir_d;
  rename_d* Rename_d;
  delete_d* Delete_d;

  rmdir_d* RmDir_d;
};

struct _chgdir_d
{
  GtkWidget *window;
  GtkWidget *table;
  GtkWidget *OK;
  GtkWidget *Cancel;
  GtkWidget *Dir;
  GtkWidget *dir;
};

struct _mkdir_d
{
  GtkWidget *dlg;
  GtkWidget *Dir;
  GtkWidget *dir;
};

struct _rename_d
{
  GtkWidget *window;
  GtkWidget *table;
  GtkWidget *OK;
  GtkWidget *Cancel;
  GtkWidget *File;
  GtkWidget *file;
};

struct _delete_d
{
  GtkWidget *window;
  GtkWidget *table;
  GtkWidget *Yes;
  GtkWidget *No;
  GtkWidget *Cancel;
  GtkWidget *File;
};
struct _rmdir_d
{
  GtkWidget *window;
  GtkWidget *table;
  GtkWidget *Yes;
  GtkWidget *No;
  GtkWidget *Cancel;
  GtkWidget *Dir;
};

struct _lsmove
{
  GtkWidget* box;
  GtkWidget* vbox;
  
  GtkWidget* Upload;
  GtkWidget* Download;
};

struct _lstransfer
{
  GtkWidget* box;
  GtkWidget* vbox;

  GSList* group;

  GtkWidget* Text;
  GtkWidget* Binary;
  GtkWidget* Auto;
};

struct _lscontrol
{
  GtkWidget* hbox;

  GtkWidget* Connect;
  GtkWidget* Cancel;
  GtkWidget* ViewLog;
  GtkWidget* Help;
  GtkWidget* Options;
  GtkWidget* About;
  GtkWidget* Exit;
};

struct _lswin
{
  lssystem *remote;
  lssystem *local;
  lsmove   *move;

  lstransfer *transfer;
  lscontrol  *control;

  GtkWidget* app;
  GtkWidget* vbox;
  GtkWidget* hbox;
  GtkWidget* tips;
  GtkWidget* table;
  GtkWidget* sbar;
  GtkWidget* text;

  Connect_d* connect_d;
  ViewLog_d* viewlog_d;
  Options_d* options_d;

  lsprogress_d* get_progress_d;
  lsprogress_d* put_progress_d;

  lsNFTPC *nftpc;
  
  char *localloc;
  char *remoteloc;
  char *localfile;
  char *tempfile;
  
  char *localcwd;
  char *remotecwd;
  
  int local_connected;
  int remote_connected;

  opts* gopts;


  GList* backup; /*backup the config so  the user can cancel mods.*/
};

struct _Connect_d
{
  lsgeneral *general;
  lsstartup *startup;
  lsadvanced *advanced;
  lsfirewall *firewall;

  GtkWidget *hbox;
  GtkWidget *vbox;
  GtkWidget *Notebook;
  GtkWidget *window;
  GtkWidget *OK;
  GtkWidget *Cancel;
  GtkWidget *Apply;
  GtkWidget *Help;
};
struct _ViewLog_d
{
  GtkWidget *window;
  GtkWidget *text;
  GtkWidget *sbar;
  GtkWidget *table;
  
};

struct _Options_d
{
  lssession *session;

  GtkWidget *pbox;
};

struct _lssession
{
  GtkWidget *Session;

  GtkWidget *AutoResume;
  GtkWidget *autoresume;

  GtkWidget *table;
  GtkWidget *box;
  GtkWidget *vbox;
  GtkWidget *hbox;
};

struct _lsgeneral
{
  GtkWidget *General;

  GtkWidget *Profile;
  GtkWidget *profile;
  GtkWidget *Host;
  GtkWidget *host;
  GtkWidget *Type;
  GtkWidget *type;
  GtkWidget *Userid;
  GtkWidget *userid;
  GtkWidget *Password;
  GtkWidget *password;
  GtkWidget *Account;
  GtkWidget *account;
  GtkWidget *Comment;
  GtkWidget *comment;
  GtkWidget *New;
  GtkWidget *Delete;
  GtkWidget *Anonymous;
  GtkWidget *SavePasswd;

  GtkWidget *table;
  GtkWidget *box;
  GtkWidget *vbox;
  GtkWidget *hbox;
  
  GList *prolist;
  GList *typelist;

  site* curedit;
  char* curtext;
  gint c1; /*Used to disconnect the cobmo box!!!*/
  int keyed;
};

struct _lsstartup
{
  GtkWidget *Startup;

  GtkWidget *Remote;
  GtkWidget *remote;
  GtkWidget *Local;
  GtkWidget *local;
  GtkWidget *Commands;
  GtkWidget *commands;
  GtkWidget *RemoteMask;
  GtkWidget *remoteMask;
  GtkWidget *LocalMask;
  GtkWidget *localMask;
  GtkWidget *TimeOffset;
  GtkWidget *timeOffset;

  GtkWidget *table;
  GtkWidget *box;
};

struct _lsadvanced
{
  GtkWidget *Advanced;

  GtkWidget *Retry;
  GtkWidget *retry;
  GtkWidget *retryi;
  GtkWidget *Timeout;
  GtkWidget *timeout;
  GtkWidget *timeouti;
  GtkWidget *Port;
  GtkWidget *port;
  GtkWidget *porti;
  GtkWidget *Passive;
  GtkWidget *passivei;
  
  GtkWidget *table;
  GtkWidget *box;
};

struct _lsfirewall
{
  GtkWidget *Firewall;

  GtkWidget *Host;
  GtkWidget *host;
  GtkWidget *UseFirewall;
  GtkWidget *Userid;
  GtkWidget *userid;
  GtkWidget *Password;
  GtkWidget *password;
  GtkWidget *Port;
  GtkWidget *port;
  GtkWidget *SavePasswd;

  GSList    *group;
  GtkWidget *SITEhostname;
  GtkWidget *USERafter;
  GtkWidget *ProxyOPEN;
  GtkWidget *Transparent;
  GtkWidget *USERnolog;
  GtkWidget *USERfidat;
  GtkWidget *USERremote;
  GtkWidget *USERatat;

  GtkWidget *frame;
  GtkWidget *toptable;
  GtkWidget *btable;
  GtkWidget *vbox;
};

/*Each set must have MAXSORTS worth of elements.*/
static int SortEn[] = {   DIRFILE | ALPHA,
			   FILEDIR| ALPHA,
			   DIRFILE | RALPHA,
			   FILEDIR| RALPHA,
			   ALPHA,
			   RALPHA,

			  DIRFILE | ALPHA,
			   FILEDIR| ALPHA,
			   DIRFILE | RALPHA,
			   FILEDIR| RALPHA,
			   ALPHA,
			   RALPHA,

			  DIRFILE | my_DATE,
			   DIRFILE | RDATE,
			   my_DATE,
			   RDATE,
			   0,
			   0,
			  
			  DIRFILE | my_SIZE,
			   DIRFILE | RSIZE, 
			   my_SIZE, 
			   RSIZE,
			   0,
			   0
};

