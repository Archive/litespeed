#include "lspeed.h"
static gint remote_combo_key_press (GtkWidget             *widget,
				    GdkEventKey           *event,
				    lswin*                 win);

void remote_init (lswin *win)
{
  lssystem *sys= win->remote;
  GtkWidget *entry = GTK_COMBO(sys->directory)->entry;
  
  gtk_signal_connect (GTK_OBJECT 
		      (entry), "key_press_event",
		      (GtkSignalFunc) remote_combo_key_press, win);
  
  /*gtk_widget_grab_focus (entry);*/
}

static gint
remote_combo_key_press (GtkWidget   *widget,
		       GdkEventKey *event,
		       lswin*       win)
{
  lssystem *sys= win->remote;
  GtkWidget *entry = GTK_COMBO(sys->directory)->entry;
  
  char *text;
  
  g_return_val_if_fail (widget != NULL, FALSE);
  g_return_val_if_fail (event != NULL, FALSE);
  
  /*if (event->keyval == GDK_Tab || event->keyval == GDK_Return)*/
  if (event->keyval == GDK_Return)
  {
    gtk_signal_emit_stop_by_name (GTK_OBJECT (widget), "key_press_event");
    
    text = gtk_entry_get_text (GTK_ENTRY (entry));

    if(event->keyval == GDK_Tab)
      start_clist_populate (win, sys, text, TRUE);
    if(event->keyval == GDK_Return)
      start_clist_populate (win, sys, text, FALSE);
    
    return TRUE;
  }
  
  return FALSE;
}

gint remote_key_press (GtkWidget *widget, GdkEventKey *event, 
		       lswin* win)
{
  lssystem *sys= win->remote;
  GtkWidget *entry = GTK_COMBO(sys->directory)->entry;
  
  char *text;
  
  g_return_val_if_fail (widget != NULL, FALSE);
  g_return_val_if_fail (event != NULL, FALSE);
  
  if (event->keyval == GDK_Tab || event->keyval == GDK_Return)
  {
    gtk_signal_emit_stop_by_name (GTK_OBJECT (widget), "key_press_event");
    
    text = gtk_entry_get_text (GTK_ENTRY (entry));
    /*
    if(event->keyval == GDK_Tab)
      remote_populate (win, win->connection);
    */
    if(event->keyval == GDK_Return)
    {
      /*remote_populate (win, win->connection);*/
      text = gtk_entry_get_text(GTK_ENTRY(entry));
      /*FtpStartDir(win, g_strdup(text));*/
    }
    
    return TRUE;
  }
  
  return FALSE;
}
  
/*
void remote_populate(lswin* win, lsconnection* conn)
{
  char* tempfile = conn->tempfile;
  lssystem *sys= win->remote;
  GtkWidget* clist = sys->clist;
  GtkWidget *entry = GTK_COMBO(sys->directory)->entry;

  
  gtk_clist_freeze (GTK_CLIST (clist));
  gtk_clist_clear (GTK_CLIST (clist));

  GList* retlist = parselist(win, sys, tempfile);

  //fillclist(clist, retlist);

  if( !inlist(sys->cbitems, conn->remotecwd) )
  {
    sys->cbitems = g_list_prepend(sys->cbitems, g_strdup(conn->remotecwd));
    gtk_combo_set_popdown_strings (GTK_COMBO (sys->directory), 
				   sys->cbitems);
  }
  
  gtk_entry_set_text(GTK_ENTRY(entry),conn->remotecwd);
  gtk_clist_thaw (GTK_CLIST (clist));

  //cerr << start << endl;
  //msync(Dir, 0, 0);
}
*/

/*
main()
{

  lswin *win;

  remote_populate(win, "/tmp/tmpftpfile");

}
*/
