#include "lspeed.h"

/*Add a slash to the end of the [] if it doesn't already have one.*/
/*
void add_slash(char path[])
{
  if(strlen(path) == 0)
  {
    return;
  }

  if( *(path+strlen(path)-1) != '/' )
  {
    strcat(path, "/");
  }
}
*/

popup_progress(GtkWidget* window, lswin* win)
{
  int x, y, xsize, ysize, xpop, ypop;
    
  gdk_window_get_position ((GdkWindow*)
			   (&GNOME_APP(win->app)->parent_object), &x, &y);
  gdk_window_get_size ((GdkWindow*)
		       (&GNOME_APP(win->app)->parent_object), &xsize, &ysize);
  gtk_widget_size_request (window, &window->requisition);
  xpop = window->requisition.width;
  ypop = window->requisition.height;
  gtk_widget_set_uposition(window,
			   x+xsize/2 - xpop/2, y+ysize/2 - ypop/2);


  gtk_widget_show_now(window);
}

void Upload_cb (GtkWidget *widget, lswin *win)
{
  GtkWidget* window = win->put_progress_d->window;
  int i;
  GtkWidget* clist = win->local->clist;
  lssystem* sys = win->local;
  lsaction* action;
  InfoList *files;
  InfoList *infolist;
  Info *remotecwd;
  GList *list;
  int size;
  int row;
  
  action = g_new0(lsaction,1);
  action->sys = sys;
  action->win = win;
  action->action = LA_NOTHING;

  infolist = sys->infolist;
  list = GTK_CLIST(clist)->selection;

  size = g_list_length(list);

  files = InfoList__alloc();
  files->_maximum = 0;
  files->_length = size;
  files->_buffer = g_new0(Info, size);
  files->_release = 0;

  for(i=0; list; i++)
  {
    row = (gint) list->data;
    
    memcpy(&(files->_buffer[i]), &(infolist->_buffer[row]), sizeof(Info));
    
    list = list->next;
  }  
  
  popup_progress(window, win);

  /*
  remotecwd = Info__alloc();
  remotecwd->perms = g_strdup("");
  remotecwd->user = g_strdup("user");
  remotecwd->group = g_strdup("group");
  remotecwd->loc_ior = g_strdup("loc_ior");
  */
  
  NFTPLoc_PutRecur(sys->filer, remotecwd, files, action, &sys->filer_ev);
}


void Download_cb (GtkWidget *widget, lswin *win)
{
  GtkWidget* window = win->get_progress_d->window;
  Info* item;
  int i;
  GtkWidget* clist = win->remote->clist;
  lssystem* sys = win->remote;
  lsaction* action;
  InfoList *files;
  InfoList *infolist;
  Info *localcwd;
  GList *list;
  int size;
  int row;
  
  action = g_new0(lsaction,1);
  action->sys = sys;
  action->win = win;
  action->action = LA_NOTHING;

  infolist = sys->infolist;

  fprintf(stderr, "sys = %i\n", sys);
  fprintf(stderr, "infolist = %i\n", infolist);
  list = GTK_CLIST(clist)->selection;

  size = g_list_length(list);
  fprintf(stderr, "size = %i\n", size);

  files = InfoList__alloc();
  files->_maximum = 0;
  files->_length = size;
  files->_buffer = g_new0(Info, size);
  files->_release = 0;

  for(i=0; list; i++)
  {
    row = (gint) list->data;

    memcpy(&(files->_buffer[i]), &(infolist->_buffer[row]), sizeof(Info));
    
    fprintf(stderr, "files->_length = %i\n", files->_length);
    fprintf(stderr, "infolist = %i, row = %i\n", infolist, row);

    fprintf(stderr, "infolist[%i].url.name = %s\n", row, 
	    infolist->_buffer[row].url.name);
    
    /*
    fprintf(stderr, "files[%i].name = %s\n", i, files->_buffer[i].name);
    */
    
    list = list->next;
  }  

  /*popup_progress(window, win);*/

  /*
  localcwd = Info__alloc();
  localcwd->complete = g_strdup("/usr/freebsd/");
  localcwd->name = g_strdup("freebsd");
  localcwd->perms = g_strdup("");
  localcwd->user = g_strdup("user");
  localcwd->group = g_strdup("group");
  localcwd->loc_ior = g_strdup("loc_ior");
  */
  
  NFTPLoc_GetRecur(sys->lister, localcwd, files, action, &sys->filer_ev);
}


