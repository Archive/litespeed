#include "lspeed.h"

CORBA_Object name_service;


lsNCS* LS_and_CORBA_init(int *argc, char** argv)
{
  FILE* host_ior_fp;

  lsNCS* server;
  char IOR[4000];
  char *name = "edox";
  int n = 1;


  server = g_new0(lsNCS,1);
  printf("in LS_and_CORBA_init\n");

  IIOPAddConnectionHandler = orb_add_connection;
  IIOPRemoveConnectionHandler = orb_remove_connection;

  /*find the master CORBA orb*/
  CORBA_exception_init(&server->ev);
  /*
  server->ORB = gnome_CORBA_init("litespeed", NULL, argc, argv, 
	  0, &server->ev);
  */

  server->ORB = CORBA_ORB_init(&n, &name /* dummy */, "orbit-local-orb", &server->ev);

  ORBit_custom_run_setup(server->ORB, &server->ev);

  printf("got ORB\n");

  /*find the NCS master server*/
  host_ior_fp = fopen(ncs_get_default_ior_filename(), "r");
  fscanf(host_ior_fp, "%s", IOR);
  fclose(host_ior_fp);

  server->NCS = CORBA_ORB_string_to_object(server->ORB, IOR, &server->ev);
  
  printf("master NCS = %i\n", server->NCS);
  if(!server->NCS)
  {
    printf("Please run the master NCS\n");
    exit(1);
  }
  return server;
}

lsNFTPC* ls_cb_CORBA_new(CORBA_ORB orb, CORBA_Environment* ev)
{
  lsNFTPC *cb_server;
  PortableServer_POA poa;

  cb_server = g_new0(lsNFTPC, 1);

  cb_server->poa = (PortableServer_POA)CORBA_ORB_resolve_initial_references
                              (orb, "RootPOA", ev);

  cb_server->NFTPC = impl_NFTPC__create_mine
      (cb_server->poa, &cb_server->servant, &cb_server->ev);
  PortableServer_POAManager_activate
      (PortableServer_POA__get_the_POAManager(cb_server->poa, &cb_server->ev), 
       &cb_server->ev);


  /*
  name_service = gnome_name_service_get();
  goad_server_register(name_service, cb_server->NFTPC, "litespeed_cb", 
			      "object", &cb_server->ev);
			      */
  return cb_server;
}


int
ls_nftpc_open_cb(n_action* unused, CORBA_long ret, 
	CORBA_Object loc, lsaction* list_action)
{
  lsaction * file_action;
  n_action* nlist_action;

  lswin* win;
  lssystem* sys;
  char* cwd;

  fprintf(stderr, "IN ls_nftpc_open_cb!!!\n");
  fprintf(stderr, "action = %i\n", list_action->action);
  fflush(stderr);

  file_action = g_new0(lsaction, 1);
		  
  win = list_action->win;
  sys = list_action->sys;

  file_action->win = win;
  file_action->sys = sys;
  file_action->action = LA_NOTHING;

  if(sys == win->local)
  {
    cwd = win->localcwd;
  }
  else
  {
    cwd = win->remotecwd;
  }

  sys->lister = (NCS_Loc)loc;

  fprintf(stderr, "cwd == %s\n", cwd);

  nlist_action = n_loc_list_new(sys->lister, cwd);
  list_action->action = LA_FILL_CLIST;
  n_signal_connect(nlist_action, "done",
	  N_SIGNAL_FUNC(ls_nftpc_list_cb), list_action);
  n_action_activate(nlist_action);

  return TRUE;
}


int
ls_nftpc_list_cb(n_action* unused, CORBA_long ret, 
	InfoList *list, lsaction* action)
{
  lswin* win;
  lssystem* sys;
  
  fprintf(stderr, "in ls_nftpc_list_cb\n");
  win = action->win;
  sys = action->sys;
  
  sys->infolist = InfoList_clone(list);

  fprintf(stderr, "sys = %i\n", sys);
  fprintf(stderr, "infolist = %i\n", sys->infolist);
  /*sort_infolist(sys, list);*/
  
  fprintf(stderr, "GOING TO FILL CLIST\n");
  gtk_clist_freeze (GTK_CLIST (sys->clist));
  fillclist(sys, sys->infolist);
  gtk_clist_thaw (GTK_CLIST (sys->clist));

  return TRUE;
}

int
ls_nftpc_getrecur_cb(n_action* unused, CORBA_long ret, 
	InfoList *list, lsaction* action)
{
  lswin* win;
  lssystem* sys;
}

