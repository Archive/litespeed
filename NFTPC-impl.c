
#include "NFTPC-impl.h"
                  /*** epv structures ***/





 PortableServer_ServantBase__epv impl_Metadata_base_epv = {
NULL, /* _private data */
(gpointer)&impl_Metadata__destroy, /* finalize routine */
NULL, /* default_POA routine */
};
 POA_Metadata__epv impl_Metadata_epv = {
NULL, /* _private */
(gpointer)&impl_Metadata__get_metadata,
(gpointer)&impl_Metadata__set_metadata,

};
 PortableServer_ServantBase__epv impl_NCC_base_epv = {
NULL, /* _private data */
(gpointer)&impl_NFTPC__destroy, /* finalize routine */
NULL, /* default_POA routine */
};
 POA_NCC__epv impl_NCC_epv = {
NULL, /* _private */
(gpointer)&impl_NFTPC_All_cb,

(gpointer)&impl_NFTPC_Open_cb,

(gpointer)&impl_NFTPC_Close_cb,

(gpointer)&impl_NFTPC_StartRead_cb,

(gpointer)&impl_NFTPC_StartWrite_cb,

(gpointer)&impl_NFTPC_Read_cb,

(gpointer)&impl_NFTPC_Write_cb,

(gpointer)&impl_NFTPC_GetMetaData_cb,

(gpointer)&impl_NFTPC_SetMetaData_cb,

(gpointer)&impl_NFTPC_GetResource_cb,

(gpointer)&impl_NFTPC_SetResource_cb,

(gpointer)&impl_NFTPC_GetCurrentBase_cb,

(gpointer)&impl_NFTPC_Create_cb,

(gpointer)&impl_NFTPC_Delete_cb,

(gpointer)&impl_NFTPC_Move_cb,

(gpointer)&impl_NFTPC_Copy_cb,

(gpointer)&impl_NFTPC_List_cb,

(gpointer)&impl_NFTPC_Aliases_cb,

(gpointer)&impl_NFTPC_Pause_cb,

(gpointer)&impl_NFTPC_Resume_cb,

(gpointer)&impl_NFTPC_Abort_cb,

(gpointer)&impl_NFTPC_NewMetadata_cb,

(gpointer)&impl_NFTPC_NewLocation_cb,

(gpointer)&impl_NFTPC_Response_cb,

};
 PortableServer_ServantBase__epv impl_NFTPC_base_epv = {
NULL, /* _private data */
(gpointer)&impl_NFTPC__destroy, /* finalize routine */
NULL, /* default_POA routine */
};
 POA_NFTPC__epv impl_NFTPC_epv = {
NULL, /* _private */
(gpointer)&impl_NFTPC_GetRecur_cb,

(gpointer)&impl_NFTPC_PutRecur_cb,

}; POA_NCC__epv impl_NFTPC_NCC_epv = {
NULL, /* _private */
(gpointer)&impl_NFTPC_All_cb,
(gpointer)&impl_NFTPC_Open_cb,
(gpointer)&impl_NFTPC_Close_cb,
(gpointer)&impl_NFTPC_StartRead_cb,
(gpointer)&impl_NFTPC_StartWrite_cb,
(gpointer)&impl_NFTPC_Read_cb,
(gpointer)&impl_NFTPC_Write_cb,
(gpointer)&impl_NFTPC_GetMetaData_cb,
(gpointer)&impl_NFTPC_SetMetaData_cb,
(gpointer)&impl_NFTPC_GetResource_cb,
(gpointer)&impl_NFTPC_SetResource_cb,
(gpointer)&impl_NFTPC_GetCurrentBase_cb,
(gpointer)&impl_NFTPC_Create_cb,
(gpointer)&impl_NFTPC_Delete_cb,
(gpointer)&impl_NFTPC_Move_cb,
(gpointer)&impl_NFTPC_Copy_cb,
(gpointer)&impl_NFTPC_List_cb,
(gpointer)&impl_NFTPC_Aliases_cb,
(gpointer)&impl_NFTPC_Pause_cb,
(gpointer)&impl_NFTPC_Resume_cb,
(gpointer)&impl_NFTPC_Abort_cb,
(gpointer)&impl_NFTPC_NewMetadata_cb,
(gpointer)&impl_NFTPC_NewLocation_cb,
(gpointer)&impl_NFTPC_Response_cb,
};

/*** vepv structures ***/





 POA_Metadata__vepv impl_Metadata_vepv = {
&impl_Metadata_base_epv,
&impl_Metadata_epv,
};
 POA_NCC__vepv impl_NCC_vepv = {
&impl_NCC_base_epv,
&impl_NCC_epv,
};
 POA_NFTPC__vepv impl_NFTPC_vepv = {
&impl_NFTPC_base_epv,
&impl_NFTPC_NCC_epv,
&impl_NFTPC_epv,
};

/*** Stub implementations ***/





 Metadata impl_Metadata__create(PortableServer_POA poa, CORBA_Environment *ev)
{
Metadata retval;
impl_POA_Metadata *newservant;
PortableServer_ObjectId *objid;

newservant = g_new0(impl_POA_Metadata, 1);
newservant->servant.vepv = &impl_Metadata_vepv;
newservant->poa = poa;
POA_Metadata__init((PortableServer_Servant)newservant, ev);
objid = PortableServer_POA_activate_object(poa, newservant, ev);
CORBA_free(objid);
retval = PortableServer_POA_servant_to_reference(poa, newservant, ev);

return retval;
}

/* You shouldn't call this routine directly without first deactivating the servant... */
 void
impl_Metadata__destroy(impl_POA_Metadata *servant, CORBA_Environment *ev)
{

POA_Metadata__fini((PortableServer_Servant)servant, ev);
g_free(servant);
}

Info*
impl_Metadata__get_metadata(impl_POA_Metadata *servant,
CORBA_Environment *ev)
{
Info* retval;

return retval;
}

void
impl_Metadata__set_metadata(impl_POA_Metadata *servant,
Info* value,
CORBA_Environment *ev)
{
}



 NCC impl_NCC__create(PortableServer_POA poa, CORBA_Environment *ev)
{
NCC retval;
impl_POA_NCC *newservant;
PortableServer_ObjectId *objid;

newservant = g_new0(impl_POA_NCC, 1);
newservant->servant.vepv = &impl_NCC_vepv;
newservant->poa = poa;
POA_NCC__init((PortableServer_Servant)newservant, ev);
objid = PortableServer_POA_activate_object(poa, newservant, ev);
CORBA_free(objid);
retval = PortableServer_POA_servant_to_reference(poa, newservant, ev);

return retval;
}

/* You shouldn't call this routine directly without first deactivating the servant... */
 void
impl_NCC__destroy(impl_POA_NCC *servant, CORBA_Environment *ev)
{

POA_NCC__fini((PortableServer_Servant)servant, ev);
g_free(servant);
}

void
impl_NCC_All_cb(impl_POA_NCC *servant,
CORBA_long sn,
CORBA_long pointer,
CORBA_Environment *ev)
{
}


void
impl_NCC_Open_cb(impl_POA_NCC *servant,
CORBA_long ret,
CORBA_Object loc,
CORBA_long pointer,
CORBA_Environment *ev)
{
}


void
impl_NCC_Close_cb(impl_POA_NCC *servant,
CORBA_long ret,
CORBA_long pointer,
CORBA_Environment *ev)
{
}


void
impl_NCC_StartRead_cb(impl_POA_NCC *servant,
CORBA_long ret,
CORBA_long id,
CORBA_char * local,
CORBA_long pointer,
CORBA_Environment *ev)
{
}


void
impl_NCC_StartWrite_cb(impl_POA_NCC *servant,
CORBA_long ret,
CORBA_long id,
CORBA_char * local,
CORBA_long pointer,
CORBA_Environment *ev)
{
}


void
impl_NCC_Read_cb(impl_POA_NCC *servant,
CORBA_long ret,
Buffer* buf,
CORBA_long pointer,
CORBA_Environment *ev)
{
}


void
impl_NCC_Write_cb(impl_POA_NCC *servant,
CORBA_long ret,
CORBA_long pointer,
CORBA_Environment *ev)
{
}


void
impl_NCC_GetMetaData_cb(impl_POA_NCC *servant,
CORBA_long ret,
Metadata data,
CORBA_long pointer,
CORBA_Environment *ev)
{
}


void
impl_NCC_SetMetaData_cb(impl_POA_NCC *servant,
CORBA_long ret,
CORBA_long pointer,
CORBA_Environment *ev)
{
}


void
impl_NCC_GetResource_cb(impl_POA_NCC *servant,
CORBA_long ret,
CORBA_char * resource,
CORBA_long pointer,
CORBA_Environment *ev)
{
}


void
impl_NCC_SetResource_cb(impl_POA_NCC *servant,
CORBA_long ret,
CORBA_long pointer,
CORBA_Environment *ev)
{
}


void
impl_NCC_GetCurrentBase_cb(impl_POA_NCC *servant,
CORBA_long ret,
CORBA_char * base,
CORBA_long pointer,
CORBA_Environment *ev)
{
}


void
impl_NCC_Create_cb(impl_POA_NCC *servant,
CORBA_long ret,
CORBA_long pointer,
CORBA_Environment *ev)
{
}


void
impl_NCC_Delete_cb(impl_POA_NCC *servant,
CORBA_long ret,
CORBA_long pointer,
CORBA_Environment *ev)
{
}


void
impl_NCC_Move_cb(impl_POA_NCC *servant,
CORBA_long ret,
CORBA_long pointer,
CORBA_Environment *ev)
{
}


void
impl_NCC_Copy_cb(impl_POA_NCC *servant,
CORBA_long ret,
CORBA_long pointer,
CORBA_Environment *ev)
{
}


void
impl_NCC_List_cb(impl_POA_NCC *servant,
CORBA_long ret,
InfoList* list,
CORBA_long pointer,
CORBA_Environment *ev)
{
}


void
impl_NCC_Aliases_cb(impl_POA_NCC *servant,
CORBA_long ret,
CORBA_Object loc,
CORBA_long pointer,
CORBA_Environment *ev)
{
}


void
impl_NCC_Pause_cb(impl_POA_NCC *servant,
CORBA_long ret,
CORBA_long pointer,
CORBA_Environment *ev)
{
}


void
impl_NCC_Resume_cb(impl_POA_NCC *servant,
CORBA_long ret,
CORBA_long pointer,
CORBA_Environment *ev)
{
}


void
impl_NCC_Abort_cb(impl_POA_NCC *servant,
CORBA_long ret,
CORBA_long pointer,
CORBA_Environment *ev)
{
}


void
impl_NCC_NewMetadata_cb(impl_POA_NCC *servant,
CORBA_long ret,
Metadata data,
CORBA_long pointer,
CORBA_Environment *ev)
{
}


void
impl_NCC_NewLocation_cb(impl_POA_NCC *servant,
CORBA_long ret,
CORBA_Object loc,
CORBA_long pointer,
CORBA_Environment *ev)
{
}


void
impl_NCC_Response_cb(impl_POA_NCC *servant,
CORBA_long ret,
CORBA_char * resp,
CORBA_long pointer,
CORBA_Environment *ev)
{
}



 NFTPC impl_NFTPC__create(PortableServer_POA poa, CORBA_Environment *ev)
{
NFTPC retval;
impl_POA_NFTPC *newservant;
PortableServer_ObjectId *objid;

newservant = g_new0(impl_POA_NFTPC, 1);
newservant->servant.vepv = &impl_NFTPC_vepv;
newservant->poa = poa;
POA_NFTPC__init((PortableServer_Servant)newservant, ev);
objid = PortableServer_POA_activate_object(poa, newservant, ev);
CORBA_free(objid);
retval = PortableServer_POA_servant_to_reference(poa, newservant, ev);

return retval;
}

/* You shouldn't call this routine directly without first deactivating the servant... */
 void
impl_NFTPC__destroy(impl_POA_NFTPC *servant, CORBA_Environment *ev)
{

POA_NFTPC__fini((PortableServer_Servant)servant, ev);
g_free(servant);
}

void
impl_NFTPC_GetRecur_cb(impl_POA_NFTPC *servant,
CORBA_long ret,
CORBA_long pointer,
CORBA_Environment *ev)
{
}


void
impl_NFTPC_PutRecur_cb(impl_POA_NFTPC *servant,
CORBA_long ret,
CORBA_long pointer,
CORBA_Environment *ev)
{
}


void
impl_NFTPC_All_cb(impl_POA_NFTPC *servant,
CORBA_long sn,
CORBA_long pointer,
CORBA_Environment *ev)
{
}

void
impl_NFTPC_Open_cb(impl_POA_NFTPC *servant,
CORBA_long ret,
CORBA_Object loc,
CORBA_long pointer,
CORBA_Environment *ev)
{
}

void
impl_NFTPC_Close_cb(impl_POA_NFTPC *servant,
CORBA_long ret,
CORBA_long pointer,
CORBA_Environment *ev)
{
}

void
impl_NFTPC_StartRead_cb(impl_POA_NFTPC *servant,
CORBA_long ret,
CORBA_long id,
CORBA_char * local,
CORBA_long pointer,
CORBA_Environment *ev)
{
}

void
impl_NFTPC_StartWrite_cb(impl_POA_NFTPC *servant,
CORBA_long ret,
CORBA_long id,
CORBA_char * local,
CORBA_long pointer,
CORBA_Environment *ev)
{
}

void
impl_NFTPC_Read_cb(impl_POA_NFTPC *servant,
CORBA_long ret,
Buffer* buf,
CORBA_long pointer,
CORBA_Environment *ev)
{
}

void
impl_NFTPC_Write_cb(impl_POA_NFTPC *servant,
CORBA_long ret,
CORBA_long pointer,
CORBA_Environment *ev)
{
}

void
impl_NFTPC_GetMetaData_cb(impl_POA_NFTPC *servant,
CORBA_long ret,
Metadata data,
CORBA_long pointer,
CORBA_Environment *ev)
{
}

void
impl_NFTPC_SetMetaData_cb(impl_POA_NFTPC *servant,
CORBA_long ret,
CORBA_long pointer,
CORBA_Environment *ev)
{
}

void
impl_NFTPC_GetResource_cb(impl_POA_NFTPC *servant,
CORBA_long ret,
CORBA_char * resource,
CORBA_long pointer,
CORBA_Environment *ev)
{
}

void
impl_NFTPC_SetResource_cb(impl_POA_NFTPC *servant,
CORBA_long ret,
CORBA_long pointer,
CORBA_Environment *ev)
{
}

void
impl_NFTPC_GetCurrentBase_cb(impl_POA_NFTPC *servant,
CORBA_long ret,
CORBA_char * base,
CORBA_long pointer,
CORBA_Environment *ev)
{
}

void
impl_NFTPC_Create_cb(impl_POA_NFTPC *servant,
CORBA_long ret,
CORBA_long pointer,
CORBA_Environment *ev)
{
}

void
impl_NFTPC_Delete_cb(impl_POA_NFTPC *servant,
CORBA_long ret,
CORBA_long pointer,
CORBA_Environment *ev)
{
}

void
impl_NFTPC_Move_cb(impl_POA_NFTPC *servant,
CORBA_long ret,
CORBA_long pointer,
CORBA_Environment *ev)
{
}

void
impl_NFTPC_Copy_cb(impl_POA_NFTPC *servant,
CORBA_long ret,
CORBA_long pointer,
CORBA_Environment *ev)
{
}

void
impl_NFTPC_List_cb(impl_POA_NFTPC *servant,
CORBA_long ret,
InfoList* list,
CORBA_long pointer,
CORBA_Environment *ev)
{
}

void
impl_NFTPC_Aliases_cb(impl_POA_NFTPC *servant,
CORBA_long ret,
CORBA_Object loc,
CORBA_long pointer,
CORBA_Environment *ev)
{
}

void
impl_NFTPC_Pause_cb(impl_POA_NFTPC *servant,
CORBA_long ret,
CORBA_long pointer,
CORBA_Environment *ev)
{
}

void
impl_NFTPC_Resume_cb(impl_POA_NFTPC *servant,
CORBA_long ret,
CORBA_long pointer,
CORBA_Environment *ev)
{
}

void
impl_NFTPC_Abort_cb(impl_POA_NFTPC *servant,
CORBA_long ret,
CORBA_long pointer,
CORBA_Environment *ev)
{
}

void
impl_NFTPC_NewMetadata_cb(impl_POA_NFTPC *servant,
CORBA_long ret,
Metadata data,
CORBA_long pointer,
CORBA_Environment *ev)
{
}

void
impl_NFTPC_NewLocation_cb(impl_POA_NFTPC *servant,
CORBA_long ret,
CORBA_Object loc,
CORBA_long pointer,
CORBA_Environment *ev)
{
}

void
impl_NFTPC_Response_cb(impl_POA_NFTPC *servant,
CORBA_long ret,
CORBA_char * resp,
CORBA_long pointer,
CORBA_Environment *ev)
{
}



NFTPC impl_NFTPC__create_mine(  
        PortableServer_POA poa, 
        impl_POA_NFTPC **servant,
        CORBA_Environment *ev)
{
  NFTPC retval;
  impl_POA_NFTPC *newservant;
  PortableServer_ObjectId *objid;
  newservant = g_new0(impl_POA_NFTPC, 1);
  newservant->servant.vepv = &impl_NFTPC_vepv;
  newservant->poa = poa;
  POA_NFTPC__init((PortableServer_Servant)newservant, ev);
  objid = PortableServer_POA_activate_object(poa, newservant, ev);
  CORBA_free(objid);
  retval = PortableServer_POA_servant_to_reference(poa, newservant, ev);
  *servant = newservant;
  return retval;
}
