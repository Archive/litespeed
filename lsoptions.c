#include "lspeed.h"

static void prop_set(GnomePropertyBox * pb, gint page,  Options_d *options_d)
{
   g_print("Resume is %d\n", GTK_TOGGLE_BUTTON(options_d->session->AutoResume)->active);
}

void Options_cb (GtkWidget *widget, lswin* win)
{
  /*ViewLog_d *viewlog = win->viewlog_d;*/

  gtk_widget_show(win->options_d->pbox);
}

lssession* new_lssession(GtkWidget *pbox)
{
  lssession* session;
  lssession* s;

  session = g_new0(lssession,1);
  s = session;
  
  s->Session = label_new("Session");
  s->vbox = gtk_vbox_new(FALSE, 8);
  gtk_widget_show(s->vbox);
  s->table =  gtk_table_new(4, 4, FALSE);
  gtk_widget_show(s->table);
  gtk_container_border_width (GTK_CONTAINER (s->vbox), 15);
  gtk_box_pack_start(GTK_BOX(s->vbox), s->table, FALSE, FALSE, 0);

  s->AutoResume = check_new("Automaticly resume failed transmissions.");
  gtk_signal_connect_object(GTK_OBJECT(s->AutoResume), "clicked",
                            GTK_SIGNAL_FUNC(gnome_property_box_changed),
			    GTK_OBJECT(pbox));
  /*s->autoresume = entry_new();*/
  
  /*gtk_table_attach_defaults (GTK_TABLE(s->table), s->autoresume, 0, 1, 0, 1);*/
  gtk_table_attach_defaults (GTK_TABLE(s->table), s->AutoResume, 0, 1, 0, 1);
  gnome_property_box_append_page(GNOME_PROPERTY_BOX(pbox), s->vbox, s->Session);
  return session;
  
}


Options_d * new_Options_d()
{
  Options_d* options_d;
  Options_d* o;

  options_d = g_new0(Options_d,1);
  o = options_d;

  
  o->pbox = gnome_property_box_new();
  gtk_container_border_width (GTK_CONTAINER (o->pbox), 15);

  gtk_signal_connect(GTK_OBJECT(o->pbox), "apply",
                     GTK_SIGNAL_FUNC(prop_set), o);

  gnome_dialog_close_hides(GNOME_DIALOG(o->pbox), TRUE);
  o->session = new_lssession(o->pbox);

  return options_d;
}
