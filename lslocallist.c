#include "lspeed.h"
#include "fnmatch.h"


void local_init (lswin *win);

static gint local_combo_key_press     (GtkWidget             *widget,
				       GdkEventKey           *event,
				       lswin*                 win);

static void pop_abort (lswin *win);

/* Saves errno when something cmpl does fails. */
static gint cmpl_errno;

void local_init (lswin *win)
{
  lssystem *sys= win->local;
  
  GtkWidget *entry = GTK_COMBO(sys->directory)->entry;
  
  gtk_signal_connect (GTK_OBJECT 
		      (entry), "key_press_event",
		      (GtkSignalFunc) local_combo_key_press, win);
  
  start_clist_populate (win, sys, "", FALSE);
  
  /*gtk_widget_grab_focus (entry);*/
}

static gint
local_combo_key_press (GtkWidget   *widget,
		       GdkEventKey *event,
		       lswin*       win)
{
  lssystem *sys= win->local;
  GtkWidget *entry = GTK_COMBO(sys->directory)->entry;
  
  char *text;
  
  g_return_val_if_fail (widget != NULL, FALSE);
  g_return_val_if_fail (event != NULL, FALSE);
  
  /*if (event->keyval == GDK_Tab || event->keyval == GDK_Return)*/
  if (event->keyval == GDK_Return)
  {
    gtk_signal_emit_stop_by_name (GTK_OBJECT (widget), "key_press_event");
    
    text = gtk_entry_get_text (GTK_ENTRY (entry));
    if(event->keyval == GDK_Tab)
      start_clist_populate (win, sys, text, TRUE);
    if(event->keyval == GDK_Return)
      start_clist_populate (win, sys, text, FALSE);
    
    return TRUE;
  }
  
  return FALSE;
}

int inlist(GList *list, char *text)
{
  char *txt;
  while (list)
  {
    txt = (char *) list->data;
    if(strcmp(txt, text) == 0)
    {
      return 1;
    }
    list = list->next;
  }  
  return 0;
}

void fillclist(lssystem* sys, InfoList* list)
{
  GtkWidget* clist = sys->clist;
  int i;
  struct tm* timest;
  gchar* text[5] = {NULL, NULL, NULL, NULL, NULL};
  char temp[256]={'\0'};
  Info* item;


  fprintf(stderr, "list = %i\n", list);
  
  if(!list) 
  {
    return;
  }

  gtk_clist_clear (GTK_CLIST (clist));
    
  fprintf(stderr, "list->_length = %i\n", list->_length);
  for(i=0; i < list->_length; i++)
  {
    item = &list->_buffer[i];
    sprintf(temp, "%lu", item->Ntype);
    text[0] = g_strdup(temp);

    text[2] = g_strdup(item->perms);

    sprintf(temp, "%lu", item->Nsize);
    text[3] = g_strdup(temp);


    /*sprintf(temp, "%lu", item->date);*/
    /*FIXME: date stuff*/
    /*fprintf(stderr, "mtime = %i\n", item->Nmtime);*/
    timest = localtime((const long int*)&item->Nmtime);
    strftime(temp, 255, "%D %r", timest);
    text[4] = g_strdup(temp);

    /*fprintf(stderr, "name = %s\n", item->name);*/
    text[1] = g_strdup(item->url.name);
    gtk_clist_append (GTK_CLIST (clist), text);
  }
}

void start_clist_populate(lswin* win, lssystem *sys, char* path, int complete)
{
  GtkWidget* clist = sys->clist;
  GtkWidget *entry = GTK_COMBO(sys->directory)->entry;
  lsaction* action;
  
  n_action* list_action;

  fprintf(stderr, "path = %s\n", path);
  
  /*gtk_clist_freeze (GTK_CLIST (clist));*/

  /*FIXME: freeze interface*/
  
  action = g_new0(lsaction,1);
  action->sys = sys;
  action->win = win;
  action->action = LA_FILL_CLIST;

  if(sys->lister)
  {
    win->remotecwd = g_strdup(path);

    list_action = n_loc_list_new(sys->lister, path);
    n_signal_connect(list_action, "done",
	    N_SIGNAL_FUNC(ls_nftpc_list_cb), action);
    n_action_activate(list_action);

    /*
    NCS_Loc_List(sys->lister, path, action, &sys->lister_ev);
    */
  }
  else
  {
    fprintf(stderr, "error: lister not defined\n");
  }
  
  if( !inlist(sys->cbitems, path) )
  {
    sys->cbitems = g_list_prepend(sys->cbitems, g_strdup(path));
    gtk_combo_set_popdown_strings (GTK_COMBO (sys->directory), 
				   sys->cbitems);
  }
  
  /*gtk_entry_set_text(GTK_ENTRY(entry),path);*/
}


static void
pop_abort (lswin *win)
{
  gchar err_buf[256];

  /*  BEEP gdk_beep();  */
  
}

void
delete_file_confirmed (GtkWidget *widget, lssystem* sys, lswin* win)
{
  GtkWidget* clist = sys->clist;
  
  int haderror = 0, row;
  
  gchar *path;
  char *txt;
  gchar *full_path;
  gchar *buf;
  
  GList* list = GTK_CLIST(clist)->selection;
  
  while (list)
  {
    row = (gint) list->data;
    gtk_clist_get_text (GTK_CLIST (clist), row, 1, &txt);
    full_path = g_strconcat (path, "/", txt, NULL);
    if ( (unlink (full_path) < 0) )
    {
      haderror = 1; 
      buf = g_strconcat ("Error deleting file \"", txt, "\":  ",
			 g_strerror(errno), "\n", NULL);

      lsStatusText (win, buf, LC_ERROR);
    }
    g_free (full_path);
    list = list->next;
  } 

  /*start_clist_populate (win, sys, "", FALSE)*/
} 

#if 0
lslistitem* file_to_item(char* filename)
{
  struct stat parbuf;
  lslistitem *item= new lslistitem;
  item->name = g_strdup(filename);
  if (stat(filename, &parbuf) < 0)
  {
    item->date = 0;
    item->size = 0;
    item->type = LINK;
  }
  else
  {
    item->date = int(parbuf.st_ctime); 
    item->size = int(parbuf.st_size); 
    /*item->type = DIRECTORY;*/
    if(parbuf.st_mode & S_IFDIR)
    {
      item->type = DIRECTORY;
    }
    else
    {
      item->type = REGULAR;
    }
  }
  
}
#endif /*0*/
