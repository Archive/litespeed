#include "lspeed.h"

void show_rename(lswin *win);

void clist_select_row (int row, int state, lssystem *sys) {
  /*struct server *s;*/
  GList *list = GTK_CLIST (sys->clist)->selection;
  int i;
  int low, high;

  if (state & GDK_CONTROL_MASK) {       /* add to selection */
    if (g_list_find (list, (gpointer) row))
      gtk_clist_unselect_row (GTK_CLIST (sys->clist), row, 0);
    else
      gtk_clist_select_row (GTK_CLIST (sys->clist), row, 0);

    sys->last_clicked_row = row;
  }
  else if (state & GDK_SHIFT_MASK) {    /* extend selection */
    if (row >= sys->last_clicked_row) {
      low = sys->last_clicked_row;
      high = row;
    }
    else {
      low = row;
      high = sys->last_clicked_row;
    }
 
    while (list) {
      i = (int) list->data;
      list = list->next;
      if (i < low || i > high)
        gtk_clist_unselect_row (GTK_CLIST (sys->clist), i, 0);
    }
    
    list = GTK_CLIST (sys->clist)->selection;
     
    for (i = low; i <= high; i++) {
      if (!g_list_find (list, (gpointer) i))
        gtk_clist_select_row (GTK_CLIST (sys->clist), i, 0);
    }
  }
  else {
    while (list) {
      i = (int) list->data;
      list = list->next;
      if (i != row)
        gtk_clist_unselect_row (GTK_CLIST (sys->clist), i, 0);
    }
    
    if (!GTK_CLIST (sys->clist)->selection)
      gtk_clist_select_row (GTK_CLIST (sys->clist), row, 0);
     
    sys->last_clicked_row = row;
  }  
  
  /*sync_selection ();*/
}

void do_double_click(lswin *win, lssystem* sys, GtkWidget* clist)
{
  GtkWidget * local_clist = win->local->clist;
  GtkWidget * remote_clist = win->remote->clist;
  GList* list;
  GtkWidget *entry;

  Info* item;
  int row;
  char getcwd_buf[2*MAXPATHLEN];
  char* localcwd;
  char* remotecwd;
  char *txt;
  
  /*listing*/
  n_action* list_action;

  lsaction * action;
  
  sys->lastclick = 0;

  entry = GTK_COMBO(sys->directory)->entry;

  localcwd = win->localcwd;
  remotecwd = win->remotecwd;
  
  list = GTK_CLIST(clist)->selection;

  row = (gint) list->data;
  item = &sys->infolist->_buffer[row];
  txt = g_strdup(item->url.name);
  
  if(clist == local_clist)
  {
    GtkWidget *clist = local_clist;

    if( DIRECTORY == item->Ntype || 
	LINK == item->Ntype)
    {
      strcpy (getcwd_buf,  localcwd);
      add_slash(getcwd_buf);
      strcat(getcwd_buf, txt);

      win->localcwd = g_strdup(getcwd_buf);
      gtk_entry_set_text(GTK_ENTRY(entry),getcwd_buf);
      start_clist_populate (win, sys, getcwd_buf, FALSE);
    }
    else
    {
      Upload_cb(clist, win);
    }

  }

  else /* clist must be remote_list*/
  {
    GtkWidget *clist = remote_clist;

    if(DIRECTORY == item->Ntype || LINK == item->Ntype)
    {
      strcpy (getcwd_buf,  remotecwd);
      add_slash(getcwd_buf);
      strcat(getcwd_buf, txt);

      win->remotecwd = g_strdup(getcwd_buf);
      gtk_entry_set_text(GTK_ENTRY(entry),getcwd_buf);
      
      action = g_new0(lsaction, 1);
      action->win = win;
      action->sys = sys;
      action->action = LA_FILL_CLIST;

      win->remotecwd = g_strdup(getcwd_buf);      

      fprintf(stderr, "getcwd_buf = %s\n", getcwd_buf);

      list_action = n_loc_list_new(sys->lister, getcwd_buf);
      n_signal_connect(list_action, "done", 
	      N_SIGNAL_FUNC(ls_nftpc_list_cb), action);
      n_action_activate(list_action);

      /*
      NCS_Loc_List(sys->lister, getcwd_buf, action, &sys->lister_ev);
      */
    }
    else
    {
      /*Download_cb(clist, (void*)win);*/
    }
  }
}
  
/*
void clist_drag_cb (GtkWidget *widget, GdkEventDragRequest *event, lswin* win)
{
  g_print("dragging type %s", event->data_type);
  
}
*/

/*
void clist_drop_highlight_cb (GtkWidget *widget, GdkEvent *event)
{
  g_print("drop highlight\n");
  if (event->type == GDK_DROP_ENTER)
  {
    gtk_widget_set_state(widget, GTK_STATE_PRELIGHT);
  } else if (event->type == GDK_DROP_LEAVE)
  {
    gtk_widget_set_state(widget, GTK_STATE_NORMAL);
  }
  
}
*/


int clist_event_cb (GtkWidget *widget, GdkEvent *event, lswin* win)
{
  GdkEventButton *bevent = (GdkEventButton *) event;
  GList *selection;
  int row;
  int notintitle = 0;
  
  GtkWidget *entry;
  GtkWidget * local_clist = win->local->clist;
  GtkWidget * remote_clist = win->remote->clist;
  lssystem *sys;


  if(widget == local_clist)
  {
    sys = win->local;
  }
  if(widget == remote_clist)
  {
    sys = win->remote;
  }

  entry = GTK_COMBO(sys->directory)->entry;

  if (bevent->type == GDK_BUTTON_RELEASE && bevent->button == 1)
  {
    if(bevent->time - sys->lastclick  > 350 &&
       bevent->time - sys->lastclick  < 950)
    {
      show_rename(win);
    }
  }
  if (event->type == GDK_BUTTON_PRESS || 
      event->type == GDK_2BUTTON_PRESS ||
      event->type == GDK_3BUTTON_PRESS)
  {
    
    if (bevent->window == GTK_CLIST (sys->clist)->clist_window)
    {

      if (bevent->type == GDK_BUTTON_PRESS && bevent->button == 3)
      {
	notintitle = 1;
	/*
        if (gtk_clist_get_selection_info (GTK_CLIST (sys->clist), 
                                          bevent->x, bevent->y, &row, NULL)) {
          selection = GTK_CLIST (sys->clist)->selection;
          if (!g_list_find (selection, (gpointer) row) && 
              (bevent->state & (GDK_CONTROL_MASK | GDK_SHIFT_MASK)) == 0) {
            clist_select_row (row, 0, sys);
          }
        }
	*/
        gtk_menu_popup (GTK_MENU (sys->menu), NULL, NULL, NULL, NULL,
			bevent->button, bevent->time);
      } 



      else /* They clicked with left mouse button.*/
      {
	gtk_signal_emit_stop_by_name(GTK_OBJECT(widget), "event");
	notintitle = 1;
        if (gtk_clist_get_selection_info (GTK_CLIST (sys->clist), 
			  (int)bevent->x, (int)bevent->y, &row, NULL))
	{
          clist_select_row (row, bevent->state, sys);
          if (!(bevent->state & (GDK_SHIFT_MASK | GDK_CONTROL_MASK)))
	  {
            if (bevent->type == GDK_2BUTTON_PRESS && bevent->button == 1)
	    {
	      
              /*server_menu_launch_callback (NULL, LAUNCH_NORMAL);*/
            }

            if (bevent->type == GDK_BUTTON_PRESS && bevent->button == 2)
	    {
              /*stat_one_server (cur_server);*/
            }
	    
	    if((bevent->time - sys->lastclick) < 10)
	    {
	      do_double_click(win, sys, sys->clist);
	    }
	    else
	    {
	      sys->lastclick = bevent->time;
	      return TRUE;
	    }
	    
          }
        }
      }
      sys->lastclick = bevent->time;
    }
    if(!notintitle)
    {
      return FALSE;
    }
    return TRUE;
  }
  return FALSE;
}




void show_rename(lswin* win)
{
  lssystem *sys  = win->local;
  GtkWidget* clist = sys->clist;
  GtkWidget *entry, *hbox;
  gint height, width, x, y, xpos, ypos;
#ifdef _WHAT_WAS_I_THINKING  
  //lsrename * rename = sys->rename;


  //superlist_get_coords(view->list, view->list->active_row, 0, &x, &y);
  //width = clist.clist_window_width + clist.column[0].area.
  width = 40;
  //height = clist->list.row_height + 5;
  height = 20;

  //gtk_box_pack_end(GTK_BOX(window), hbox, FALSE, FALSE, -height / 2);

  //gtk_widget_set_uposition(rename->window, 200, 200);
  //gtk_widget_set_uposition(entry, 0, 0);
  gdk_window_get_position ((GdkWindow*)(&GNOME_APP(win->app)->parent_object), &x, &y);

  //sys->rename->xoff = x - 200;
  //sys->rename->yoff = y - 200;
  
  //gtk_widget_set_usize (rename->hbox, width, height);
  //gtk_widget_set_usize (rename->entry, width, height);

  //gtk_entry_set_text(GTK_ENTRY(rename->entry), 
	//	     "test");
  //gtk_widget_realize(rename->entry);
  
  //gtk_grab_add(rename->entry);
  //sys->rename->c1 = 
   // gtk_signal_connect(GTK_OBJECT(sys->rename->entry),
//		       "event", 
//		       (GtkSignalFunc)rename_handler, 
//		       win);
  //sys->rename->c2 = 
    //gtk_signal_connect(GTK_OBJECT(win->app),
//		       "event", 
//		       (GtkSignalFunc)rename_handler, 
//		       win);
  
  //GTK_WIDGET_UNSET_FLAGS(view->widgets.list, GTK_CAN_FOCUS);
  //gtk_widget_show(rename->window);
  //gtk_widget_grab_focus(rename->entry);
  //gtk_grab_add(rename->entry);
  //gtk_grab_remove(entry);
  //view->widgets.tmp = hbox;
  //view->widgets.tmp_entry = entry;
#endif /*_WHAT_WAS_I_THINKING*/
}
