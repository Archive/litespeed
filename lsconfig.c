#include "lspeed.h"

int ls_write_config(opts* opts, char *filename)
{
  proplist_t lsrc_file, rc_pl;
  int i, numberofsites, sitesec_inited=0;
  site *cursite;
  GList* list;
  char* command;

  char file[MAXPATHLEN];

  char autodetect[256];
  char autoresume[256];
  char logxfers[256];
  char goodspeed[256];
  char port[256];
  char useanon[256];


  proplist_t value;
  
  proplist_t prefs_value, prefs_key;
  proplist_t sitesec_value, sitesec_key, site_key, site_value,site_key_array;
  proplist_t colorsec_key, colorsec_value;

  proplist_t normal, success, warning, error;
  proplist_t fast, medium, slow;

  sprintf(autodetect, "%i", opts->autodetecthost);
  sprintf(autoresume, "%i", opts->autoresume);
  sprintf(logxfers, "%i", opts->logxfers);
  sprintf(goodspeed, "%i", opts->goodspeed);

  prefs_value = PLMakeDictionaryFromEntries
    (
     PLMakeString("ConfigVersion"), PLMakeString("0.0.1"),
     PLMakeString("AutoDetectHostType"), PLMakeString(autodetect),
     PLMakeString("AutoResume"), PLMakeString(autoresume),
     PLMakeString("LogXfers"), PLMakeString(logxfers),
     PLMakeString("GoodSpeed"), PLMakeString(goodspeed),
     PLMakeString("AnonUser"), PLMakeString(opts->anonuser),
     PLMakeString("AnonPassword"), PLMakeString(opts->anonpassword),
     NULL
     );

  prefs_key = PLMakeString("LSGlobalPrefs");
  rc_pl = PLMakeDictionaryFromEntries(prefs_key, prefs_value, NULL);

  /*make color section*/
  colorsec_value = PLMakeDictionaryFromEntries  
    (
     PLMakeString("Normal"), PLMakeString(opts->normal),
     PLMakeString("Success"), PLMakeString(opts->success),
     PLMakeString("Warning"), PLMakeString(opts->warning),
     PLMakeString("Error"), PLMakeString(opts->error),
     PLMakeString("ProgressFast"), PLMakeString(opts->fast),
     PLMakeString("ProgressMedium"), PLMakeString(opts->medium),
     PLMakeString("ProgressSlow"), PLMakeString(opts->slow),
     NULL
     );
  
  colorsec_key = PLMakeString("LSColors");
  rc_pl = PLInsertDictionaryEntry(rc_pl, colorsec_key, colorsec_value);

  list = opts->sites;
  while(list)
  {
    cursite = (site*)list->data;
    sprintf(port, "%i", cursite->port);
    sprintf(useanon, "%i", cursite->useanon);
    site_value = PLMakeDictionaryFromEntries
      (
       PLMakeString("Name"), PLMakeString(cursite->name),
       PLMakeString("Address"), PLMakeString(cursite->address),
       PLMakeString("InitialDir"), PLMakeString(cursite->initialdir),
       PLMakeString("Login"), PLMakeString(cursite->login),
       PLMakeString("Password"), PLMakeString(cursite->password),
       PLMakeString("Port"), PLMakeString(port),
       PLMakeString("UseAnon"), PLMakeString(useanon),
       PLMakeString("HostType"), PLMakeString(cursite->hosttype),
       PLMakeString("Comment"), PLMakeString(cursite->comment),
       NULL
      );

    /*
    printf(cursite->name);
    printf(cursite->address);
    printf(cursite->initialdir);
    printf(cursite->login);
    printf(cursite->password);
    printf(port);
    printf(useanon);
    printf(cursite->hosttype);
    printf(cursite->comment);
    fflush(stdout);
    */

    site_key = PLMakeString(cursite->name);

    if(!sitesec_inited)
    {
      sitesec_value = PLMakeDictionaryFromEntries(site_key, site_value, NULL);
      sitesec_inited = TRUE;
    }
    else
    {
      sitesec_value = PLInsertDictionaryEntry(sitesec_value, 
					      site_key, site_value);
    }
    list = list->next;
  }

  sitesec_key = PLMakeString("Sites");
  rc_pl = PLInsertDictionaryEntry(rc_pl, sitesec_key, sitesec_value);

  sprintf(file, "%s/%s", g_get_home_dir(), filename);
  fprintf(stderr, "file = %s\n", file);
  lsrc_file = PLMakeString(file);
  PLSetFilename(rc_pl, lsrc_file);
  if (!PLSave(rc_pl, FALSE))
  {
    return FALSE;
  }
  PLSynchronize(rc_pl);
  command = g_strconcat("chmod og-rwx ", file, NULL);
  system(command);
}

void free_sites(opts* opts)
{
  GList* list;
  site* cursite;

  list = opts->sites;

  while(list)
  {
    cursite = (site*)list->data;
    g_free(cursite->name);
    g_free(cursite->address);
    g_free(cursite->initialdir);
    g_free(cursite->login);
    g_free(cursite->password);
    g_free(cursite->comment);
    g_free(cursite->hosttype);
    g_free(cursite);

    list = list->next;
  }

  g_list_free(opts->sites);
  opts->sites = NULL;
}

int ls_read_config(opts* opts, char *filename)
{

  proplist_t lsrc_file, rc_pl;
  char file[MAXPATHLEN];
  char command[MAXPATHLEN*2];
  int i, numberofsites;
  site *newsite;

  proplist_t value;
  char* tmp_str;

  proplist_t tmp_pl;
  
  proplist_t prefs_value, prefs_key;
  proplist_t sitesec_value, sitesec_key, site_key, site_value,site_key_array;
  proplist_t colorsec_key, colorsec_value;

  proplist_t normal, success, warning, error;
  proplist_t fast, medium, slow;
  
  sprintf(file, "%s/%s", g_get_home_dir(), filename);
  rc_pl = PLGetProplistWithPath(file);
  /*fprintf(stderr, "file = %s\n", file);*/

  if( rc_pl == NULL )/*Will have to create an initial rc file*/
  {

    /*make global prefs*/
    prefs_value = PLMakeDictionaryFromEntries
      (
       PLMakeString("ConfigVersion"), PLMakeString("0.0.1"),
       PLMakeString("AutoDetectHostType"), PLMakeString("1"),
       PLMakeString("AutoResume"), PLMakeString("1"),
       PLMakeString("LogXfers"), PLMakeString("1"),
       PLMakeString("GoodSpeed"), PLMakeString("3"),
       PLMakeString("AnonUser"), PLMakeString("anonymous"),
       PLMakeString("AnonPassword"), PLMakeString("your@email.com"),
       NULL
       );

    prefs_key = PLMakeString("LSGlobalPrefs");
    rc_pl = PLMakeDictionaryFromEntries(prefs_key, prefs_value, NULL);

    /*make color section*/
    colorsec_value = PLMakeDictionaryFromEntries  
      (
       PLMakeString("Normal"), PLMakeString("black"),
       PLMakeString("Success"), PLMakeString("green"),
       PLMakeString("Warning"), PLMakeString("orange"),
       PLMakeString("Error"), PLMakeString("red"),
       PLMakeString("ProgressFast"), PLMakeString("green"),
       PLMakeString("ProgressMedium"), PLMakeString("orange"),
       PLMakeString("ProgressSlow"), PLMakeString("red"),
       NULL
       );

    colorsec_key = PLMakeString("LSColors");
    rc_pl = PLInsertDictionaryEntry(rc_pl, colorsec_key, colorsec_value);

    /*make site section*/
    site_value = PLMakeDictionaryFromEntries
      (
       PLMakeString("Name"), PLMakeString("Gnome"),
       PLMakeString("Address"), PLMakeString("ftp.gnome.org"),
       PLMakeString("InitialDir"), PLMakeString("/pub"),
       PLMakeString("Login"), PLMakeString("anonymous"),
       PLMakeString("Password"), PLMakeString("p@sscode"),
       PLMakeString("Port"), PLMakeString("21"),
       PLMakeString("UseAnon"), PLMakeString("1"),
       PLMakeString("HostType"), PLMakeString("Auto"),
       PLMakeString("Comment"), PLMakeString("Get gnome from here."),
       NULL
       );

    site_key = PLMakeString("Gnome");
    sitesec_value = PLMakeDictionaryFromEntries(site_key, site_value, NULL);

    site_value = PLMakeDictionaryFromEntries
      (
       PLMakeString("Name"), PLMakeString("Litespeed"),
       PLMakeString("Address"), PLMakeString("ftp.litespeed.org"),
       PLMakeString("InitialDir"), PLMakeString("/pub"),
       PLMakeString("Login"), PLMakeString("anonymous"),
       PLMakeString("Password"), PLMakeString("p@sscode"),
       PLMakeString("Port"), PLMakeString("21"),
       PLMakeString("UseAnon"), PLMakeString("1"),
       PLMakeString("HostType"), PLMakeString("Auto"),
       PLMakeString("Comment"), PLMakeString("Get litespeed from here."),
       NULL
       );

    site_key = PLMakeString("Litespeed");
    sitesec_value = PLInsertDictionaryEntry(sitesec_value, 
					     site_key, site_value);

    sitesec_key = PLMakeString("Sites");
    rc_pl = PLInsertDictionaryEntry(rc_pl, sitesec_key, sitesec_value);


    /*File empty, we need to create it*/
    lsrc_file = PLMakeString(file);
    PLSetFilename(rc_pl, lsrc_file);

    /*using system to save me work and errors*/
    /*make sure the rc is safe(safer) from prying eyes*/
    sprintf(command, "touch %s; chmod 0600 %s", file, file);
    system(command);

    if (!PLSave(rc_pl, FALSE))
    {
      return FALSE;
    }
    PLSynchronize(rc_pl);
  }

  /*Now we know we have a rc file and rc_pl*/
  
  prefs_value = PLGetDictionaryEntry(rc_pl, PLMakeString("LSGlobalPrefs") );
  fprintf(stderr, "prefs_value = %i\n", prefs_value);

  /*Should free up some strings in opts*/

  tmp_pl = PLGetDictionaryEntry(prefs_value, 
	  PLMakeString("AutoDetectHostType") );
  if(tmp_pl)
  {
    tmp_str = PLGetString(tmp_pl);
    free(tmp_pl);
  }
  if(tmp_str)
  {
    opts->autodetecthost = atoi(tmp_str);
    free(tmp_str);
  }
  

  tmp_pl = PLGetDictionaryEntry(prefs_value, 
	  PLMakeString("AutoResume") );
  if(tmp_pl)
    tmp_str = PLGetString(tmp_pl);
  if(tmp_str)
    opts->autoresume = atoi(tmp_str);

  opts->logxfers = atoi
    (
     PLGetString
     (PLGetDictionaryEntry(prefs_value, PLMakeString("LogXfers") ) )
     );
  opts->goodspeed = atoi
    ( 
     PLGetString
     (PLGetDictionaryEntry(prefs_value, PLMakeString("GoodSpeed") ) )
     );
  opts->anonuser = PLGetString
    (PLGetDictionaryEntry(prefs_value, PLMakeString("AnonUser") ) );
  opts->anonpassword = PLGetString
    (PLGetDictionaryEntry(prefs_value, PLMakeString("AnonPassword") ) );


  colorsec_value = PLGetDictionaryEntry(rc_pl, PLMakeString("LSColors") );

  opts->normal = PLGetString
    (PLGetDictionaryEntry(colorsec_value, PLMakeString("Normal") ) );
  opts->success = PLGetString
    (PLGetDictionaryEntry(colorsec_value, PLMakeString("Success") ) );
  opts->warning = PLGetString
    (PLGetDictionaryEntry(colorsec_value, PLMakeString("Warning") ) );
  opts->error = PLGetString
    (PLGetDictionaryEntry(colorsec_value, PLMakeString("Error") ) );

  /*
  gdk_color_parse( opts->normal, &opts->LC_Color[LC_NORMAL]); 
  gdk_color_parse( opts->success, &opts->LC_Color[LC_SUCCESS]); 
  gdk_color_parse( opts->warning, &opts->LC_Color[LC_WARNING]); 
  gdk_color_parse( opts->error, &opts->LC_Color[LC_ERROR]);
  */

  opts->fast = PLGetString
    (PLGetDictionaryEntry(colorsec_value, PLMakeString("ProgressFast") ) );
  opts->medium = PLGetString
    (PLGetDictionaryEntry(colorsec_value, PLMakeString("ProgressMedium") ) );
  opts->slow = PLGetString
    (PLGetDictionaryEntry(colorsec_value, PLMakeString("ProgressSlow") ) );

  /*
  gdk_color_parse( opts->fast, &opts->LP_Color[LP_FAST]); 
  gdk_color_parse( opts->medium, &opts->LP_Color[LP_MEDIUM]); 
  gdk_color_parse( opts->slow, &opts->LP_Color[LP_SLOW]); 
  */

  /*free up gopts->sites structures*/
  free_sites(opts);
  
  sitesec_value = PLGetDictionaryEntry(rc_pl, PLMakeString("Sites") );
  site_key_array = PLGetAllDictionaryKeys(sitesec_value);
  numberofsites = PLGetNumberOfElements(site_key_array);

  fprintf(stderr, "Number of sites = %i\n", numberofsites);
  for(i = 0; i < numberofsites; i++)
  {
    newsite = g_new0(site,1);

    site_key = PLGetArrayElement(site_key_array, i);

    site_value = PLGetDictionaryEntry(sitesec_value, site_key);

    value = PLGetDictionaryEntry(site_value, PLMakeString("Name"));
    newsite->name = PLGetString(value);
    value = PLGetDictionaryEntry(site_value, PLMakeString("Address"));
    newsite->address = PLGetString(value);
    value = PLGetDictionaryEntry(site_value, PLMakeString("InitialDir"));
    newsite->initialdir = PLGetString(value);
    value = PLGetDictionaryEntry(site_value, PLMakeString("Login"));
    newsite->login = PLGetString(value);
    value = PLGetDictionaryEntry(site_value, PLMakeString("Password"));
    newsite->password = PLGetString(value);
    value = PLGetDictionaryEntry(site_value, PLMakeString("Comment"));
    newsite->comment = PLGetString(value);
    value = PLGetDictionaryEntry(site_value, PLMakeString("Port"));
    newsite->port = atoi(PLGetString(value));
    value = PLGetDictionaryEntry(site_value, PLMakeString("GoodSpeed"));
    if(value)
      newsite->goodspeed = atoi(PLGetString(value));
    else
      newsite->goodspeed = opts->goodspeed;
    value = PLGetDictionaryEntry(site_value, PLMakeString("UseAnon"));
    newsite->useanon = atoi(PLGetString(value));
    value = PLGetDictionaryEntry(site_value, PLMakeString("HostType"));
    newsite->hosttype = PLGetString(value);

    opts->sites = g_list_append(opts->sites, newsite);
    
  }

  return TRUE;
}

void ls_init_config(opts* opts)
{

}
