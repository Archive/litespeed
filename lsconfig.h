#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <glib.h>

typedef struct _site site;
typedef struct _opts opts;
typedef struct _connectstat connectstat;

#define COLORBUFSIZE 40

struct _site
{
  char *name;
  char *address;
  char *initialdir;
  char *login;
  char *password;
  char *comment;
  int port;
  int goodspeed;
  int useanon;
  char* hosttype;

  int totalbytesin;
  int totalbytesout;
  int totaltimexfer;
  int filesdownloaded;
};

struct _opts
{
  int autodetecthost;
  int autoresume;
  int autosavecfg;
  int persistant;
  int totalsites;
  int logxfers;
  int goodspeed;
  
  char *anonpassword;        
  char *anonuser;        
  
  GdkColor LC_Color[4];
  GdkColor LP_Color[3];

  char* normal;
  char* success;
  char* warning;
  char* error;
  
  char* fast;
  char* medium;
  char* slow;

  GList* sites;
};

struct _connectstat
{
  int connected;
  int transferring;
  int completed;

  char *curdir;
  int binary;
  float speed;
};

void ls_tempshowall(opts*);
int ls_write_config(opts*, char *filename);
int ls_remove_site(opts*, char* session);
int ls_read_config(opts*, char *filename);
void ls_init_config(opts*);
