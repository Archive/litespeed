#include "NCC-actions.h"
#include <stdio.h>

void impl_NFTPC_Open_cb(impl_POA_NFTPC * servant,
			     CORBA_long ret,
			     CORBA_Object loc,
			     CORBA_long pointer, 
			     CORBA_Environment * ev)
{
  n_connection_open_action * action = (n_connection_open_action*)
      GINT_TO_POINTER(pointer);
  GSList* signals =  action->signals;
  n_sig* sig;
  
  fprintf(stderr, "IN impl_NFTPC_Open_cb!!!\n");

  while(signals)
  {
    sig = (n_sig*)signals->data;

    /*Call user callback functions*/
    if(strcmp(sig->sig_name, "done") == 0)
    {
      (N_CONNECTION_OPEN_SIGNAL_FUNC(sig->callback))
	  (action, ret, loc, sig->user_data);
    }

    signals = signals->next;
  }

  return;
}

void
impl_NFTPC_List_cb(impl_POA_NFTPC * servant,
                 CORBA_long ret,
		 InfoList *list,
                 CORBA_long pointer,
                 CORBA_Environment * ev)
{
  n_loc_list_action* action = (n_loc_list_action*)GINT_TO_POINTER(pointer);
  GSList* signals =  action->signals;
  n_sig* sig;

  fprintf(stderr, "IN impl_NFTPC_Open_cb!!!\n");

  while(signals)
  {
    sig = (n_sig*)signals->data;

    /*Call user callback functions*/
    if(strcmp(sig->sig_name, "done") == 0)
    {
      (N_LOC_LIST_SIGNAL_FUNC(sig->callback))
	  (action, ret, list, sig->user_data);
    }

    signals = signals->next;
  }
  return;
}

void
impl_NFTPC_GetRecur_cb(impl_POA_NFTPC * servant,
                       CORBA_long ret,
                       CORBA_long pointer,
                       CORBA_Environment * ev)
{
}

